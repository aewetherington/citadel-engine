#include "pch.h"
#include "Entity.h"
#include "Sector.h"
#include "WorldState.h"

namespace Library
{
	RTTI_DEFINITIONS(Entity);

	Entity::Entity(const std::string& name)
		: Attributed(), mName(name), mActions(new Scope()), mLocalID(UINT32_MAX), mSector(nullptr)
	{
		Populate();
	}

	Entity::Entity(const Entity& rhs)
		: Attributed(rhs), mName(rhs.mName), mLocalID(UINT32_MAX), mSector(nullptr)
	{
		// Name is an external attribute, so it needs to be updated.
		Find("name")->SetStorage(&mName, 1);

		// REFACTOR ? For copying, don't automatically adopt the entity into rhs's Sector.
	}

	Entity& Entity::operator=(const Entity& rhs)
	{
		Attributed::operator=(rhs);

		// Do some initialization.
		mName = rhs.mName;
		mLocalID = UINT32_MAX;
		mSector = nullptr;

		// Name is an external attribute, so it needs to be updated.
		Find("name")->SetStorage(&mName, 1);

		return *this;
	}

	void Entity::Populate()
	{
		int isActive = 1;

		INIT_SIGNATURE();

		EXTERNAL_ATTRIBUTE("name", Datum::DatumType::String, &mName, 1);
		INTERNAL_ATTRIBUTE("isActive", Datum::DatumType::Integer, &isActive, 1);
		INTERNAL_ATTRIBUTE("actions", Datum::DatumType::Table, &mActions, 1);

		Attributed::Populate();
	}

	Scope* Entity::GetActions()
	{
		return mActions;
	}

	Action* Entity::CreateAction(const std::string& actionClassName, const std::string& actionInstanceName)
	{
		Action* newAction = Factory<Action>::Create(actionClassName);

		if (newAction == nullptr)
		{
			throw std::runtime_error("Failed to instantiate Action of type " + actionClassName);
		}

		newAction->SetName(actionInstanceName);
		newAction->SetOwner(this);
		return newAction;
	}

	const std::string& Entity::GetName() const
	{
		return mName;
	}

	void Entity::SetName(const std::string& name)
	{
		Orphan();
		mName = name;
		if (mSector != nullptr)
		{
			mLocalID = mSector->GetEntities()->Adopt(*this, mName);
		}
		else
		{
			mLocalID = UINT32_MAX;
		}
	}

	std::string Entity::GetGUID() const
	{
		if (mSector != nullptr)
		{
			return mSector->GetGUID() + "_" + mName + "_" + std::to_string(mLocalID);
		}
		else
		{
			return mName + "_" + std::to_string(mLocalID);
		}
	}

	Sector* Entity::GetSector() const
	{
		return mSector;
	}

	void Entity::SetSector(Sector* newSector)
	{
		Orphan();
		mSector = newSector;
		if (mSector != nullptr)
		{
			mLocalID = mSector->GetEntities()->Adopt(*this, mName);
		}
		else
		{
			mLocalID = UINT32_MAX;
		}
	}

	void Entity::SetActive(int state)
	{
		Find("isActive")->Set(state);
	}

	int Entity::IsActive()
	{
		return Find("isActive")->Get<int>();
	}

	void Entity::Update(WorldState& state)
	{
		// For each set of actions with matching names
		for (std::uint32_t i = 0; i != mActions->GetSize(); ++i)
		{
			Datum& sameNameActions = (*mActions)[i];

			// For each action with a given matching name
			for (std::uint32_t j = 0; j != sameNameActions.GetSize(); ++j)
			{
				// Call their update function.
				state.Action = sameNameActions.GetTable(j)->As<Action>();
				if (state.Action == nullptr)
				{
					throw std::runtime_error("Non-Action found in " + GetGUID() + "'s Action list.");
				}
				if (state.Action->Find("isActive")->Get<int>())
				{
					state.Action->Update(state);
				}
			}
		}

		state.Action = nullptr;
	}

}