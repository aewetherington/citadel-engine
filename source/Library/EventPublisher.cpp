#include "pch.h"
#include "EventPublisher.h"
#include "EventSubscriber.h"
#include <list>

using namespace std;
using namespace std::chrono;

namespace Library
{

	RTTI_DEFINITIONS(EventPublisher);

	EventPublisher::EventPublisher(SList<EventSubscriber*>& subscribers, mutex& subscribersMutex)
		: mSubscribersReference(&subscribers), mSubscribersMutexReference(&subscribersMutex), mEnqueuedTime(), mDelay(0)
	{

	}

	void EventPublisher::SetTime(const std::chrono::high_resolution_clock::time_point& currentTime, const std::chrono::milliseconds& delay)
	{
		lock_guard<mutex> lock(mTimeMutex);
		mEnqueuedTime = currentTime;
		mDelay = delay;
	}

	const std::chrono::high_resolution_clock::time_point EventPublisher::GetTimeEnqueued() const
	{
		lock_guard<mutex> lock(mTimeMutex);
		return mEnqueuedTime;
	}

	const std::chrono::milliseconds EventPublisher::GetDelay() const
	{
		lock_guard<mutex> lock(mTimeMutex);
		return mDelay;
	}

	bool EventPublisher::IsPrimed(const high_resolution_clock::time_point& currentTime) const
	{
		lock_guard<mutex> lock(mTimeMutex);
		if (currentTime > (mEnqueuedTime + mDelay))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void EventPublisher::Deliver(shared_ptr<EventPublisher> thisEventPublisher) const
	{
		list<future<void>> futures;

		{
			lock_guard<mutex> lock(*mSubscribersMutexReference);
			for (auto subscriber : *mSubscribersReference)
			{
				// CHECK : Is this correct syntax?
				// Fire off a new notify thread
				futures.emplace_back(async(&EventSubscriber::Notify, subscriber, thisEventPublisher));

				// Old version:
				// subscriber->Notify(thisEventPublisher);
			}
		}

		// Wait for notify threads to finish.
		for (auto& f : futures)
		{
			// Re-throw exceptions in the calling context.
			f.get();
		}
	}

}