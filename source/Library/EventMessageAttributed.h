#pragma once

/*!	\file EventMessageAttributed.h
	\brief Contains the declaration for EventMessageAttributed, which is used to provide generic event messages, primarily to ReactionAttributed objects.
*/

#include "pch.h"
#include "Attributed.h"

namespace Library
{
	class World;

	/*!	\brief Used to provide generic event messages, primarily to ReactionAttributed objects, and primarily constructed by ActionEvents.
	*/
	class EventMessageAttributed : public Attributed
	{
		RTTI_DECLARATIONS(EventMessageAttributed, Attributed);

	public:
		/*!	\brief Sets initial values.
		*/
		EventMessageAttributed();

		/*!	\brief Frees any allocated data.
		*/
		virtual ~EventMessageAttributed() = default;

		/*!	\brief Performs a deep copy of this EventMessageAttributed
			@param rhs The EventMessageAttributed to copy.
		*/
		EventMessageAttributed(const EventMessageAttributed& rhs);

		/*!	\brief Performs a deep copy of this EventMessageAttributed
			@param rhs The EventMessageAttributed to copy.
			@return The newly copied EventMessageAttributed.
		*/
		EventMessageAttributed& operator=(const EventMessageAttributed& rhs);

		/*!	\brief Populates the prescribed attributes of EventMessageAttributed.
		*/
		void Populate();

		/*!	\brief Sets the subtype parameter of this EventMessageAttributed.
			@param subtype The subtype parameter to set for this EventMessageAttributed.
		*/
		void SetSubtype(const std::string& subtype);

		/*!	\brief Gets the subtype parameter of this EventMessageAttributed.
			@return The subtype parameter of this EventMessageAttributed.
		*/
		const std::string& GetSubtype() const;

		/*!	\brief Sets the world parameter of this EventMessageAttributed.
			@param subtype The world parameter to set for this EventMessageAttributed.
		*/
		void SetWorld(World* world);

		/*!	\brief Gets the world parameter of this EventMessageAttributed.
			@return The world parameter of this EventMessageAttributed.
		*/
		World* GetWorld() const;

		/*!	\brief A string used to locate the subtype parameter.
		*/
		const static std::string sSubtypeTag;

	private:
		std::string mSubtype;
		World* mWorld;
	};

}