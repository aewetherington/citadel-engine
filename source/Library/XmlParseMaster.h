#pragma once

/*!	\file XmlParseMaster.h
	\brief Contains the declarations for XmlParseMaster and SharedData classes.
*/

#include "pch.h"
#include "RTTI.h"
#include "expat.h"

namespace Library
{
	class IXmlParseHelper;

	/*!	\brief The managing component of an XML-parsing system. Has a 1-to-1 relationship with SharedData objects.
	*/
	class XmlParseMaster final
	{
	public:
		
		/*!	\brief The base for data classes that a given XmlParseMaster uses. Has a 1-to-1 relationship with XmlParseMaster objects.
		*/
		class SharedData : public RTTI
		{
			friend XmlParseMaster;

			RTTI_DECLARATIONS(SharedData, RTTI);

		public:
			/*!	\brief Initializes members to default values.
			*/
			SharedData();

			/*!	\brief Cleans up allocated data owned by this class.
			*/
			virtual ~SharedData() = default;

			/*!	\brief Generates a deep copy of this SharedData object on the heap, prepares it for usage, and returns a pointer to it.

				User is responsible for calling delete on the returned value once finished with it, unless it is owned by a cloned XmlParseMaster.
				@return Address of a new SharedData object.
			*/
			virtual SharedData* Clone() const = 0;

			/*!	\brief Retrieves the address of the associated XmlParseMaster object.
				@return The address of the associated XmlParseMaster object
			*/
			XmlParseMaster* GetXmlParseMaster() const;

			/*!	\brief Called by the associated XmlParseMaster when the XML parsing results in going deeper by one level.
			*/
			virtual void IncrementDepth();

			/*!	\brief Called by the associated XmlParseMaster when the XML parsing results in returning upwards by one level.
			*/
			virtual void DecrementDepth();

			/*!	\brief Returns the current depth level. Zero is the lowest level, which represents being outside of any XML blocks.
				@return The current parsing depth level.
			*/
			std::uint32_t GetDepth() const;

			/*!	\brief Returns whether this SharedData object is a clone or not.
				@return Returns true if this object is a clone, and false otherwise.
			*/
			bool IsClone() const;

		protected:
			bool mIsClone;	// Whether this object is a clone or not.

		private:
			// Sets the parse master.
			void SetXmlParseMaster(XmlParseMaster* parseMaster);

			SharedData(const SharedData& rhs) = delete;
			SharedData& operator=(const SharedData& rhs) = delete;

			std::uint32_t mDepth;	// The current parsing depth.
			XmlParseMaster* mParseMaster;	// The address of the associated XmlParseMaster
		};

		/*!	\brief Initializes data members.
			@param sharedData The SharedData object to be used by this XmlParseMaster.
		*/
		explicit XmlParseMaster(SharedData* sharedData = nullptr);

		/*!	\brief Cleans up any owned data.

			If this XmlParseMaster object is a clone, deletes its associated helpers and SharedData as well.
		*/
		~XmlParseMaster();

		/*!	\brief Generates a deep copy of this XmlParseMaster on the heap, prepares it for usage, and returns a pointer to it.

			User is responsible for calling delete on the returned value once finished with it.
			@return Address of a new XmlParseMaster.
		*/
		XmlParseMaster* Clone() const;

		/*!	\brief Adds this helper object to the list of registered helpers.
			@param helper The helper object to be added.
		*/
		void AddHelper(IXmlParseHelper& helper);

		/*!	\brief Removes this helper object from the list of registered helpers.
			@param helper The helper object to be removed.
		*/
		void RemoveHelper(IXmlParseHelper& helper);

		/*!	\brief Parses the provided string, and calls the appropriate handler methods.
			@param data The string to be parsed.
			@param length The number of non-null characters to be parsed in data.
			@param isFinal Pass in true to mean this is the last block of text, and false if there is more text to come.
			@throws std::runtime_error Thrown if the provided XML is invalid.
			@see ParseFromFile(const std::string&)
		*/
		void Parse(const char* data, std::uint32_t length, bool isFinal) const;

		/*!	\brief Parses an XML file with the specified name.
			@param filename The name of the file to be parsed.
			@throws std::runtime_error Thrown if the file failed to open.
			@see Parse(const char*, std::uint32_t, bool)
		*/
		void ParseFromFile(const std::string& filename);

		/*!	\brief Returns the name of the file currently being parsed.
			@return The name of the file currently being parsed. If no file is being parsed, returns an empty string.
		*/
		std::string& GetFileName();

		/*! \brief Returns an address to the SharedData object associated with this XmlParseMasteer.
			@return The address to the SharedData object associated with this XmlParseMaster.
		*/
		SharedData* GetSharedData() const;

		/*!	\brief Constructs the relationship between this XmlParseMaster object and the provided SharedData object.
			
			If this XmlParseMaster object had a previous relationship with a SharedData object, breaks that relationship first.
			@param shared The SharedData object to link to this XmlParseMaster object.
		*/
		void SetSharedData(SharedData* shared);

	private:
		// Distributes the start element handling request across the helpers.
		static void StartElementHandler(void* userData, const char* name, const char** attribs);

		// Distributes the end element handling request across the helpers.
		static void EndElementHandler(void* userData, const char* name);

		// Distributes the character data handling request across the helpers.
		static void CharDataHandler(void* userData, const char* str, int length);

		XmlParseMaster(const XmlParseMaster& rhs) = delete;
		XmlParseMaster& operator=(const XmlParseMaster& rhs) = delete;

		XML_Parser mParser;					// The Expat parser object this class wraps.
		SList<IXmlParseHelper*> mHelpers;	// List of parse helper objects.
		std::string mFilename;				// Name of file being parsed.
		SharedData* mSharedData;			// The SharedData object associated with this XmlParseMaster.
		bool mIsClone;						// Whether this object is a clone or not.
	};

}

