#pragma once

/*!	\file Reaction.h
	\brief Contains the declaration for Reaction, which is the parent of all Reaction classes, which only update when triggered by an event.
*/

#include "pch.h"
#include "ActionList.h"
#include "EventSubscriber.h"

namespace Library
{
	/*!	\brief The parent of all Reaction classes, which only update when triggered by an event.
	*/
	class Reaction : public ActionList, public EventSubscriber
	{
		RTTI_DECLARATIONS(Reaction, ActionList);

	public:
		/*!	\brief Sets initial values based on the passed parameters
			@param mName Initial name to give this Reaction.
		*/
		Reaction(const std::string& name = "Reaction");

		/*!	\brief Frees any allocated data.
		*/
		virtual ~Reaction() = default;

		/*!	\brief Populates the prescribed attributes of Reaction.
		*/
		void Populate();

		/*!	\brief Overrides ActionList's Update to prevent it from being called by normal means, so that it is instead only called as a result of a Notify() call.
			@param state The current WorldState.
		*/
		virtual void Update(WorldState& state) override;

		/*!	\brief Callback function for when an event get primed and delivered to the class, if it has previously subscribed to that type of event.
			@param eventPublisher The event that was fired off and now delivered.
		*/
		virtual void Notify(std::shared_ptr<EventPublisher> eventPublisher) override;
	};

	ActionFactory(Reaction);

}