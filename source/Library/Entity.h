#pragma once

/*!	\file Entity.h
	\brief Contains the declaration for Entity, the root class for all entities in the game.
*/

#include "Attributed.h"
#include "Factory.h"
#include "Action.h"

namespace Library
{
	class Sector;
	class WorldState;

	/*!	\brief The root class for all entities in the game
	*/
	class Entity : public Attributed
	{
		RTTI_DECLARATIONS(Entity, Attributed);

	public:
		/*!	\brief Constructs a new Entity to default values, and uses the provided name.
			@param name The name to give the new Entity.
		*/
		Entity(const std::string& name = "Entity");

		/*!	\brief Performs a deep copy of the argument.
			@param rhs The Entity to copy.
		*/
		Entity(const Entity& rhs);

		/*!	\brief Destroys any owned data.
		*/
		virtual ~Entity() = default;

		/*!	\brief Performs a deep copy of the argument
			@param rhs The Entity to copy.
			@return A reference to this entity, post-copy.
		*/
		Entity& operator=(const Entity& rhs);

		/*!	\brief Initializes prescribed members.
		*/
		void Populate();

		/*!	\brief Returns a pointer to the Scope that holds the Actions for this object.
		*/
		Scope* GetActions();

		/*!	\brief Creates a new Action based on the name and instance strings within this Entity.
			@param actionClassName The classname of the Action to be created
			@param actionInstanceName The name to give the Action
			@return A pointer to the newly created Action.
			@throws std::runtime_error Thrown when the Action fails to be created.
		*/
		Action* CreateAction(const std::string& actionClassName, const std::string& actionInstanceName);


		/*!	\brief Returns the name of this Entity
			@return The name of this Entity.
		*/
		const std::string& GetName() const;

		/*!	\brief Sets the name of this Entity
			@param name The name to set this Entity's name to.
		*/
		void SetName(const std::string& name);

		/*!	\brief Retrieves a GUID string, which is only unique at the precision of the highest point in this object's hierarchy.
			@return A GUID string.
		*/
		std::string GetGUID() const;

		/*!	\brief Returns a pointer to the Sector this Entity is contained in.
			@return The Sector that contains this Entity.
		*/
		Sector* GetSector() const;

		/*!	\brief Sets the Sector this Entity is contained in.
			@param newSector The Sector to contain this Entity in.
		*/
		void SetSector(Sector* newSector);

		/*!	\brief Sets the active state of this object, based on the parameter.
			@param The active state to set this object to.
		*/
		void SetActive(int state);

		/*!	\brief Gets the active state of this object, and returns it.
			@return The active state of this object.
		*/
		int IsActive();

		/*!	\brief To be called on every frame. Performs a full Entity update.
			@param state The game state, which is to be considered every update.
		*/
		virtual void Update(WorldState& state);

	protected:
		std::string mName;
		std::uint32_t mLocalID;
		Sector* mSector;
		Scope* mActions;

	};

#define EntityFactory(ConcreteEntityT) ConcreteFactory(ConcreteEntityT, Entity);

	EntityFactory(Entity)

}