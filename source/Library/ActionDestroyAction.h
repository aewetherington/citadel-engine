#pragma once

/*!	\file ActionDestroyAction.h
	\brief Contains the declaration for ActionDestroyAction, which is an action that deletes sibling actions with a given name.
*/

#include "Action.h"
#include "Factory.h"

namespace Library
{
	/*!	\brief Action that deletes sibling actions with a given name.
	*/
	class ActionDestroyAction : public Action
	{
		RTTI_DECLARATIONS(ActionDestroyAction, Action);

	public:
		/*!	\brief Initializes default values.
			@param name The name to give the action.
		*/
		ActionDestroyAction(const std::string& name = "ActionDestroyAction");

		/*!	\brief Deallocates owned memory.
		*/
		virtual ~ActionDestroyAction() = default;

		/*!	\brief Populates the prescribed attributes for this object.
		*/
		void Populate();

		/*!	\brief Sets the name for the Action to be deleted on update.
			@param deleteActionName The name to search for.
		*/
		void SetDeleteAction(const std::string& deleteActionName);

		/*!	\brief Returns the name for the Action to be deleted on update.
			@return The name to search for.
		*/
		const std::string& GetDeleteAction() const;

		/*!	\brief To be called every frame.
			Attempts to all sibling Actions with the known name, leaving the name's entry empty.
			@param state The game state, which is to be considered every update.
			@throws std::runtime_error Thrown if this action currently has no owner.
			@throws std::runtime_error Thrown if the deleteActionName attribute isn't set.
			@throws std::runtime_error Thrown if attempting to destroy self.
			@throws std::runtime_error Thrown if attempting to destroy an object that isn't an Action.
		*/
		virtual void Update(WorldState& state) override;

		/*!	\brief The string key used to directly access the delete actions with name attribute.
		*/
		static const std::string sDeleteActionAttribute;
	};

	ActionFactory(ActionDestroyAction);

}