#pragma once

class DummyClass {
public:
	bool static isPositive(int val);
	bool static isEven(int val);
};