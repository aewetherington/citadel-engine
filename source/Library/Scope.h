#pragma once

/*!	\file Scope.h
	\brief Contains the declaration for a runtime-determined Datum hierarchy.
*/

#include "pch.h"

namespace Library
{

	/*!	\brief Runtime-determined Datum hierarchy object.
	*/
	class Scope : public RTTI
	{
		RTTI_DECLARATIONS(Scope, RTTI);

	public:
		typedef HashMap<std::string, Datum>::PairType StringDatumPair;

		/*!	\brief Initializes a new Scope instance.
			@param capacity The size to make the underlying HashMap.
		*/
		explicit Scope(std::uint32_t capacity = 10U);

		/*!	\brief Makes a copy of the provided Scope object.
			@param rhs The Scope to be copied.
			@see operator=(const Scope&)
		*/
		Scope(const Scope& rhs);

		/*!	\brief Clears any stored memory, and handles potential changes in the Scope hierarchy.
		*/
		virtual ~Scope();

		/*!	\brief Makes a copy of the provided Scope object.
			@param rhs The Scope to be copied.
			@return A reference to the new Scope object.
			@see Scope(const Scope&)
		*/
		Scope& operator=(const Scope& rhs);

		/*!	\brief Accesses a Datum based on the specified string.

			If no match is found, a new entry is made.
			@param key The key to search with.
			@return A reference to the Datum that was either found, or created using the key.
			@see Append(const std::string&)
		*/
		Datum& operator[](const std::string& key);

		/*!	\brief Accesses a Datum based on the specified index.
			@param index The index to use in the lookup.
			@return A reference to the Datum that was found at the specified index.
			@throws std::runtime_error Thrown when the index is out of range.
		*/
		Datum& operator[](std::uint32_t index) const;

		/*!	\brief Compares this Scope to the parameter, to test if the two are equivalent.
			@param rhs The Scope to compare against.
			@return True only if the two Scope instances are equivalent.
		*/
		bool operator==(const Scope& rhs) const;

		/*!	\brief Compares this Scope to the parameter, to test if the two are not equivalent.
			@param rhs The Scope to compare against.
			@return True only if the two Scope instances are not equivalent.
		*/
		bool operator!=(const Scope& rhs) const;

		/*!	\brief Attempts to find an entry with the associated key
			@param key The key to be used in the search.
			@return A pointer to the Datum object that matches the key. If a match isn't found, nullptr is returned.
		*/
		Datum* Find(const std::string& key) const;

		/*!	\brief Given a table, looks for the matching name of the table in this Scope.
			@param table The Scope object to look for.
			@return If a match is found, returns the name. If no match is found, returns an empty string.
		*/
		std::string FindName(const Scope& table) const;

		/*!	\brief Given a table, looks for the matching name of the table in this Scope.
			@param table The Scope object to look for.
			@param foundName The output parameter that the name is stored in.
			@return True only if a match was found.
		*/
		bool FindName(const Scope& table, std::string& foundName) const;

		/*!	\brief Searches up the Scope hierarchy to try and find a Datum with the matching key.

			If provided, a pointer to the Scope in which the Datum was found will be set in associatedScope.
			If no match was found for the key, the Scope* pointer in associatedScope will be set to nullptr.
			@param key The key to search for.
			@param associatedScope An output parameter which, if provided, receives a Scope* to the Scope which held the matching Datum.
			@return A pointer to the first Datum with a matching key. If none was found, nullptr is returned.
		*/
		Datum* Search(const std::string& key, Scope** associatedScope = nullptr) const;

		/*!	\brief Creates a Datum associated with the string key parameter, if it doesn't exist.

			Creates a Datum if no match is found for the key. If a match is found, returns that Datum instead.
			@param key The key to search for.
			@return The Datum associated with this key.
			@see AppendScope(const std::string&)
		*/
		Datum& Append(const std::string& key);

		/*!	\brief Creates a Scope associated with the string key parameter, if it doesn't exist.

			Creates a Scope if no match is found for the key. If a match is found, returns that Scope instead.
			@param key The key to search for.
			@return The Scope associated with this key.
			@throws std::runtime_error Thrown if key already associated with a non-Scope Datum.
			@throws std::runtime_error Thrown if key already associated with externally stored Datum.
			@see Append(const std::string&)
		*/
		Scope& AppendScope(const std::string& key);

		/*!	\brief Places the given child into this table, with the given name, at the given index.
			@param child The child to be adopted.
			@param key The name to give the child being adopted.
			@param index The location at which to try to store the child within the Datum obtained with the key.
			@throws std::runtime_error Thrown if key already associated with a non-Scope Datum.
			@throws std::runtime_error Thrown if key already associated with externally stored Datum.
			@return The index at which the child was actually stored. This isn't equal to the index parameter only when it wasn't provided, or is greater than the size of the array.
		*/
		std::uint32_t Adopt(Scope& child, const std::string& key, std::uint32_t index = UINT32_MAX);

		/*!	\brief Returns the address of the table which contains this one.
			@return The Scope that is the parent of this one.
		*/
		Scope* GetParent() const;

		/*!	\brief Retrieves a pointer to a StringDatumPair from the ordered references list.
			@param index The index to use in the lookup.
			@return A pointer to the pair at the specified index.
			@throws std::runtime_error Thrown when the index is out of range.
		*/
		StringDatumPair* GetOrderedReference(std::uint32_t index) const;

		/*!	\brief Breaks the link between this Scope and its parent, if one exists.
			@throws std::runtime_error Thrown when the child claimed to have a parent, but the parent had no record of the child.
		*/
		void Orphan();

		/*! \brief Destroys all data and child scopes this Scope has as internal storage.
		*/
		virtual void Clear();

		/*!	\brief Returns the number of StringDatumPair entries in the Scope.
			@return The number of entries in the Scope.
		*/
		std::uint32_t GetSize() const;

	private:
		HashMap<std::string, Datum> mMap;
		Vector<StringDatumPair*> mReferences;
		Scope* mParent;
	};
}