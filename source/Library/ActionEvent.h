#pragma once

/*!	\file ActionEvent.h
	\brief Contains the declaration for ActionEvent, which is an Action that creates EventMessageAttributed objects and enqueues them in the current world.
*/

#include "pch.h"
#include "Action.h"
#include "Factory.h"

namespace Library
{
	/*!	\brief An Action that creates EventMessageAttributed objects and enueues events holding them in the current world.
	*/
	class ActionEvent : public Action
	{
		RTTI_DECLARATIONS(ActionEvent, Action);

	public:
		/*!	\brief Sets initial values based on the passed parameters
			@param mName Initial name to give this ActionEvent.
		*/
		ActionEvent(const std::string& name = "ActionEvent");

		/*!	\brief Frees any allocated data.
		*/
		virtual ~ActionEvent() = default;

		/*!	\brief Populates the prescribed attributes of ActionEvent.
		*/
		void Populate();

		/*!	\brief Generates a new Event with template parameter EventMessageAttributed, based on the current ActionEvent's subtype, delay, and auxiliary attributes.
			@param state The current WorldState.
		*/
		virtual void Update(WorldState& state) override;

		/*!	\brief Sets the subtype parameter of this ActionEvent.
			@param subtype The subtype parameter to set for this ActionEvent.
		*/
		void SetSubtype(const std::string& subtype);

		/*!	\brief Gets the subtype parameter of this ActionEvent.
			@return The subtype parameter of this ActionEvent.
		*/
		const std::string& GetSubtype() const;
		
		/*!	\brief Sets the delay parameter of this ActionEvent.
			@param subtype The delay parameter to set for this ActionEvent.
		*/
		void SetDelay(std::int32_t delay);

		/*!	\brief Gets the subtype parameter of this ActionEvent.
			@return The subtype parameter of this ActionEvent.
		*/
		std::int32_t GetDelay() const;

		/*!	\brief A string used to locate the delay parameter.
		*/
		const static std::string sDelayTag;
	};

	ActionFactory(ActionEvent);
}