#pragma once

/*!	\file Attributed.h
	\brief Contains the declaration for an interface with which to mirror native C++ classes in Scope form.
*/

#include "pch.h"
#include "Scope.h"
#include "HashMap.h"
#include "Vector.h"

namespace Library
{
	/*!	\brief Interface with which to easily mirror native C++ classes in Scope form.
	*/
	class Attributed : public Scope
	{
		RTTI_DECLARATIONS(Attributed, Scope)

	public:
		/*!	\brief Initializes a new Attributed instance.
		*/
		Attributed();

		/*!	\brief Deep copies a provided Attributed instance.
			@param rhs The Attributed to be copied.
			@see operator=(const Attributed&)
		*/
		Attributed(const Attributed& rhs);

		/*!	\brief Destructs the Attributed instance.
		*/
		virtual ~Attributed() = default;

		/*!	\brief Deep copies a provided Attributed instance.
			@param rhs The Attributed to be copied.
			@return The newly copied Attributed object.
			@see Attributed(const Attributed&);
		*/
		Attributed& operator=(const Attributed& rhs);

		/*!	\brief Populates the Scope with prescribed members.

			This must be called before any auxiliary attributes are appended. It must only be called once per signature set, typically in the class's constructor.
			@throws std::runtime_error Thrown if a signature is found that has either both or neither internal or external storage specified.
			@throws std::runtime_error Thrown if a signature is found with an engine-reserved name.
			@throws std::runtime_error Thrown if a signature is found with an already-in-use name.
		*/
		void Populate();

		/*!	\brief Returns true if the provided string is the name of a prescribed attribute for this class.
			@param attr The string to test for.
			@return True only if the provided string is the name of a prescribed attribute for this class.
			@see IsAuxiliaryAttribute(const std::string&)
			@see IsAttribute(const std::string&)
		*/
		bool IsPrescribedAttribute(const std::string& attr) const;

		/*!	\brief Returns true if the provided string is the name of an auxiliary attribute for this object.
			@param attr The string to test for.
			@return True only if the provided string is the name of an auxiliary attribute for this object.
			@see IsPrescribedAttribute(const std::string&)
			@see IsAttribute(const std::string&)
		*/
		bool IsAuxiliaryAttribute(const std::string& attr) const;

		/*!	\brief Returns true if the provided string is the name of an attribute for this object.
			@param attr The string to test for.
			@return True only if the provided string is the name of an attribute for this object.
			@see IsPrescribedAttribute(const std::string&)
			@see IsAuxiliaryAttribute(const std::string&)
		*/
		bool IsAttribute(const std::string& attr) const;

		/*!	\brief Appends a new Datum, with the name provided, and returns a reference to it.
			@param attr The string to test for.
			@return A reference to the newly appended Datum.
			@throws std::runtime_error Thrown if the Attributed has not been populated yet.
			@throws std::runtime_error Thrown if the provided name matches that of an existing prescribed attribute.
		*/
		Datum& AppendAuxiliaryAttribute(const std::string& attr);

		/*!	\brief Returns the index of the first auxiliary attribute.
			@return The index of the first auxiliary attribute.
		*/
		std::uint32_t AuxiliaryBegin() const;

		/*!	\brief Clears the Scope data, then prepares Attributed for repopulation, and re-adds reserved members.

			If overridden, must be called from the child at the start of the override.
		*/
		virtual void Clear() override;

	protected:
		union DatumValue
		{
			std::int32_t* i;
			float* f;
			glm::vec4* v;
			glm::mat4* m;
			std::string* s;
			Scope** t;
			RTTI** r;

			DatumValue() : i(nullptr) {}
			DatumValue(void* val) { i = reinterpret_cast<int*>(val); }
		};

		class Signature
		{
		public:
			std::string Name;
			Datum::DatumType Type;
			DatumValue InitialValue;
			std::uint32_t Size;
			void* Storage;

			Signature();
			Signature(std::string name, Datum::DatumType type, void* InitialValue, std::uint32_t Size, void* Storage);
		};

		static HashMap<std::uint32_t, Vector<Signature>> sSignatures;
		
	private:

		bool mIsPopulated;
		std::uint32_t mNumPrescribedAttributes;

		// Populate helper functions.
		typedef void(*PopulateFunction)(Attributed&, const Signature&);
		static void PopulateUnknown(Attributed& a, const Signature& sig);
		static void PopulateInteger(Attributed& a, const Signature& sig);
		static void PopulateFloat(Attributed& a, const Signature& sig);
		static void PopulateVector(Attributed& a, const Signature& sig);
		static void PopulateMatrix(Attributed& a, const Signature& sig);
		static void PopulateString(Attributed& a, const Signature& sig);
		static void PopulateTable(Attributed& a, const Signature& sig);
		static void PopulatePointer(Attributed& a, const Signature& sig);
		static const PopulateFunction sPopulateFuncts[static_cast<int>(Datum::DatumType::MAX)];

#define INIT_SIGNATURE()																			\
	std::uint32_t SIGNATURES_VECTOR_SIZE = sSignatures[TypeIdInstance()].GetSize();					\
	sSignatures[TypeIdInstance()].Clear();															\
	sSignatures[TypeIdInstance()].Reserve(SIGNATURES_VECTOR_SIZE);

#define INTERNAL_ATTRIBUTE(name, type, initialValue, size)											\
	sSignatures[TypeIdInstance()].PushBack(Signature(name, type, initialValue, size, nullptr));

#define EXTERNAL_ATTRIBUTE(name, type, storage, size)												\
	sSignatures[TypeIdInstance()].PushBack(Signature(name, type, nullptr, size, storage));

	};

}