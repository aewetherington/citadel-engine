#include "pch.h"
#include "ActionEvent.h"
#include "EventMessageAttributed.h"
#include "Event.h"
#include "World.h"

using namespace std;
using namespace std::chrono;

namespace Library
{

	RTTI_DEFINITIONS(ActionEvent);

	ActionEvent::ActionEvent(const std::string& name)
		: Action(name)
	{
		Populate();
	}

	void ActionEvent::Populate()
	{
		Action::Populate();

		string subtype;
		int delay = 0;

		INIT_SIGNATURE();

		INTERNAL_ATTRIBUTE(EventMessageAttributed::sSubtypeTag, Datum::DatumType::String, &subtype, 1);
		INTERNAL_ATTRIBUTE(sDelayTag, Datum::DatumType::Integer, &delay, 1);

		Attributed::Populate();
	}

	void ActionEvent::Update(WorldState& state)
	{
		EventMessageAttributed newEventMessage;
		newEventMessage.SetSubtype(Find(EventMessageAttributed::sSubtypeTag)->Get<string>());
		newEventMessage.SetWorld(state.World);

		for (uint32_t i = AuxiliaryBegin(); i < GetSize(); ++i)
		{
			newEventMessage.AppendAuxiliaryAttribute(GetOrderedReference(i)->first) = GetOrderedReference(i)->second;
		}

		auto newEvent = make_shared<Event<EventMessageAttributed>>(newEventMessage);

		state.World->GetEventQueue().Enqueue(newEvent, high_resolution_clock::now(), milliseconds(Find(sDelayTag)->Get<int>()));
	}

	void ActionEvent::SetSubtype(const std::string& subtype)
	{
		Find(EventMessageAttributed::sSubtypeTag)->Set(subtype);
	}

	const std::string& ActionEvent::GetSubtype() const
	{
		return Find(EventMessageAttributed::sSubtypeTag)->Get<string>();
	}

	void ActionEvent::SetDelay(std::int32_t delay)
	{
		Find(sDelayTag)->Set(delay);
	}

	std::int32_t ActionEvent::GetDelay() const
	{
		return Find(sDelayTag)->Get<int32_t>();
	}

	const std::string ActionEvent::sDelayTag = "delay";
}