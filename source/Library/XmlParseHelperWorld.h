#pragma once

/*!	\file XmlParseHelperTable.h
	\brief Contains the declaration for an XML parse helper that parses XML World data.
*/

#include "IXmlParseHelper.h"
#include "SharedDataTable.h"

namespace Library
{
	/*!	\brief XML parse helper for parsing Worlds, which checks to enforce grammar syntax rules.
	*/
	class XmlParseHelperWorld : public IXmlParseHelper
	{
	public:
		/*!	\brief Initializes this instance to a default state.
		*/
		XmlParseHelperWorld() = default;

		/*!	\brief Cleans up any owned data.
		*/
		virtual ~XmlParseHelperWorld() = default;

		/*!	\brief Handler for XML object start tags, called by the XmlParseMaster that is using this helper.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper. If no SharedData object is associated, the value is nullptr.
			@param name The name of the tag.
			@param atttributes A HashMap containing name/value pairs of attributes listed in the XML start tag.
			@throws std::runtime_error Thrown if no name is provided for the element.
			@throws std::runtime_error Thrown if a reserved term conflights with usage in the XML.
			@throws std::runtime_error Thrown if the size attributed is specified, and the value was invalid.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes) override;

		/*!	\brief Handler for XML object end tags, called by the XmlParseMaster that is using this helper. If no SharedData object is associated, the value is nullptr.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper.
			@param name The name of the tag.
			@throws std::runtime_error Thrown if the depth is invalid for a given tag.
			@throws std::runtime_error Thrown if a given tag's parent tag is invalid.
			@throws std::runtime_error Thrown if the root is detected to be an array of tables.
			@throws std::runtime_error Thrown if ambiguity between scalar and array is detected.
			@throws std::runtime_error Thrown if no data is provided for a type that requires data.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name) override;

		/*!	\brief Generates a deep copy of this XmlParseHelperTable on the heap, prepares it for usage, and returns a pointer to it.

			User is responsible for calling delete on the returned value once finished with it, unless it is owned by a cloned XmlParseMaster.
			@return Address of a new parse helper.
		*/
		virtual IXmlParseHelper* Clone() const override;

	private:
		// Helper function for EndElementHandler(). Parses value tags.
		void ParseValue(SharedDataTable* shared, std::string name);

		// Helper function for EndElementHandler(). Parses world tags.
		void ParseWorld(SharedDataTable* shared, std::string name);
	};

}