#include "Event.h"

namespace Library
{
	template <typename ConcreteMessageT>
	RTTI_DEFINITIONS(Event<ConcreteMessageT>);

	template <typename ConcreteMessageT>
	SList<EventSubscriber*> Event<ConcreteMessageT>::mSubscribers;

	template <typename ConcreteMessageT>
	Event<ConcreteMessageT>::Event(const ConcreteMessageT& message)
		: EventPublisher(mSubscribers), mMessage(reinterpret_cast<const RTTI*>(&message))
	{
		
	}

	template <typename ConcreteMessageT>
	void Event<ConcreteMessageT>::Subscribe(EventSubscriber& subscriber)
	{
		mSubscribers.PushBack(&subscriber);
	}

	template <typename ConcreteMessageT>
	void Event<ConcreteMessageT>::Unsubscribe(EventSubscriber& subscriber)
	{
		mSubscribers.Remove(&subscriber);
	}

	template <typename ConcreteMessageT>
	void Event<ConcreteMessageT>::UnsubscribeAll()
	{
		mSubscribers.Clear();
	}

	template <typename ConcreteMessageT>
	ConcreteMessageT* Event<ConcreteMessageT>::GetMessage()
	{
		return mMessage->As<ConcreteMessageT>();
	}
}