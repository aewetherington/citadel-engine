#pragma once
#include "XmlParseMaster.h"

namespace Library
{
	/*!	\brief Shared data class for handling the parsing of table (Scope) data.
	*/
	class SharedDataTable : public XmlParseMaster::SharedData
	{
		friend class XmlParseHelperTable;
		friend class XmlParseHelperPrimitive;
		friend class XmlParseHelperEntity;
		friend class XmlParseHelperSector;
		friend class XmlParseHelperWorld;
		friend class XmlParseHelperAction;

		RTTI_DECLARATIONS(SharedDataTable, SharedData);

	public:
		/*!	\brief Constructs a new SharedDataTable that uses the Scope argument as its root.
		@param root The Scope set as the root of this instance.
		*/
		SharedDataTable(Scope& root);

		/*!	\brief Cleans up any owned data.
		*/
		virtual ~SharedDataTable();

		/*!	\brief Creates an empty, ready-for-use clone based on the settings of this SharedDataTable.
		@return A pointer to the new SharedDataTable
		*/
		virtual SharedData* Clone() const;

		/*!	\brief Returns a pointer to the root of this SharedDataTable
		@return The root of this SharedDataTable
		*/
		Scope* GetRoot() const;

		/*!	\brief Returns the name of the root object.
		@return The name of the root object.
		*/
		std::string GetRootName() const;

	private:
		// The stack used to store data temporarily, and then construct Datum objects once an end tag is found.
		Stack<HashMap<std::string, Datum>> mStack;

		// The root to be filled.
		Scope* mRoot;

		// The name of the root.
		std::string mRootName;

		// Constants for reserved keys and known tags/attributes.
		static const std::string mStringDataKey;
		static const std::string mDataTypeKey;
		static const std::string mParentTypeKey;
		static const std::string mNumArrayElementsKey;
		static const std::string mArrayElementsKey;
		static const std::string mIsScalarKey;
		static const int mReservedKeysLength = 6;
		static const std::string mReservedKeys[mReservedKeysLength];

		static const std::string mNameAttribute;
		static const std::string mSizeAttribute;
		static const std::string mClassAttribute;

		static const std::string mValueTag;
		static const std::string mIntegerTag;
		static const std::string mFloatTag;
		static const std::string mVectorTag;
		static const std::string mMatrixTag;
		static const std::string mStringTag;
		static const std::string mTableTag;
		static const std::string mEntityTag;
		static const std::string mSectorTag;
		static const std::string mWorldTag;
		static const std::string mActionTag;
	};

}