#pragma once

/*!	\file XmlParseHelperPrimitive.h
	\brief Contains the declaration for an XML parse helper that parses XML table (Scope) data.
*/

#include "pch.h"
#include "IXmlParseHelper.h"
#include "XmlParseMaster.h"
#include "SharedDataTable.h"
#include "HashMap.h"
#include "Scope.h"

namespace Library
{
	/*!	\brief XML parse helper for parsing tables (Scope), which checks to enforce grammar syntax rules.
	*/
	class XmlParseHelperPrimitive : public IXmlParseHelper
	{
	public:
		/*!	\brief Initializes this instance to a default state.
		*/
		XmlParseHelperPrimitive() = default;

		/*!	\brief Cleans up any owned data.
		*/
		virtual ~XmlParseHelperPrimitive() = default;

		/*!	\brief Handler for XML object start tags, called by the XmlParseMaster that is using this helper.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper. If no SharedData object is associated, the value is nullptr.
			@param name The name of the tag.
			@param atttributes A HashMap containing name/value pairs of attributes listed in the XML start tag.
			@throws std::runtime_error Thrown if no name is provided for the element.
			@throws std::runtime_error Thrown if a reserved term conflights with usage in the XML.
			@throws std::runtime_error Thrown if the size attributed is specified, and the value was invalid.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes) override;

		/*!	\brief Handler for XML object end tags, called by the XmlParseMaster that is using this helper. If no SharedData object is associated, the value is nullptr.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper.
			@param name The name of the tag.
			@throws std::runtime_error Thrown if an unknown XML tag is encountered.
			@throws std::runtime_error Thrown if the depth is invalid for a given tag.
			@throws std::runtime_error Thrown if a given tag's parent tag is invalid.
			@throws std::runtime_error Thrown if the root is detected to be an array of tables.
			@throws std::runtime_error Thrown if ambiguity between scalar and array is detected.
			@throws std::runtime_error Thrown if no data is provided for a type that requires data.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name) override;

		/*!	\brief Handler for XML object character data, called by the XmlParseMaster that is using this helper. If no SharedData object is associated, the value is nullptr.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper.
			@param data A null-terminated C-string of data to be handled.
			@param length The length of data.
			@throws std::runtime_error Thrown if ambiguity between scalar and array is detected.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool CharElementHandler(SharedDataTable::SharedData* sharedData, const char* data, int length) override;

		/*!	\brief Generates a deep copy of this XmlParseHelperPrimitive on the heap, prepares it for usage, and returns a pointer to it.

			User is responsible for calling delete on the returned value once finished with it, unless it is owned by a cloned XmlParseMaster.
			@return Address of a new parse helper.
		*/
		virtual IXmlParseHelper* Clone() const override;

	private:
		// Helper function for EndElementHandler(). Parses value tags.
		void ParseValue(SharedDataTable* shared, std::string name);

		// Helper function for EndElementHandler(). Parses raw data type tags.
		void ParsePrimitive(SharedDataTable* shared, std::string name, Datum::DatumType type);
	};

}
