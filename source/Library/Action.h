#pragma once

/*!	\file Action.h
	\brief Contains the declaration for the base class of all Actions.
*/

#include "Attributed.h"
#include "WorldState.h"

namespace Library
{
	/*!	\brief The base class of all Actions.
	*/
	class Action : public Attributed
	{
		RTTI_DECLARATIONS(Action, Attributed);

	public:
		/*!	\brief Constructs a new Action to default values, and uses the provided name.
			@param name The name to give the new Action.
		*/
		Action(const std::string& name = "Action");

		/*!	\brief Performs a deep copy of the argument.
			@param rhs The Action to copy.
		*/
		Action(const Action& rhs);

		/*!	\brief Destroys any owned data.
		*/
		virtual ~Action() = default;

		/*!	\brief Performs a deep copy of the argument
			@param rhs The Action to copy.
			@return A reference to this Action, post-copy.
		*/
		Action& operator=(const Action& rhs);

		/*!	\brief Initializes prescribed members.
		*/
		void Populate();

		/*!	\brief Returns the name of this Action
			@return The name of this Action.
		*/
		const std::string& GetName() const;

		/*!	\brief Sets the name of this Action
			@param name The name to set this Action's name to.
		*/
		void SetName(const std::string& name);

		/*!	\brief Retrieves a GUID string, which is only unique at the precision of the highest point in this object's hierarchy.
			@return A GUID string.
		*/
		std::string GetGUID() const;

		/*!	\brief Returns a pointer to the object this Action is contained in.
			@return The object that contains this Action.
		*/
		Scope* GetOwner() const;

		/*!	\brief Sets the object this Action is contained in.
			@param newOwner The object to contain this Action in. Must be a World, Sector, Entity, or ActionList.
		*/
		void SetOwner(Scope* newOwner);

		/*!	\brief Sets the active state of this object, based on the parameter.
			@param The active state to set this object to.
		*/
		void SetActive(int state);

		/*!	\brief Gets the active state of this object, and returns it.
			@return The active state of this object.
		*/
		int IsActive();

		/*!	\brief To be called on every frame. Performs a full Action update.
			@param state The game state, which is to be considered every update.
		*/
		virtual void Update(WorldState& state) = 0;

	protected:
		std::string mName;
		std::uint32_t mLocalID;
		Scope* mOwner;
	};

#define ActionFactory(ConcreteActionT) ConcreteFactory(ConcreteActionT, Action)

}