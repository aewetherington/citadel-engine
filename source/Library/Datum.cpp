#include "pch.h"
#include "Datum.h"
#include "Scope.h"

namespace Library {

#pragma region Tors
	Datum::Datum()
		: mType(DatumType::Unknown), mData(), mSize(0), mCapacity(0), mIsExternal(false)
	{

	}

	Datum::Datum(const Datum& rhs)
		: Datum()
	{
		operator=(rhs);
	}

	Datum::Datum(Datum&& rhs)
		: mType(rhs.mType), mData(rhs.mData), mSize(rhs.mSize), mCapacity(rhs.mCapacity), mIsExternal(rhs.mIsExternal)
	{
		rhs.mType = DatumType::Unknown;
		rhs.mData = DatumValue();
		rhs.mSize = 0;
		rhs.mCapacity = 0;
		rhs.mIsExternal = false;
	}

	Datum::Datum(DatumType type, std::uint32_t size)
		: mType(type), mData(), mSize(0), mCapacity(0), mIsExternal(false)
	{
		SetSize(size);
	}

	Datum::~Datum()
	{
		Clear();
	}

#pragma endregion

#pragma region Assignment
	Datum& Datum::operator=(const Datum& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		Clear();


		mType = rhs.mType;

		if (rhs.IsExternal())
		{
			mData.vp = rhs.mData.vp;
			mIsExternal = true;
			mSize = rhs.mSize;
			mCapacity = rhs.mCapacity;
		}
		else
		{
			// Only allocate just enough to fit all the stuff to copy, if there's anything at all.
			if (rhs.mSize > 0)
			{
				SetSize(rhs.mSize);
				Datum::sCopyFuncts[static_cast<int32_t>(rhs.mType)](*this, rhs.mData, rhs.mSize);
				mIsExternal = false;
				mSize = rhs.mSize;
				mCapacity = rhs.mSize;
			}
		}

		return *this;
	}

	Datum& Datum::operator=(Datum&& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		Clear();

		mType = rhs.mType;
		mData.vp = rhs.mData.vp;
		mSize = rhs.mSize;
		mCapacity = rhs.mCapacity;
		mIsExternal = rhs.mIsExternal;

		rhs.mType = DatumType::Unknown;
		rhs.mData = DatumValue();
		rhs.mSize = 0;
		rhs.mCapacity = 0;
		rhs.mIsExternal = false;

		return *this;
	}

	Datum& Datum::operator=(const std::int32_t& rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Integer)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}

		mType = DatumType::Integer;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}

	Datum& Datum::operator=(const float& rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Float)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}

		mType = DatumType::Float;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}

	Datum& Datum::operator=(const glm::vec4& rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Vector)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}

		mType = DatumType::Vector;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}

	Datum& Datum::operator=(const glm::mat4& rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Matrix)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}
		mType = DatumType::Matrix;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}

	Datum& Datum::operator=(const std::string& rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::String)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}
		mType = DatumType::String;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}

	Datum& Datum::operator=(Scope* rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Table)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}
		mType = DatumType::Table;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}

	Datum& Datum::operator=(RTTI* rhs)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Pointer)
		{
			throw std::runtime_error("Type mismatch on assignment.");
		}
		mType = DatumType::Pointer;

		SetSize(1);

		Set(rhs, 0);

		return *this;
	}
#pragma endregion

#pragma region SetStorage
	void Datum::SetStorage(std::int32_t* dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::Integer)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::Integer;

		mData.i = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(float* dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::Float)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::Float;

		mData.f = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(glm::vec4* dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::Vector)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::Vector;

		mData.v = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(glm::mat4* dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::Matrix)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::Matrix;

		mData.m = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(std::string* dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::String)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::String;

		mData.s = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(Scope** dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::Table)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::Table;

		mData.t = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(RTTI** dataArray, std::uint32_t size)
	{
		Clear();

		if (mType != DatumType::Unknown && mType != DatumType::Pointer)
		{
			throw std::runtime_error("Type mismatch on SetStorage.");
		}

		mType = DatumType::Pointer;

		mData.r = dataArray;
		mSize = size;
		mCapacity = size;
		mIsExternal = true;
	}

	void Datum::SetStorage(void* dataArray, DatumType dataType, std::uint32_t size)
	{
		switch (dataType)
		{
		case DatumType::Integer:
			SetStorage(reinterpret_cast<int*>(dataArray), size);
			break;
		case DatumType::Float:
			SetStorage(reinterpret_cast<float*>(dataArray), size);
			break;
		case DatumType::Vector:
			SetStorage(reinterpret_cast<glm::vec4*>(dataArray), size);
			break;
		case DatumType::Matrix:
			SetStorage(reinterpret_cast<glm::mat4*>(dataArray), size);
			break;
		case DatumType::String:
			SetStorage(reinterpret_cast<std::string*>(dataArray), size);
			break;
		case DatumType::Table:
			SetStorage(reinterpret_cast<Scope**>(dataArray), size);
			break;
		case DatumType::Pointer:
			SetStorage(reinterpret_cast<RTTI**>(dataArray), size);
			break;
		default:
			throw std::runtime_error("Invalid DatumType provided to generic SetStorage.");
			break;
		}
	}
#pragma endregion

#pragma region Compare
	bool Datum::operator==(const Datum& rhs) const
	{
		return mType == rhs.mType && mSize == rhs.mSize && Datum::sCompareFuncts[static_cast<int32_t>(mType)](*this, rhs.mData, rhs.mSize);
	}

	bool Datum::operator!=(const Datum& rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const std::int32_t& rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::Integer)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return mData.i[0] == rhs;
	}

	bool Datum::operator!=(const std::int32_t& rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const float& rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::Float)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return mData.f[0] == rhs;
	}

	bool Datum::operator!=(const float& rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const glm::vec4& rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::Vector)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return mData.v[0] == rhs;
	}

	bool Datum::operator!=(const glm::vec4& rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const glm::mat4& rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::Matrix)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return mData.m[0] == rhs;
	}

	bool Datum::operator!=(const glm::mat4& rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const std::string& rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::String)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return mData.s[0] == rhs;
	}

	bool Datum::operator!=(const std::string& rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const Scope* rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::Table)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return *(mData.t[0]) == *rhs;
	}

	bool Datum::operator!=(const Scope* rhs) const
	{
		return !operator==(rhs);
	}

	bool Datum::operator==(const RTTI* rhs) const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Cannot compare when Datum is empty.");
		}

		if (mType != DatumType::Pointer)
		{
			throw std::runtime_error("Datum type does not match the compared object type.");
		}

		return mData.r[0] == rhs;
	}

	bool Datum::operator!=(const RTTI* rhs) const
	{
		return !operator==(rhs);
	}
#pragma endregion

#pragma region Set
	void Datum::Set(const std::int32_t& data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Integer)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::Integer;

		if (index >= mSize)
		{
			SetSize(index+1);
		}

		mData.i[index] = data;
	}

	void Datum::Set(const float& data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Float)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::Float;

		if (index >= mSize)
		{
			SetSize(index + 1);
		}

		mData.f[index] = data;
	}

	void Datum::Set(const glm::vec4& data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Vector)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::Vector;

		if (index >= mSize)
		{
			SetSize(index + 1);
		}

		mData.v[index] = data;
	}

	void Datum::Set(const glm::mat4& data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Matrix)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::Matrix;

		if (index >= mSize)
		{
			SetSize(index + 1);
		}

		mData.m[index] = data;
	}

	void Datum::Set(const std::string& data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::String)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::String;

		if (index >= mSize)
		{
			SetSize(index + 1);
		}

		mData.s[index] = data;
	}

	void Datum::Set(Scope* data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Table)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::Table;

		if (index >= mSize)
		{
			SetSize(index + 1);
		}

		mData.t[index] = data;
	}

	void Datum::Set(RTTI* data, std::uint32_t index)
	{
		if (mType != DatumType::Unknown && mType != DatumType::Pointer)
		{
			throw std::runtime_error("Type mismatch on Set.");
		}

		mType = DatumType::Pointer;

		if (index >= mSize)
		{
			SetSize(index + 1);
		}

		mData.r[index] = data;
	}
#pragma endregion

#pragma region Get
	template <>	std::int32_t& Datum::Get<std::int32_t>(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.i[index];
	}

	template <> float& Datum::Get<float>(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.f[index];
	}

	template <> glm::vec4& Datum::Get<glm::vec4>(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.v[index];
	}

	template <> glm::mat4& Datum::Get<glm::mat4>(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.m[index];
	}

	template <> std::string& Datum::Get<std::string>(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.s[index];
	}

	Scope* Datum::GetTable(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.t[index];
	}

	RTTI* Datum::GetPointer(std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return mData.r[index];
	}
#pragma endregion

#pragma region DatumString
	void Datum::SetFromString(const std::string& data, std::uint32_t index)
	{
		if (mType == Datum::DatumType::Unknown)
		{
			throw std::runtime_error("Datum type must be set before string can be parsed.");
		}

		if (0 == Datum::sFromStringFuncts[static_cast<int32_t>(mType)](*this, data, index))
		{
			throw std::runtime_error("Failed to parse data from the string argument.");
		}
	}

	std::string Datum::ToString(std::uint32_t index) const
	{
		if (mType == Datum::DatumType::Unknown)
		{
			throw std::runtime_error("Datum type must be set before string can be parsed.");
		}

		if (index >= mSize)
		{
			throw std::runtime_error("Index out of range.");
		}

		return Datum::sToStringFuncts[static_cast<int32_t>(mType)](*this, index);
	}
#pragma endregion

#pragma region Bookkeep
	Datum::DatumType Datum::GetType() const
	{
		return mType;
	}

	void Datum::SetType(DatumType type)
	{
		if (type == DatumType::Unknown || type == DatumType::MAX)
		{
			throw std::runtime_error("Invalid type.");
		}
		else
		{
			mType = type;
		}
	}

	std::uint32_t Datum::GetSize() const
	{
		return mSize;
	}

	void Datum::SetSize(std::uint32_t size)
	{
		if (mType == DatumType::Unknown)
		{
			throw std::runtime_error("Cannot set the size of a Datum with an unknown type.");
		}

		if (mIsExternal)
		{
			if (size > mCapacity)
			{
				throw std::runtime_error("Cannot change the capacity of a datum with external storage.");
			}
			else
			{
				// Soft shrink/grow the Datum.
				mSize = size;
			}
		}
		else
		{
			if (size > mCapacity)
			{
				// First, save some temporary information on the old array.
				std::uint32_t oldSize = mSize;
				DatumValue oldData;
				oldData.vp = mData.vp;
				
				// We're now free to update the size and capacity.
				mSize = size;

				// Create the new mData array, generically.
				Datum::sAllocFuncts[static_cast<int32_t>(mType)](*this, mSize);
				mCapacity = mSize;

				// Copy the contents of the old array.
				Datum::sCopyFuncts[static_cast<int32_t>(mType)](*this, oldData, oldSize);

				// Temporarily store the new pointer, and generically delete the old data.
				DatumValue tmpData;
				tmpData.vp = mData.vp;
				mData.vp = oldData.vp;
				Datum::sDeallocFuncts[static_cast<int32_t>(mType)](*this);

				// Restore the pointer to the new data.
				mData.vp = tmpData.vp;
			}
			else
			{
				// Soft shrink/grow the Datum.
				mSize = size;
			}
		}
	}

	bool Datum::IsExternal() const
	{
		return mIsExternal;
	}

	void Datum::Clear()
	{
		if (!mIsExternal && mType != DatumType::Unknown)
		{
			Datum::sDeallocFuncts[static_cast<int32_t>(mType)](*this);
		}

		mData = DatumValue();
		mSize = 0;
		mCapacity = 0;
		mIsExternal = false;
	}
#pragma endregion

#pragma region FunctPtrs
	const Datum::AllocFunction Datum::sAllocFuncts[] = {
		AllocUnknown,
		AllocInteger,
		AllocFloat,
		AllocVector,
		AllocMatrix,
		AllocString,
		AllocTable,
		AllocPointer
	};

	const Datum::DeallocFunction Datum::sDeallocFuncts[] = {
		DeallocUnknown,
		DeallocInteger,
		DeallocFloat,
		DeallocVector,
		DeallocMatrix,
		DeallocString,
		DeallocTable,
		DeallocPointer
	};

	const Datum::CopyFunction Datum::sCopyFuncts[] = {
		CopyUnknown,
		CopyInteger,
		CopyFloat,
		CopyVector,
		CopyMatrix,
		CopyString,
		CopyTable,
		CopyPointer
	};

	const Datum::CompareFunction Datum::sCompareFuncts[] = {
		CompareUnknown,
		CompareInteger,
		CompareFloat,
		CompareVector,
		CompareMatrix,
		CompareString,
		CompareTable,
		ComparePointer
	};

	const Datum::FromStringFunction Datum::sFromStringFuncts[] = {
		FromStringUnknown,
		FromStringInteger,
		FromStringFloat,
		FromStringVector,
		FromStringMatrix,
		FromStringString,
		FromStringTable,
		FromStringPointer
	};

	const Datum::ToStringFunction Datum::sToStringFuncts[] = {
		ToStringUnknown,
		ToStringInteger,
		ToStringFloat,
		ToStringVector,
		ToStringMatrix,
		ToStringString,
		ToStringTable,
		ToStringPointer
	};
#pragma endregion

#pragma region AllocFuncts
	void Datum::AllocUnknown(Datum& d, std::uint32_t size)
	{
		throw std::runtime_error("Allocation attempted on a Datum with unknown type.");
	}

	void Datum::AllocInteger(Datum& d, std::uint32_t size)
	{
		d.mData.i = new std::int32_t[size];
		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.i[index] = 0;
		}
	}

	void Datum::AllocFloat(Datum& d, std::uint32_t size)
	{
		d.mData.f = new float[size];
		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.f[index] = 0.0f;
		}
	}

	void Datum::AllocVector(Datum& d, std::uint32_t size)
	{
		d.mData.v = new glm::vec4[size];
		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.v[index] = glm::vec4(0.0f);
		}
	}

	void Datum::AllocMatrix(Datum& d, std::uint32_t size)
	{
		d.mData.m = new glm::mat4[size];
		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.m[index] = glm::mat4(0.0f);
		}
	}

	void Datum::AllocString(Datum& d, std::uint32_t size)
	{
		d.mData.s = new std::string[size];
	}

	void Datum::AllocTable(Datum& d, std::uint32_t size)
	{
		d.mData.t = new Scope*[size];
		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.t[index] = nullptr;
		}
	}

	void Datum::AllocPointer(Datum& d, std::uint32_t size)
	{
		d.mData.r = new RTTI*[size];
		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.r[index] = nullptr;
		}
	}
#pragma endregion

#pragma region DeallocFuncts
	void Datum::DeallocUnknown(Datum& d)
	{
		throw std::runtime_error("Deallocation attempted on a Datum with unknown type.");
	}

	void Datum::DeallocInteger(Datum& d)
	{
		delete[] d.mData.i;
	}

	void Datum::DeallocFloat(Datum& d)
	{
		delete[] d.mData.f;
	}

	void Datum::DeallocVector(Datum& d)
	{
		delete[] d.mData.v;
	}

	void Datum::DeallocMatrix(Datum& d)
	{
		delete[] d.mData.m;
	}

	void Datum::DeallocString(Datum& d)
	{
		delete[] d.mData.s;
	}

	void Datum::DeallocTable(Datum& d)
	{
		delete[] d.mData.t;
	}

	void Datum::DeallocPointer(Datum& d)
	{
		delete[] d.mData.r;
	}
#pragma endregion

#pragma region CopyFuncts
	void Datum::CopyUnknown(Datum& d, DatumValue data, std::uint32_t size)
	{
		throw std::runtime_error("Copy attempted on a Datum with unknown type.");
	}

	void Datum::CopyInteger(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.i == data.i)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.i[index] = data.i[index];
		}
	}

	void Datum::CopyFloat(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.f == data.f)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.f[index] = data.f[index];
		}
	}

	void Datum::CopyVector(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.v == data.v)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.v[index] = data.v[index];
		}
	}

	void Datum::CopyMatrix(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.m == data.m)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.m[index] = data.m[index];
		}
	}

	void Datum::CopyString(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.s == data.s)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.s[index] = data.s[index];
		}
	}

	void Datum::CopyTable(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.t == data.t)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.t[index] = data.t[index];
		}
	}

	void Datum::CopyPointer(Datum& d, DatumValue data, std::uint32_t size)
	{
		if (d.mData.r == data.r)
		{
			return;
		}

		for (std::uint32_t index = 0; index < size; ++index)
		{
			d.mData.r[index] = data.r[index];
		}
	}
#pragma endregion

#pragma region CompareFuncts
	bool Datum::CompareUnknown(const Datum& d, DatumValue data, std::uint32_t size)
	{
		return true;
	}

	bool Datum::CompareInteger(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (d.mData.i[index] != data.i[index])
			{
				return false;
			}
		}

		return true;
	}

	bool Datum::CompareFloat(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (d.mData.f[index] != data.f[index])
			{
				return false;
			}
		}

		return true;
	}

	bool Datum::CompareVector(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (d.mData.v[index] != data.v[index])
			{
				return false;
			}
		}

		return true;
	}

	bool Datum::CompareMatrix(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (d.mData.m[index] != data.m[index])
			{
				return false;
			}
		}

		return true;
	}

	bool Datum::CompareString(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (d.mData.s[index] != data.s[index])
			{
				return false;
			}
		}

		return true;
	}

	bool Datum::CompareTable(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (*(d.mData.t[index]) != *(data.t[index]))
			{
				return false;
			}
		}

		return true;
	}

	bool Datum::ComparePointer(const Datum& d, DatumValue data, std::uint32_t size)
	{
		for (std::uint32_t index = 0; index < size; ++index)
		{
			if (d.mData.r[index] != data.r[index])
			{
				return false;
			}
		}

		return true;
	}
#pragma endregion

#pragma region FromStringFuncts
	std::int32_t Datum::FromStringUnknown(Datum& d, const std::string& str, std::uint32_t index)
	{
		throw std::runtime_error("Parsing data from a string attempted on a Datum with unknown type.");
	}

	std::int32_t Datum::FromStringInteger(Datum& d, const std::string& str, std::uint32_t index)
	{
		std::int32_t tmp;
		std::int32_t result = sscanf_s(str.c_str(), "%d", &tmp);
		if (result == 1)
		{
			d.Set(tmp, index);
		}

		return result;
	}

	std::int32_t Datum::FromStringFloat(Datum& d, const std::string& str, std::uint32_t index)
	{
		float tmp;
		std::int32_t result = sscanf_s(str.c_str(), "%f", &tmp);
		if (result == 1)
		{
			d.Set(tmp, index);
		}

		return result;
	}

	std::int32_t Datum::FromStringVector(Datum& d, const std::string& str, std::uint32_t index)
	{
		glm::vec4 tmp;
		std::int32_t result = sscanf_s(str.c_str(), "(%f,%f,%f,%f)", &(tmp.x), &(tmp.y), &(tmp.z), &(tmp.w));

		// If we didn't get at least the first three filled, return a failure.
		if (result < 3)
		{
			return 0;
		}
		else
		{
			// Default value for w is 1 (defines a point).
			if (result == 3)
			{
				tmp.w = 1.0f;
			}

			d.Set(tmp, index);
			return 1;
		}
	}

	std::int32_t Datum::FromStringMatrix(Datum& d, const std::string& str, std::uint32_t index)
	{
		glm::mat4 tmp;
		std::int32_t result = sscanf_s(str.c_str(), "[%f%f%f%f,%f%f%f%f,%f%f%f%f,%f%f%f%f]",
			&(tmp[0].x), &(tmp[0].y), &(tmp[0].z), &(tmp[0].w),
			&(tmp[1].x), &(tmp[1].y), &(tmp[1].z), &(tmp[1].w),
			&(tmp[2].x), &(tmp[2].y), &(tmp[2].z), &(tmp[2].w),
			&(tmp[3].x), &(tmp[3].y), &(tmp[3].z), &(tmp[3].w));

		// If we didn't get all sixteen filled, return a failure.
		if (result != 16)
		{
			return 0;
		}
		else
		{
			d.Set(tmp, index);
			return 1;
		}
	}

	std::int32_t Datum::FromStringString(Datum& d, const std::string& str, std::uint32_t index)
	{
		d.Set(str, index);
		return 1;
	}

	std::int32_t Datum::FromStringTable(Datum& d, const std::string& str, std::uint32_t index)
	{
		// TODO
		return 0;
	}

	std::int32_t Datum::FromStringPointer(Datum& d, const std::string& str, std::uint32_t index)
	{
		// TODO
		return 0;
	}
#pragma endregion

#pragma region ToStringFuncts
	std::string Datum::ToStringUnknown(const Datum& d, std::uint32_t index)
	{
		throw std::runtime_error("Formatting data to a string attempted on a Datum with unknown type.");
	}

	std::string Datum::ToStringInteger(const Datum& d, std::uint32_t index)
	{
		return std::to_string(d.Get<std::int32_t>(index));
	}

	std::string Datum::ToStringFloat(const Datum& d, std::uint32_t index)
	{
		return std::to_string(d.Get<float>(index));
	}

	std::string Datum::ToStringVector(const Datum& d, std::uint32_t index)
	{
		glm::vec4 tmp = d.Get<glm::vec4>(index);

		return "(" + std::to_string(tmp.x) + " " + std::to_string(tmp.y) + " " + std::to_string(tmp.z) + " " + std::to_string(tmp.w) + ")";
	}

	std::string Datum::ToStringMatrix(const Datum& d, std::uint32_t index)
	{
		glm::mat4 tmp = d.Get<glm::mat4>(index);

		return	"[" + std::to_string(tmp[0].x) + " " + std::to_string(tmp[0].y) + " " + std::to_string(tmp[0].z) + " " + std::to_string(tmp[0].w) + "," +
				" " + std::to_string(tmp[1].x) + " " + std::to_string(tmp[1].y) + " " + std::to_string(tmp[1].z) + " " + std::to_string(tmp[1].w) + "," +
				" " + std::to_string(tmp[2].x) + " " + std::to_string(tmp[2].y) + " " + std::to_string(tmp[2].z) + " " + std::to_string(tmp[2].w) + "," +
				" " + std::to_string(tmp[3].x) + " " + std::to_string(tmp[3].y) + " " + std::to_string(tmp[3].z) + " " + std::to_string(tmp[3].w) + "]";
	}

	std::string Datum::ToStringString(const Datum& d, std::uint32_t index)
	{
		return d.Get<std::string>(index);
	}

	std::string Datum::ToStringTable(const Datum& d, std::uint32_t index)
	{
		// Placeholder for if support for serializing Scope objects is added later.
		char buf[25];
		Scope* tmp = d.GetTable(index);
		sprintf_s(buf, 25, "Table: 0x%x", tmp);

		return std::string(buf);
	}

	std::string Datum::ToStringPointer(const Datum& d, std::uint32_t index)
	{
		// Placeholder for if support for serializing RTTI objects is added later.
		char buf[25];
		RTTI* tmp = d.GetPointer(index);
		sprintf_s(buf, 25, "RTTI: 0x%x", tmp);

		return std::string(buf);
	}
#pragma endregion

}