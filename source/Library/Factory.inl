namespace Library
{

	template <typename AbstractProductT>
	HashMap<std::string, Factory<AbstractProductT>*> Factory<AbstractProductT>::sFactoryTable;

	template <typename AbstractProductT>
	Factory<AbstractProductT>* Factory<AbstractProductT>::Find(const std::string& className)
	{
		Factory<AbstractProductT>* foundFactory = nullptr;

		auto findings = sFactoryTable.Find(className);
		if (findings != sFactoryTable.end())
		{
			foundFactory = findings->second;
		}

		return foundFactory;
	}

	template <typename AbstractProductT>
	AbstractProductT* Factory<AbstractProductT>::Create(const std::string& className)
	{
		AbstractProductT* madeProduct = nullptr;

		auto findings = sFactoryTable.Find(className);
		if (findings != sFactoryTable.end())
		{
			madeProduct = findings->second->Create();
		}

		return madeProduct;
	}

	template <typename AbstractProductT>
	typename HashMap<std::string, Factory<AbstractProductT>*>::Iterator Factory<AbstractProductT>::begin()
	{
		return sFactoryTable.begin();
	}

	template <typename AbstractProductT>
	typename HashMap<std::string, Factory<AbstractProductT>*>::Iterator Factory<AbstractProductT>::end()
	{
		return sFactoryTable.end();
	}

	template <typename AbstractProductT>
	void Factory<AbstractProductT>::Add(Factory<AbstractProductT>* factory)
	{
		if (factory == nullptr)
		{
			return;
		}

		sFactoryTable.Insert(factory->GetClassName(), factory);
	}

	template <typename AbstractProductT>
	void Factory<AbstractProductT>::Remove(Factory<AbstractProductT>* factory)
	{
		if (factory == nullptr)
		{
			return;
		}

		sFactoryTable.Remove(factory->GetClassName());
	}

}