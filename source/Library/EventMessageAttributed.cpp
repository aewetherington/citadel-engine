#include "pch.h"
#include "EventMessageAttributed.h"

namespace Library
{
	RTTI_DEFINITIONS(EventMessageAttributed);

	EventMessageAttributed::EventMessageAttributed()
		: Attributed(), mSubtype(), mWorld(nullptr)
	{
		Populate();
	}

	EventMessageAttributed::EventMessageAttributed(const EventMessageAttributed& rhs)
		: Attributed(rhs), mSubtype(rhs.mSubtype), mWorld(rhs.mWorld)
	{
		(*this)[sSubtypeTag].SetStorage(&mSubtype, 1);
	}

	EventMessageAttributed& EventMessageAttributed::operator=(const EventMessageAttributed& rhs)
	{
		Attributed::operator=(rhs);

		mSubtype = rhs.mSubtype;
		mWorld = rhs.mWorld;

		(*this)[sSubtypeTag].SetStorage(&mSubtype, 1);

		return *this;
	}

	void EventMessageAttributed::Populate()
	{
		INIT_SIGNATURE();

		EXTERNAL_ATTRIBUTE(sSubtypeTag, Datum::DatumType::String, &mSubtype, 1);

		Attributed::Populate();
	}

	void EventMessageAttributed::SetSubtype(const std::string& subtype)
	{
		mSubtype = subtype;
	}

	const std::string& EventMessageAttributed::GetSubtype() const
	{
		return mSubtype;
	}

	void EventMessageAttributed::SetWorld(World* world)
	{
		mWorld = world;
	}

	World* EventMessageAttributed::GetWorld() const
	{
		return mWorld;
	}

	const std::string EventMessageAttributed::sSubtypeTag = "subtype";

}