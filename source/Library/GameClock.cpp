#include "pch.h"
#include "GameClock.h"
#include <chrono>

using namespace std::chrono;

namespace Library
{

	GameClock::GameClock()
		: mStartTime(), mCurrentTime(), mLastTime()
	{

	}

	const high_resolution_clock::time_point& GameClock::GetStartTime() const
	{
		return mStartTime;
	}

	const high_resolution_clock::time_point& GameClock::GetCurrentTime() const
	{
		return mCurrentTime;
	}

	const high_resolution_clock::time_point& GameClock::GetLastTime() const
	{
		return mLastTime;
	}

	void GameClock::Reset()
	{
		mStartTime = high_resolution_clock::time_point();

		mCurrentTime = mStartTime;
		mLastTime = mStartTime;
	}

	void GameClock::UpdateGameTime(GameTime& gameTime)
	{
		mLastTime = mCurrentTime;
		mCurrentTime = high_resolution_clock::time_point();

		gameTime.SetCurrentTime(mCurrentTime);
		gameTime.SetTotalGameTime(duration_cast<milliseconds>(mCurrentTime - mStartTime));
		gameTime.SetElapsedGameTime(duration_cast<milliseconds>(mCurrentTime - mLastTime));
	}
}