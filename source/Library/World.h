#pragma once

/*!	\file World.h
	\brief Contains the declaration for World, which is primarily a container for Sectors.
*/

#include "Attributed.h"
#include "GameClock.h"
#include "Action.h"
#include "EventQueue.h"

namespace Library
{
	class Sector;
	class WorldState;

	/*!	\brief The root class in a game hierarchy that is primarily a container for Sector instances.
	*/
	class World final : public Attributed
	{
		RTTI_DECLARATIONS(World, Attributed);

	public:
		/*!	\brief Instantiates default values, and uses the specified name if provided.
			@param name The name to be used for this World.
		*/
		World(const std::string& name = "World");

		/*!	\brief Destructs data owned by this object
		*/
		virtual ~World() = default;

		/*!	\brief Initializes prescribed members.
		*/
		void Populate();

		/*!	\brief Returns a pointer to the Scope that holds the Actions for this object.
		*/
		Scope* GetActions();

		/*!	\brief Creates a new Action based on the name and instance strings within this World.
			@param actionClassName The classname of the Action to be created
			@param actionInstanceName The name to give the Action
			@return A pointer to the newly created Action.
			@throws std::runtime_error Thrown when the Action fails to be created.
		*/
		Action* CreateAction(const std::string& actionClassName, const std::string& actionInstanceName);

		/*!	\brief Returns the name of this World.
			@return The name of this World.
		*/
		const std::string& GetName() const;

		/*!	\brief Sets the name of this world.
			@param name The name to set this World's name as.
		*/
		void SetName(const std::string& name);

		/*!	\brief Effectively identical to GetName(), but with a syntax parallel to GUID retrieval in Entity and Sector.
			@return This object's GUID
		*/
		std::string GetGUID() const;

		/*!	\brief Returns a pointer to the Scope that contains Sectors in this World
			@return A pointer to the Scope that contains Sectors in this World.
		*/
		Scope* GetSectors() const;

		/*!	\brief Creates a new Sector based on the provided name, and makes it part of this World.
			@param sectorName The name to give the new Sector.
			@return A pointer to the newly-created Sector.
		*/
		Sector* CreateSector(const std::string& sectorName);

		/*!	\brief Returns a reference to the world's event queue, so others can enqueue events.
			@return A reference to this world's event queue.
		*/
		EventQueue& GetEventQueue();

		/*!	\brief Updates the timestep data, and calls this function on all of the contained Sectors.
			@param state Game state data to be considered and updated per frame.
		*/
		void Update(WorldState& state);

		/*!	\brief Retrieves a pointer to the last WorldState used to update this world.
			@return A pointer to the last WorldState used in Update().
		*/
		WorldState* GetWorldState() const;

	private:
		EventQueue mEventQueue;
		std::string mName;
		Scope* mSectors;
		Scope* mActions;
		GameClock mClock;
		WorldState* mWorldState;
	};

}
