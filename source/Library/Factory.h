#pragma once

/*!	\file Factory.h
	\brief Contains the declaration for Factory<AbstractProductT>, which is a template for base classes of factory classes.
*/

#include "pch.h"

namespace Library
{

	/*!	\brief A template for base classes of factory classes.
		Used to create any object that has an accessible default constructor
		entirely by a string name matching the class.
	*/
	template <typename AbstractProductT>
	class Factory
	{

	public:

		/*!	\brief Returns the class name of an instance, as a string.
			@return The class name of an instance, as a string.
		*/
		virtual std::string GetClassName() const = 0;

		/*!	\brief Returns the factory associated with the name parameter, provided it exists.
			@param className The name of the class whose factory is to be searched for.
			@return A pointer to the found factory if it exists. If it does not exist, nullptr is returned.
		*/
		static Factory<AbstractProductT>* Find(const std::string& className);

		/*!	\brief Creates a new instance of an object associated with the provided name parameter, if there is an associated factory registered.
			@param className The name of the class which is intended to be created.
			@return A pointer to the newly created class, if a match is found for the name parameter. If no match is found, nullptr is returned.
		*/
		static AbstractProductT* Create(const std::string& className);

		/*!	\brief Returns an iterator to the beginning of the HashMap of factories.
			@return An iterator to the beginning of the HashMap of factories.
		*/
		typename static HashMap<std::string, Factory<AbstractProductT>*>::Iterator begin();

		/*!	\brief Returns an iterator to the end of the HashMap of factories.
			@return An iterator to the end of the HashMap of factories.
		*/
		typename static HashMap<std::string, Factory<AbstractProductT>*>::Iterator end();

	protected:

		/*! \brief Creates a new instance of the concrete product.
			@return A pointer to the new instance of the concrete product, as a pointer to the abstract product.
		*/
		virtual AbstractProductT* Create() = 0;

		/*! \brief Registers a factory.
			@param factory The factory to be registered.
		*/
		static void Add(Factory<AbstractProductT>* factory);

		/*! \brief Removes the registration of a factory.
			@param factory The factory whose registration is to be removed.
		*/
		static void Remove(Factory<AbstractProductT>* factory);

	private:

		// The table of factories, which is managed through calls to Add() and Remove().
		static HashMap<std::string, Factory<AbstractProductT>*> sFactoryTable;

	};

#define ConcreteFactory(ConcreteProductT, AbstractProductT)								\
	class ConcreteProductT##Factory final : public Factory<AbstractProductT>			\
	{																					\
	public:																				\
		ConcreteProductT##Factory() { Add(this); }										\
		~ConcreteProductT##Factory() { Remove(this); }									\
		virtual std::string GetClassName() const override{ return #ConcreteProductT; }	\
		virtual AbstractProductT* Create() override										\
		{																				\
			AbstractProductT* product = new ConcreteProductT();							\
			assert(product != nullptr);													\
			return product;																\
		}																				\
	};

}

#include "Factory.inl"