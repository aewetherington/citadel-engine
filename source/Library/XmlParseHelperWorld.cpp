#include "pch.h"
#include "XmlParseHelperWorld.h"
#include "World.h"
#include "Sector.h"
#include "Scope.h"

namespace Library
{
	IXmlParseHelper* XmlParseHelperWorld::Clone() const
	{
		IXmlParseHelper* newHelper = new XmlParseHelperWorld();

		return newHelper;
	}

#pragma region Handlers

	bool XmlParseHelperWorld::StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mDataTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mWorldTag ||
			(name == SharedDataTable::mValueTag && parentType == SharedDataTable::mWorldTag)))
		{
			return false;
		}

		// Ensure this object at least has a name, assuming it's not a value tag.
		if (name != SharedDataTable::mValueTag && !attributes.ContainsKey(SharedDataTable::mNameAttribute))
		{
			throw std::runtime_error("New data types must have a name attribute. Offending XML Tag type: " + name);
		}

		// Ensure the user isn't trying to use a reserved name.
		for (auto str : SharedDataTable::mReservedKeys)
		{
			if (attributes.ContainsKey(str))
			{
				throw std::runtime_error("Attribute of type (" + name + ") attempted to use reserved name: " + str);
			}
		}

		if (sharedData->GetDepth() > 1)
		{
			// If this is a value tag, mark parent as an array by ensuring that key is contained in their hashmap.
			if (name == SharedDataTable::mValueTag)
			{
				shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			}
			// Otherwise, give it a scalar flag key.
			else
			{
				shared->mStack.Top()[SharedDataTable::mIsScalarKey];
			}
		}

		// Add object onto the stack.
		shared->mStack.Push(HashMap<std::string, Datum>());

		// Prepare the character data string buffer to be filled.
		shared->mStack.Top()[SharedDataTable::mStringDataKey] = std::string();

		// Set the type strings.
		shared->mStack.Top()[SharedDataTable::mDataTypeKey] = name;
		shared->mStack.Top()[SharedDataTable::mParentTypeKey] = parentType;

		// Add all attributes to the pile of data known, and retain their string form.
		for (auto& val : attributes)
		{
			Datum tmpDat;
			tmpDat = val.second;
			shared->mStack.Top().Insert(val.first, tmpDat);
		}

		// Preallocate an array, if specified.
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mSizeAttribute))
		{
			std::string sizeNumStr = shared->mStack.Top()[SharedDataTable::mSizeAttribute].Get<std::string>();

			std::uint32_t size = std::strtoul(sizeNumStr.c_str(), nullptr, 0);
			if (size == 0)
			{
				throw std::runtime_error("Invalid value in type (" + name + ") given for array size: " + sizeNumStr);
			}

			Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			valuesArray.SetType(Datum::DatumType::Table);
			valuesArray.SetSize(size);
			shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey] = 0;
		}

		return true;
	}

	bool XmlParseHelperWorld::EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mParentTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mWorldTag ||
			(name == SharedDataTable::mValueTag && parentType == SharedDataTable::mWorldTag)))
		{
			return false;
		}

		// Finish handling based on type.
		if (name == SharedDataTable::mValueTag)
		{
			ParseValue(shared, name);
		}
		else
		{
			ParseWorld(shared, name);
		}

		return true;
	}

#pragma endregion

#pragma region Helpers

	void XmlParseHelperWorld::ParseValue(SharedDataTable* shared, std::string name)
	{
		// REFACTOR : Current behavior. May support World arrays in the future easily by filling out this function.
		throw std::runtime_error("Worlds cannot be an array.");
	}

	void XmlParseHelperWorld::ParseWorld(SharedDataTable* shared, std::string name)
	{
		// Check for ambiguity on whether scalar or array.
		if (shared->mStack.Top()[SharedDataTable::mStringDataKey] != "" && shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Ambiguous declaration between scalar and array: " + name);
		}

		auto poppedMap = shared->mStack.Pop();

		// Check if the depth is OK
		if (shared->GetDepth() > 1)
		{
			throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " must be root.");
		}

		// This has to be the root. Ensure we can use the pointer.
		if (!shared->mRoot->Is("World"))
		{
			throw std::runtime_error("Root pointer is not a world.");
		}

		if (shared->mRoot == nullptr)
		{
			throw std::runtime_error("Root is null");
		}

		World* newWorld = shared->mRoot->As<World>();
		newWorld->SetName(poppedMap[SharedDataTable::mNameAttribute].Get<std::string>());

		// For all elements in this popped map.
		for (auto& it : poppedMap)
		{
			// Skip over this one if it's a reserved tag.
			bool isReservedTag = false;
			for (std::string str : SharedDataTable::mReservedKeys)
			{
				if (it.first == str)
				{
					isReservedTag = true;
				}
			}
			if (isReservedTag)
			{
				continue;
			}

			// Otherwise, throw the data into the scope object.
			if (it.second.GetType() == Datum::DatumType::Table)
			{
				// Have to manually do this for all tables in the data, in the event it's an array.
				for (std::uint32_t i = 0; i < it.second.GetSize(); ++i)
				{
					Scope* childTable = it.second.GetTable(i);
					if (childTable->Is("World") || childTable->Is("Entity"))
					{
						delete newWorld;
						throw std::runtime_error("Sectors cannot contain worlds or entities.");
					}
					else if (childTable->Is("Sector"))
					{
						childTable->As<Sector>()->SetWorld(newWorld);
					}
					else if (childTable->Is("Action"))
					{
						childTable->As<Action>()->SetOwner(newWorld);
					}
					else
					{
						// It's just a normal Scope.
						newWorld->Adopt(*childTable, it.first);
					}
				}
			}
			else
			{
				newWorld->Append(it.first) = it.second;
			}
		}
	}

#pragma endregion

}