#include "pch.h"
#include "ReactionAttributed.h"
#include "Event.h"
#include "EventMessageAttributed.h"
#include "World.h"

using namespace std;

namespace Library
{
	RTTI_DEFINITIONS(ReactionAttributed);

	ReactionAttributed::ReactionAttributed(const std::string& name)
		: Reaction(name)
	{
		Event<EventMessageAttributed>::Subscribe(*this); 

		Populate();
	}

	ReactionAttributed::~ReactionAttributed()
	{
		Event<EventMessageAttributed>::Unsubscribe(*this);
	}

	void ReactionAttributed::Populate()
	{
		ActionList::Populate();

		INIT_SIGNATURE();

		string subtypes;
		INTERNAL_ATTRIBUTE(sSubtypeReactionsTag, Datum::DatumType::String, &subtypes, 0);

		Attributed::Populate();
	}

	void ReactionAttributed::Notify(std::shared_ptr<EventPublisher> eventPublisher)
	{
		Reaction::Notify(eventPublisher);

		// If it's the right kind of event, potentially react.
		if (eventPublisher->As<Event<EventMessageAttributed>>() != nullptr)
		{
			const EventMessageAttributed& message = eventPublisher->As<Event<EventMessageAttributed>>()->GetMessage();
			
			string subtype = message.Find(EventMessageAttributed::sSubtypeTag)->Get<string>();
			Datum* subtypeReactions = Find(sSubtypeReactionsTag);
			for (uint32_t i = 0; i < subtypeReactions->GetSize(); ++i)
			{
				// If it's a known subtype, react to it.
				if (subtypeReactions->Get<string>(i) == subtype)
				{

					// Copy all auxiliary attributes into this
					for (uint32_t j = message.AuxiliaryBegin(); j < message.GetSize(); ++j)
					{
						AppendAuxiliaryAttribute(message.GetOrderedReference(j)->first) = message.GetOrderedReference(j)->second;
					}

					World* world = message.GetWorld();
					if (world != nullptr)
					{
						// Get the WorldState to update with.
						ActionList::Update(*(world->GetWorldState()));
					}
				}
			}
		}
	}

	const std::string ReactionAttributed::sSubtypeReactionsTag = "subtypeReactions";

}