#include "pch.h"
#include "XmlParseHelperSector.h"
#include "Sector.h"
#include "Entity.h"

namespace Library
{
	IXmlParseHelper* XmlParseHelperSector::Clone() const
	{
		IXmlParseHelper* newHelper = new XmlParseHelperSector();

		return newHelper;
	}

#pragma region Handlers

	bool XmlParseHelperSector::StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mDataTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mSectorTag ||
			(name == SharedDataTable::mValueTag && parentType == SharedDataTable::mSectorTag)))
		{
			return false;
		}

		// Ensure this object at least has a name, assuming it's not a value tag.
		if (name != SharedDataTable::mValueTag && !attributes.ContainsKey(SharedDataTable::mNameAttribute))
		{
			throw std::runtime_error("New data types must have a name attribute. Offending XML Tag type: " + name);
		}

		// Ensure the user isn't trying to use a reserved name.
		for (auto str : SharedDataTable::mReservedKeys)
		{
			if (attributes.ContainsKey(str))
			{
				throw std::runtime_error("Attribute of type (" + name + ") attempted to use reserved name: " + str);
			}
		}

		if (sharedData->GetDepth() > 1)
		{
			// If this is a value tag, mark parent as an array by ensuring that key is contained in their hashmap.
			if (name == SharedDataTable::mValueTag)
			{
				shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			}
			// Otherwise, give it a scalar flag key.
			else
			{
				shared->mStack.Top()[SharedDataTable::mIsScalarKey];
			}
		}

		// Add object onto the stack.
		shared->mStack.Push(HashMap<std::string, Datum>());

		// Prepare the character data string buffer to be filled.
		shared->mStack.Top()[SharedDataTable::mStringDataKey] = std::string();

		// Set the type strings.
		shared->mStack.Top()[SharedDataTable::mDataTypeKey] = name;
		shared->mStack.Top()[SharedDataTable::mParentTypeKey] = parentType;

		// Add all attributes to the pile of data known, and retain their string form.
		for (auto& val : attributes)
		{
			Datum tmpDat;
			tmpDat = val.second;
			shared->mStack.Top().Insert(val.first, tmpDat);
		}

		// Preallocate an array, if specified.
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mSizeAttribute))
		{
			std::string sizeNumStr = shared->mStack.Top()[SharedDataTable::mSizeAttribute].Get<std::string>();

			std::uint32_t size = std::strtoul(sizeNumStr.c_str(), nullptr, 0);
			if (size == 0)
			{
				throw std::runtime_error("Invalid value in type (" + name + ") given for array size: " + sizeNumStr);
			}

			Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			valuesArray.SetType(Datum::DatumType::Table);
			valuesArray.SetSize(size);
			shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey] = 0;
		}

		return true;
	}

	bool XmlParseHelperSector::EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mParentTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mSectorTag ||
			(name == SharedDataTable::mValueTag && parentType == SharedDataTable::mSectorTag)))
		{
			return false;
		}

		// Finish handling based on type.
		if (name == SharedDataTable::mValueTag)
		{
			ParseValue(shared, name);
		}
		else
		{
			ParseSector(shared, name);
		}

		return true;
	}

#pragma endregion

#pragma region Helpers

	void XmlParseHelperSector::ParseValue(SharedDataTable* shared, std::string name)
	{
		// Ensure that this is a potentially valid depth.
		if (shared->GetDepth() <= 1)
		{
			throw std::runtime_error("Invalid depth: " + name);
		}

		// Go ahead and pop it off.
		auto poppedMap = shared->mStack.Pop();

		// Check if the parent tag is invalid.
		if (shared->mStack.Top()[SharedDataTable::mDataTypeKey] == SharedDataTable::mValueTag)
		{
			throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " cannot have tags of type " + SharedDataTable::mValueTag + " as parents.");
		}

		// Add it to the parent's values array.
		Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
		std::int32_t index;
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mNumArrayElementsKey))
		{
			index = shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey].Get<int>()++;
		}
		else
		{
			index = shared->mStack.Top()[SharedDataTable::mArrayElementsKey].GetSize();
		}

		// Construct this scope based on its contents.
		Sector* newSector = new Sector();

		// For all elements in this popped map.
		for (auto& it : poppedMap)
		{
			// Skip over this one if it's a reserved tag.
			bool isReservedTag = false;
			for (std::string str : SharedDataTable::mReservedKeys)
			{
				if (it.first == str)
				{
					isReservedTag = true;
				}
			}
			if (isReservedTag)
			{
				continue;
			}

			// Otherwise, throw the data into the scope object.
			if (it.second.GetType() == Datum::DatumType::Table)
			{
				Scope* childTable = it.second.GetTable();
				if (childTable->Is("World") || childTable->Is("Sector"))
				{
					delete newSector;
					throw std::runtime_error("Sectors cannot contain worlds or sectors.");
				}
				else if (childTable->Is("Entity"))
				{
					childTable->As<Entity>()->SetSector(newSector);
				}
				else if (childTable->Is("Action"))
				{
					childTable->As<Action>()->SetOwner(newSector);
				}
				else
				{
					// It's just a normal Scope.
					newSector->Adopt(*childTable, it.first);
				}
			}
			else
			{
				newSector->Append(it.first) = it.second;
			}
		}

		// Now, place the scope into its proper place.
		valuesArray.Set(newSector, index);
	}

	void XmlParseHelperSector::ParseSector(SharedDataTable* shared, std::string name)
	{
		// Check for ambiguity on whether scalar or array.
		if (shared->mStack.Top()[SharedDataTable::mStringDataKey] != "" && shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Ambiguous declaration between scalar and array: " + name);
		}

		auto poppedMap = shared->mStack.Pop();

		// Check if the parent tag is invalid.
		if (shared->GetDepth() > 1)
		{
			if (shared->mStack.Top()[SharedDataTable::mDataTypeKey] != SharedDataTable::mWorldTag)
			{
				printf("\n%s + %d\n", poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>().c_str(), shared->mStack.GetSize());
				throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " can only have tags of type world as parents.");
			}
		}

		// Handle if array of sectors.
		if (poppedMap.ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			// Note that this array handler should only ever run if this is not the root.
			if (shared->GetDepth() <= 1)
			{
				throw std::runtime_error("Invalid depth for array of sectors.");
			}

			// Cache some data.
			Datum& datArray = poppedMap[SharedDataTable::mArrayElementsKey];
			auto& parent = shared->mStack.Top();
			std::string& datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();

			Datum& parentDatArray = parent[datName];

			for (std::uint32_t i = 0; i < datArray.GetSize(); ++i)
			{
				parentDatArray.Set(datArray.GetTable(i), i);
			}
		}
		// Handle if scalar
		else
		{
			// Check that the depth is OK.
			if (shared->GetDepth() <= 1)
			{
				throw std::runtime_error("Invalid depth for sector tag.");
			}

			// Cache some data.
			std::string& datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();

			Sector* newSector = new Sector(datName);

			// For all elements in this popped map.
			for (auto& it : poppedMap)
			{
				// Skip over this one if it's a reserved tag.
				bool isReservedTag = false;
				for (std::string str : SharedDataTable::mReservedKeys)
				{
					if (it.first == str)
					{
						isReservedTag = true;
					}
				}
				if (isReservedTag)
				{
					continue;
				}

				// Otherwise, throw the data into the scope object.
				if (it.second.GetType() == Datum::DatumType::Table)
				{
					// Have to manually do this for all tables in the data, in the event it's an array.
					for (std::uint32_t i = 0; i < it.second.GetSize(); ++i)
					{
						Scope* childTable = it.second.GetTable(i);
						if (childTable->Is("World") || childTable->Is("Sector"))
						{
							delete newSector;
							throw std::runtime_error("Sectors cannot contain worlds or sectors.");
						}
						else if (childTable->Is("Entity"))
						{
							childTable->As<Entity>()->SetSector(newSector);
						}
						else if (childTable->Is("Action"))
						{
							childTable->As<Action>()->SetOwner(newSector);
						}
						else
						{
							// It's just a normal Scope.
							newSector->Adopt(*childTable, it.first);
						}
					}
				}
				else
				{
					newSector->Append(it.first) = it.second;
				}
			}

			// Pass it up towards the parent hashmap.
			auto& parent = shared->mStack.Top();
			parent[datName] = newSector;
		}
	}

#pragma endregion

}