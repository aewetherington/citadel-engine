#include "pch.h"
#include "Scope.h"
#include "HashMap.h"

namespace Library
{
	RTTI_DEFINITIONS(Scope);

#pragma region Tors
	Scope::Scope(std::uint32_t capacity)
		: mMap(capacity), mReferences(capacity), mParent(nullptr)
	{

	}

	Scope::Scope(const Scope& rhs)
		: Scope()
	{
		operator=(rhs);
	}

	Scope::~Scope()
	{
		Orphan();
		Clear();
	}
#pragma endregion

#pragma region Operators
	Scope& Scope::operator=(const Scope& rhs)
	{
		// Clean up anything that already exists.
		Orphan();
		Clear();

		// Do a deep copy of all Datum objects.
		for (auto itRef : rhs.mReferences)
		{
			// Make a new Datum with the same name.
			Datum& dat = Append(itRef->first);

			switch (itRef->second.GetType())
			{
			case Datum::DatumType::Table:
				// We need to do a deep copy of all of the elements that exist.
				for (std::uint32_t i = 0; i < itRef->second.GetSize(); ++i)
				{
					if (itRef->second.GetTable(i) != nullptr)
					{
						// Make a deep copy (recursive call), then adopt the new scope.
						Scope* newScope = new Scope(*(itRef->second.GetTable(i)));
						try
						{
							Adopt(*newScope, itRef->first);
						}
						catch (...)
						{
							// This should never actually happen.
							delete newScope;
							throw;
						}
					}
				}
				break;

			default:
				// Just rely on Datum's copy operation.
				dat = itRef->second;
				break;
			}
		}

		return *this;
	}

	Datum& Scope::operator[](const std::string& key)
	{
		return Append(key);
	}

	Datum& Scope::operator[](std::uint32_t index) const
	{
		if (index > mReferences.GetSize())
		{
			throw std::runtime_error("Index out of range.");
		}

		return mReferences[index]->second;
	}

	bool Scope::operator==(const Scope& rhs) const
	{
		// Simply test if the two HashMaps contain the same data, not caring about order or map capacity.
		return mMap == rhs.mMap;
	}

	bool Scope::operator!=(const Scope& rhs) const
	{
		return !operator==(rhs);
	}
#pragma endregion

#pragma region Search
	Datum* Scope::Find(const std::string& key) const
	{
		auto it = mMap.Find(key);

		if (it == mMap.end())
		{
			return nullptr;
		}
		else
		{
			return &(it->second);
		}
	}

	std::string Scope::FindName(const Scope& table) const
	{
		std::string str;
		FindName(table, str);
		return str;
	}

	bool Scope::FindName(const Scope& table, std::string& foundName) const
	{
		for (auto& data : mMap)
		{
			// Is this a Scope type?
			if (data.second.GetType() == Datum::DatumType::Table)
			{
				// Iterate through all the scope objects and compare them.
				for (std::uint32_t i = 0; i < data.second.GetSize(); ++i)
				{
					if (&table == data.second.GetTable(i))
					{
						// Found it!
						foundName = data.first;
						return true;
					}
				}
			}
		}

		foundName = std::string();
		return false;
	}

	Datum* Scope::Search(const std::string& key, Scope** associatedScope) const
	{
		Datum* findResult = Find(key);

		if (findResult == nullptr)
		{
			if (mParent == nullptr)
			{
				// Couldn't find it by going up the hierarchy.
				if (associatedScope != nullptr)
				{
					*associatedScope = nullptr;
				}
				return nullptr;
			}
			else
			{
				// Try searching up the hierarchy.
				return mParent->Search(key, associatedScope);
			}
		}
		else
		{
			// Found it!
			if (associatedScope != nullptr)
			{
				*associatedScope = const_cast<Scope*>(this);
			}

			return findResult;
		}
	}
#pragma endregion

#pragma region DataManip
	Datum& Scope::Append(const std::string& key)
	{
		bool dataInserted;
		auto it = mMap.Insert(key, Datum(), dataInserted);

		// If we actually added something, update the inserted references list.
		if (dataInserted)
		{
			mReferences.PushBack(&(*it));
		}

		return it->second;
	}

	Scope& Scope::AppendScope(const std::string& key)
	{
		// Generically append/retrieve a Datum object.
		Datum& dat = Append(key);

		// Test if the Datum we got was legal.
		if (dat.GetType() != Datum::DatumType::Unknown && dat.GetType() != Datum::DatumType::Table)
		{
			throw std::runtime_error("Key already associated with a non-Scope Datum.");
		}

		if (dat.IsExternal())
		{
			throw std::runtime_error("Key already associated with externally stored Datum.");
		}

		// It's legal. Pick the first empty slot to place an object.
		std::uint32_t i;
		for (i = 0; i < dat.GetSize(); ++i)
		{
			if (dat.GetTable(i) == nullptr)
			{
				break;
			}
		}

		// Append this new scope object to the Datum.
		Scope* sc = new Scope();
		try
		{
			dat.Set(sc, i);
		}
		catch (...)
		{
			// This should never have to execute, unless something is wrong with the Datum.
			delete sc;
			throw;
		}

		Scope& newScope = *sc;
		newScope.mParent = this;
		return newScope;

	}

	std::uint32_t Scope::Adopt(Scope& child, const std::string& key, std::uint32_t index)
	{
		std::uint32_t returnIndex = UINT32_MAX;

		// Generically append/retrieve a Datum object.
		Datum& dat = Append(key);

		// Test if the Datum we got was legal.
		if (dat.GetType() != Datum::DatumType::Unknown && dat.GetType() != Datum::DatumType::Table)
		{
			throw std::runtime_error("Key already associated with a non-Scope Datum.");
		}

		if (dat.IsExternal())
		{
			throw std::runtime_error("Key already associated with externally stored Datum.");
		}

		// Make the child an orphan if necessary.
		child.Orphan();

		// Append or shuffle?
		if (index >= dat.GetSize())
		{
			// Pick the first empty slot to place an object.
			std::uint32_t i;
			for (i = 0; i < dat.GetSize(); ++i)
			{
				if (dat.GetTable(i) == nullptr)
				{
					break;
				}
			}

			dat.Set(&child, i);
			returnIndex = i;
		}
		else
		{
			// Do we need to actually shuffle?
			if (dat.GetTable(index) != nullptr)
			{
				std::uint32_t i;

				// Try to find a nullptr space we can overwrite.
				for (i = index; i < dat.GetSize() - 1; ++i)
				{
					if (dat.GetTable(i + 1) == nullptr)
					{
						break;
					}
				}

				// Now we can begin shuffling down to insert it in the right place.
				// The test against UINT32_MAX is to handle underflow, in the event that index == 0.
				for (; i >= index && i != UINT32_MAX; --i)
				{
					dat.Set(dat.GetTable(i), i + 1);
				}

			}

			// Finished making room at specified index.
			dat.Set(&child, index);
			returnIndex = index;
		}

		// Tell the child they have a family again.
		child.mParent = this;

		return returnIndex;
	}

	Scope* Scope::GetParent() const
	{
		return mParent;
	}

	Scope::StringDatumPair* Scope::GetOrderedReference(std::uint32_t index) const
	{
		if (index > mReferences.GetSize())
		{
			throw std::runtime_error("Index out of range.");
		}

		return mReferences[index];
	}

	void Scope::Orphan()
	{
		if (mParent != nullptr)
		{
			// Look through all of parent's Datum objects
			for (auto& data : mParent->mMap)
			{
				Datum& dat = data.second;

				// If it's a Table Datum, then go through all its elements
				if (dat.GetType() == Datum::DatumType::Table)
				{
					for (std::uint32_t i = 0; i < dat.GetSize(); ++i)
					{
						// If we found ourselves, awesome! Cut the connection.
						if (dat.GetTable(i) == this)
						{
							Scope* scopeNull = nullptr;
							dat.Set(scopeNull, i);
							mParent = nullptr;
							return;
						}
					}
				}
			}

			// Couldn't find self.
			throw std::runtime_error("Scope couldn't find self in parent's Datum storage.");

		}
	}

	void Scope::Clear()
	{
		// Go through the Scope and destroy any allocated data this Scope is responsible for.
		for (auto& data : mMap)
		{
			Datum& dat = data.second;

			// Check to get rid of child Scopes.
			if (dat.GetType() == Datum::DatumType::Table && dat.IsExternal() == false)
			{
				// We need to delete each of these Scope objects in the Datum.
				for (std::uint32_t i = 0; i < dat.GetSize(); ++i)
				{
					// This calls the Scope destructur, which results in a recursive delete.
					delete dat.GetTable(i);
				}
			}

		}

		// Now, we can clear out the data structures contained within this Scope.
		mMap.Clear();
		mReferences.Clear();
	}

	std::uint32_t Scope::GetSize() const
	{
		return mReferences.GetSize();
	}

#pragma endregion

}