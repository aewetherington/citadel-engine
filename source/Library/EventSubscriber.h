#pragma once

/*!	\file EventSubscriber.h
	\brief Contains the interface for event subscribers.
*/

#include "pch.h"

namespace Library
{
	class EventPublisher;

	/*!	\brief An interface for event subscribable classes.
	*/
	class EventSubscriber
	{
	public:
		/*!	\brief Frees any allocated data.
		*/
		virtual ~EventSubscriber() = default;

		/*!	\brief Callback function for when an event get primed and delivered to the class, if it has previously subscribed to that type of event.
			@param eventPublisher The event that was fired off and now delivered.
		*/
		virtual void Notify(std::shared_ptr<EventPublisher> eventPublisher) = 0;
	};

}