#include "pch.h"
#include <fstream>
#include "XmlParseMaster.h"
#include "IXmlParseHelper.h"
#include "expat.h"
#include "HashMap.h"

namespace Library
{

#pragma region SharedData

	RTTI_DEFINITIONS(XmlParseMaster::SharedData)

#pragma region Tors

	XmlParseMaster::SharedData::SharedData()
	: mParseMaster(nullptr), mDepth(0), mIsClone(false)
	{

	}

#pragma endregion

#pragma region ParseMaster

	void XmlParseMaster::SharedData::SetXmlParseMaster(XmlParseMaster* parseMaster)
	{
		mParseMaster = parseMaster;
	}

	XmlParseMaster* XmlParseMaster::SharedData::GetXmlParseMaster() const
	{
		return mParseMaster;
	}

#pragma endregion

#pragma region Depth

	void XmlParseMaster::SharedData::IncrementDepth()
	{
		++mDepth;
	}

	void XmlParseMaster::SharedData::DecrementDepth()
	{
		--mDepth;
	}

	std::uint32_t XmlParseMaster::SharedData::GetDepth() const
	{
		return mDepth;
	}

#pragma endregion

#pragma endregion

#pragma region XmlParseMaster

#pragma region Tors

	XmlParseMaster::XmlParseMaster(SharedData* sharedData)
		: mParser(XML_ParserCreate("US-ASCII")), mHelpers(), mFilename(), mIsClone(false)
	{
		XML_SetUserData(mParser, this);
		XML_SetElementHandler(mParser, StartElementHandler, EndElementHandler);
		XML_SetCharacterDataHandler(mParser, CharDataHandler);

		SetSharedData(sharedData);
	}

	XmlParseMaster::~XmlParseMaster()
	{
		XML_ParserFree(mParser);

		if (mIsClone)
		{
			// Delete all helpers.
			for (auto helper : mHelpers)
			{
				delete helper;
			}

			// Delete shared data.
			delete mSharedData;
		}
	}

	XmlParseMaster* XmlParseMaster::Clone() const
	{
		XmlParseMaster* newParseMaster = new XmlParseMaster();

		// Clone the helpers.
		for (auto helper : mHelpers)
		{
			newParseMaster->mHelpers.PushBack(helper->Clone());
		}

		// Clone the SharedData.
		if (mSharedData != nullptr)
		{
			newParseMaster->SetSharedData(mSharedData->Clone());
		}

		// Set clone self-knowledge.
		newParseMaster->mIsClone = true;

		return newParseMaster;
	}

#pragma endregion

#pragma region Helper

	void XmlParseMaster::AddHelper(IXmlParseHelper& helper)
	{
		mHelpers.PushBack(&helper);
	}

	void XmlParseMaster::RemoveHelper(IXmlParseHelper& helper)
	{
		mHelpers.Remove(&helper);
	}

#pragma endregion

#pragma region Data

	void XmlParseMaster::Parse(const char* data, std::uint32_t length, bool isFinal) const
	{
		int parseSuccess = XML_Parse(mParser, data, length, isFinal);
		if (parseSuccess != XML_STATUS_OK)
		{
			throw std::runtime_error("Failed to parse the XML: Invalid format");
		}
	}

	void XmlParseMaster::ParseFromFile(const std::string& filename)
	{
		const int bufferSize = 512;
		char buffer[bufferSize];
		std::ifstream fin(filename, std::ios::in);

		if (!fin.is_open())
		{
			throw std::runtime_error("Unable to find file: " + filename);
		}

		mFilename = filename;

		// Initialize the helper functions.
		for (auto helper : mHelpers)
		{
			helper->Initialize();
		}

		// Attempt to grab data line-by-line.
		for (fin.getline(buffer, bufferSize); buffer[0] != '\0'; fin.getline(buffer, bufferSize))
		{
			Parse(buffer, strlen(buffer), false);
		}
		Parse("", 0, true);

	}

	std::string& XmlParseMaster::GetFileName()
	{
		return mFilename;
	}

	XmlParseMaster::SharedData* XmlParseMaster::GetSharedData() const
	{
		return mSharedData;
	}

	void XmlParseMaster::SetSharedData(SharedData* shared)
	{
		if (mSharedData != nullptr)
		{
			if (mIsClone)
			{
				delete mSharedData;
			}
			else
			{
				mSharedData->SetXmlParseMaster(nullptr);
			}
		}

		mSharedData = shared;

		if (mSharedData != nullptr)
		{
			mSharedData->SetXmlParseMaster(this);
		}
	}

#pragma endregion

#pragma region Handlers

	void XmlParseMaster::StartElementHandler(void* userData, const char* name, const char** attribs)
	{
		XmlParseMaster* parseMaster = reinterpret_cast<XmlParseMaster*>(userData);

		// Construct a hashmap of the attributes.
		HashMap<std::string, std::string> attributes;

		if (attribs != nullptr)
		{
			for (std::uint32_t i = 0; attribs[i] != nullptr; i += 2)
			{
				attributes.Insert(attribs[i], attribs[i + 1]);
			}
		}

		// Update the depth.
		if (parseMaster->mSharedData != nullptr)
		{
			parseMaster->mSharedData->IncrementDepth();
		}

		// Pass along this data to the helpers.
		for (auto& helper : parseMaster->mHelpers)
		{
			// If one of the helpers handles successfully, then quit early.
			if (helper->StartElementHandler(parseMaster->mSharedData, name, attributes))
			{
				break;
			}
		}
	}

	void XmlParseMaster::EndElementHandler(void* userData, const char* name)
	{
		XmlParseMaster* parseMaster = reinterpret_cast<XmlParseMaster*>(userData);

		// Pass along this data to the helpers.
		for (auto& helper : parseMaster->mHelpers)
		{
			// If one of the helpers handles successfully, then quit early.
			if (helper->EndElementHandler(parseMaster->mSharedData, name))
			{
				break;
			}
		}

		// Update the depth.
		if (parseMaster->mSharedData != nullptr)
		{
			parseMaster->mSharedData->DecrementDepth();
		}
	}

	void XmlParseMaster::CharDataHandler(void* userData, const char* str, int length)
	{
		XmlParseMaster* parseMaster = reinterpret_cast<XmlParseMaster*>(userData);

		// Clean up the string data before distributing it to the helpers
		char* cleanedStr;
		cleanedStr = new char[length + 1];
		strncpy_s(cleanedStr, length + 1, str, length);

		// Pass along this data to the helpers.
		for (auto& helper : parseMaster->mHelpers)
		{
			// If one of the helpers handles successfully, then quit early.
			if (helper->CharElementHandler(parseMaster->mSharedData, cleanedStr, length))
			{
				break;
			}
		}

		delete[] cleanedStr;
	}

#pragma endregion

#pragma endregion

}