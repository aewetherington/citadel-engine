#include "pch.h"
#include "Attributed.h"

namespace Library
{

	RTTI_DEFINITIONS(Attributed)

	HashMap<std::uint32_t, Vector<Attributed::Signature>> Attributed::sSignatures;

#pragma region Tors&Operators

	Attributed::Attributed()
		: Scope()
	{
		(*this)["this"] = reinterpret_cast<RTTI*>(this);
		mIsPopulated = false;
		mNumPrescribedAttributes = 1;
	}

	Attributed::Attributed(const Attributed& rhs)
		: Scope(rhs)
	{
		(*this)["this"] = reinterpret_cast<RTTI*>(this);
		mIsPopulated = false;
		mNumPrescribedAttributes = rhs.mNumPrescribedAttributes;
	}

	Attributed& Attributed::operator=(const Attributed& rhs)
	{
		Scope::operator=(rhs);
		(*this)["this"] = reinterpret_cast<RTTI*>(this);
		mIsPopulated = false;
		mNumPrescribedAttributes = rhs.mNumPrescribedAttributes;

		return *this;
	}

#pragma endregion

#pragma region AttributeManip

	void Attributed::Populate()
	{

		Vector<Signature>& vecSigs = sSignatures[TypeIdInstance()];
		
		for (auto& sig : vecSigs)
		{
			// First, check that the Signature is valid (exactly one is non-null).
			if ((sig.InitialValue.i == nullptr) == (sig.Storage == nullptr))
			{
				throw std::runtime_error("Signature must have exactly one of internal or external storage specified: " + sig.Name);
			}

			// Make sure the key isn't reserved by the engine.
			if (sig.Name == "this")
			{
				throw std::runtime_error("Provided attribute name is reserved by the engine: " + sig.Name);
			}

			// Make sure we don't reuse a key that's already in use.
			if (Find(sig.Name) != nullptr)
			{
				continue;
			}

			// Does this signature describe external storage?
			if (sig.Storage != nullptr)
			{
				Append(sig.Name).SetStorage(sig.Storage, sig.Type, sig.Size);
			}
			// It describes internal storage.
			else
			{
				if (sig.Type == Datum::DatumType::MAX)
				{
					throw std::runtime_error("Attempted to populate with invalid signature: " + sig.Name);
				}

				sPopulateFuncts[static_cast<int>(sig.Type)](*this, sig);
			}

			++mNumPrescribedAttributes;
		}

		mIsPopulated = true;
	}

	bool Attributed::IsPrescribedAttribute(const std::string& attr) const
	{
		// Check for reserved names.
		if (attr == "this")
		{
			return true;
		}
		
		if (mIsPopulated)
		{
			std::uint32_t auxBegin = AuxiliaryBegin();
			for (std::uint32_t i = 0; i < auxBegin; ++i)
			{
				if (GetOrderedReference(i)->first == attr)
				{
					return true;
				}
			}
		}

		return false;
	}

	bool Attributed::IsAuxiliaryAttribute(const std::string& attr) const
	{
		return IsAttribute(attr) && !IsPrescribedAttribute(attr);
	}

	bool Attributed::IsAttribute(const std::string& attr) const
	{
		return Find(attr) != nullptr;
	}

	Datum& Attributed::AppendAuxiliaryAttribute(const std::string& attr)
	{
		if (IsPrescribedAttribute(attr))
		{
			throw std::runtime_error("Attribute was already prescribed.");
		}

		return Append(attr);
	}

	std::uint32_t Attributed::AuxiliaryBegin() const
	{
		return mNumPrescribedAttributes;
	}

	void Attributed::Clear()
	{
		Scope::Clear();
		(*this)["this"] = reinterpret_cast<RTTI*>(this);
		mIsPopulated = false;
		mNumPrescribedAttributes = 1;
	}

#pragma endregion

#pragma region SignatureTors

	Attributed::Signature::Signature()
		: Name(std::string()), Type(Datum::DatumType::Unknown), InitialValue(nullptr), Size(0), Storage(nullptr)
	{

	}

	Attributed::Signature::Signature(std::string name, Datum::DatumType type, void* initialValue, std::uint32_t size, void* storage)
		: Name(name), Type(type), InitialValue(initialValue), Size(size), Storage(storage)
	{

	}

#pragma endregion

#pragma region PopulateHelpers

	const Attributed::PopulateFunction Attributed::sPopulateFuncts[] = {
		PopulateUnknown,
		PopulateInteger,
		PopulateFloat,
		PopulateVector,
		PopulateMatrix,
		PopulateString,
		PopulateTable,
		PopulatePointer
	};

	void Attributed::PopulateUnknown(Attributed& a, const Signature& sig)
	{
		throw std::runtime_error("Attempted to populate with unknown-type signature: " + sig.Name);
	}

	void Attributed::PopulateInteger(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.i[index], index);
		}
	}

	void Attributed::PopulateFloat(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.f[index], index);
		}
	}

	void Attributed::PopulateVector(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.v[index], index);
		}
	}

	void Attributed::PopulateMatrix(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.m[index], index);
		}
	}

	void Attributed::PopulateString(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.s[index], index);
		}
	}

	void Attributed::PopulateTable(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.t[index], index);
		}
	}

	void Attributed::PopulatePointer(Attributed& a, const Signature& sig)
	{
		Datum& dat = a[sig.Name];
		for (std::uint32_t index = 0; index < sig.Size; ++index)
		{
			dat.Set(sig.InitialValue.r[index], index);
		}
	}

#pragma endregion

}