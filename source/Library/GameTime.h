#pragma once

/*!	\file GameTime.h
	\brief Contains declarations for GameTime, which is used to query for time elapsed per frame.
*/

#include <chrono>

namespace Library
{
	/*! \brief Class used to query for time elapsed per frame.
	*/
	class GameTime
	{
	public:
		/*!	\brief Sets default values.
		*/
		GameTime();

		const std::chrono::high_resolution_clock::time_point& GetCurrentTime() const;
		void SetCurrentTime(const std::chrono::high_resolution_clock::time_point& currentTime);

		const std::chrono::milliseconds& GetTotalGameTime() const;
		void SetTotalGameTime(const std::chrono::milliseconds& totalGameTime);

		const std::chrono::milliseconds& GetElapsedGameTime() const;
		void SetElapsedGameTime(const std::chrono::milliseconds& elapsedGameTime);

	private:
		std::chrono::high_resolution_clock::time_point mCurrentTime;
		std::chrono::milliseconds mTotalGameTime;
		std::chrono::milliseconds mElapsedGameTime;
	};
}
