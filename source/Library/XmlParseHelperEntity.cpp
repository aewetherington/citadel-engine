#include "pch.h"
#include "XmlParseHelperEntity.h"
#include "Entity.h"

namespace Library
{
	IXmlParseHelper* XmlParseHelperEntity::Clone() const
	{
		IXmlParseHelper* newHelper = new XmlParseHelperEntity();

		return newHelper;
	}

#pragma region Handlers

	bool XmlParseHelperEntity::StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mDataTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mEntityTag ||
			(name == SharedDataTable::mValueTag && parentType == SharedDataTable::mEntityTag)))
		{
			return false;
		}

		// Ensure this object at least has a name, assuming it's not a value tag.
		if (name != SharedDataTable::mValueTag && !attributes.ContainsKey(SharedDataTable::mNameAttribute))
		{
			throw std::runtime_error("New data types must have a name attribute. Offending XML Tag type: " + name);
		}

		// Ensure this object has a class name.
		if (name != SharedDataTable::mValueTag && !attributes.ContainsKey(SharedDataTable::mClassAttribute))
		{
			throw std::runtime_error("New Entity types must have a class attribute.");
		}

		// Ensure the user isn't trying to use a reserved name.
		for (auto str : SharedDataTable::mReservedKeys)
		{
			if (attributes.ContainsKey(str))
			{
				throw std::runtime_error("Attribute of type (" + name + ") attempted to use reserved name: " + str);
			}
		}

		if (sharedData->GetDepth() > 1)
		{
			// If this is a value tag, mark parent as an array by ensuring that key is contained in their hashmap.
			if (name == SharedDataTable::mValueTag)
			{
				shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			}
			// Otherwise, give it a scalar flag key.
			else
			{
				shared->mStack.Top()[SharedDataTable::mIsScalarKey];
			}
		}

		// Add object onto the stack.
		shared->mStack.Push(HashMap<std::string, Datum>());

		// Prepare the character data string buffer to be filled.
		shared->mStack.Top()[SharedDataTable::mStringDataKey] = std::string();

		// Set the type strings.
		shared->mStack.Top()[SharedDataTable::mDataTypeKey] = name;
		shared->mStack.Top()[SharedDataTable::mParentTypeKey] = parentType;

		// Add all attributes to the pile of data known, and retain their string form.
		for (auto& val : attributes)
		{
			Datum tmpDat;
			tmpDat = val.second;
			shared->mStack.Top().Insert(val.first, tmpDat);
		}

		// Preallocate an array, if specified.
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mSizeAttribute))
		{
			std::string sizeNumStr = shared->mStack.Top()[SharedDataTable::mSizeAttribute].Get<std::string>();

			std::uint32_t size = std::strtoul(sizeNumStr.c_str(), nullptr, 0);
			if (size == 0)
			{
				throw std::runtime_error("Invalid value in type (" + name + ") given for array size: " + sizeNumStr);
			}

			Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			valuesArray.SetType(Datum::DatumType::Table);
			valuesArray.SetSize(size);
			shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey] = 0;
		}

		return true;
	}

	bool XmlParseHelperEntity::EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mParentTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mEntityTag ||
			(name == SharedDataTable::mValueTag && parentType == SharedDataTable::mEntityTag)))
		{
			return false;
		}
		
		// Finish handling based on type.
		if (name == SharedDataTable::mValueTag)
		{
			ParseValue(shared, name);
		}
		else
		{
			ParseEntity(shared, name);
		}

		return true;
	}

#pragma endregion

#pragma region Helpers

	void XmlParseHelperEntity::ParseValue(SharedDataTable* shared, std::string name)
	{
		// Ensure that this is a potentially valid depth.
		if (shared->GetDepth() <= 1)
		{
			throw std::runtime_error("Invalid depth: " + name);
		}

		// Go ahead and pop it off.
		auto poppedMap = shared->mStack.Pop();

		// Check if the parent tag is invalid.
		if (shared->mStack.Top()[SharedDataTable::mDataTypeKey] == SharedDataTable::mValueTag)
		{
			throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " cannot have tags of type " + SharedDataTable::mValueTag + " as parents.");
		}

		// Add it to the parent's values array.
		Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
		std::int32_t index;
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mNumArrayElementsKey))
		{
			index = shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey].Get<int>()++;
		}
		else
		{
			index = shared->mStack.Top()[SharedDataTable::mArrayElementsKey].GetSize();
		}

		// Construct this entity based on its contents.
		Entity* newEntity = Factory<Entity>::Create(shared->mStack.Top()[SharedDataTable::mClassAttribute].Get<std::string>());
		if (newEntity == nullptr)
		{
			throw std::runtime_error("Failed to create new Entity of type " + shared->mStack.Top()[SharedDataTable::mClassAttribute].Get<std::string>());
		}
		newEntity->SetName(shared->mStack.Top()[SharedDataTable::mNameAttribute].Get<std::string>());

		// For all elements in this popped map.
		for (auto& it : poppedMap)
		{
			// Skip over this one if it's a reserved tag.
			bool isReservedTag = false;
			for (std::string str : SharedDataTable::mReservedKeys)
			{
				if (it.first == str)
				{
					isReservedTag = true;
				}
			}
			if (isReservedTag)
			{
				continue;
			}

			// Otherwise, throw the data into the scope object.
			if (it.second.GetType() == Datum::DatumType::Table)
			{
				Scope* childTable = it.second.GetTable();
				if (childTable->Is("World") || childTable->Is("Sector") || childTable->Is("Entity"))
				{
					delete newEntity;
					throw std::runtime_error("Entities cannot contain worlds, sectors, or entities.");
				}
				else if (childTable->Is("Action"))
				{
					childTable->As<Action>()->SetOwner(newEntity);
				}
				else
				{
					// It's just a normal Scope.
					newEntity->Adopt(*childTable, it.first);
				}
			}
			else
			{
				newEntity->Append(it.first) = it.second;
			}
		}

		// Now, place the scope into its proper place.
		valuesArray.Set(newEntity, index);
	}

	void XmlParseHelperEntity::ParseEntity(SharedDataTable* shared, std::string name)
	{
		// Check for ambiguity on whether scalar or array.
		if (shared->mStack.Top()[SharedDataTable::mStringDataKey] != "" && shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Ambiguous declaration between scalar and array: " + name);
		}

		auto poppedMap = shared->mStack.Pop();

		// Check if the parent tag is invalid.
		if (shared->GetDepth() > 1)
		{
			if (shared->mStack.Top()[SharedDataTable::mDataTypeKey] != SharedDataTable::mSectorTag)
			{
				throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " can only have sector tags as parents.");
			}
		}

		// Handle if array of entities.
		if (poppedMap.ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			// Note that this array handler should only ever run if this is not the root.
			if (shared->GetDepth() <= 1)
			{
				throw std::runtime_error("Invalid depth for array of entities.");
			}

			// Cache some data.
			Datum& datArray = poppedMap[SharedDataTable::mArrayElementsKey];
			auto& parent = shared->mStack.Top();
			std::string& datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();

			Datum& parentDatArray = parent[datName];

			for (std::uint32_t i = 0; i < datArray.GetSize(); ++i)
			{
				parentDatArray.Set(datArray.GetTable(i), i);
			}
		}
		// Handle if scalar
		else
		{
			// Cache some data.
			std::string& datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();

			// Check that the depth is OK.
			if (shared->GetDepth() <= 2)
			{
				throw std::runtime_error("Invalid depth for entity tag.");
			}

			std::string val = poppedMap[SharedDataTable::mClassAttribute].Get<std::string>();

			Entity* newEntity = Factory<Entity>::Create(poppedMap[SharedDataTable::mClassAttribute].Get<std::string>());
			newEntity->SetName(poppedMap[SharedDataTable::mNameAttribute].Get<std::string>());

			// For all elements in this popped map.
			for (auto& it : poppedMap)
			{
				// Skip over this one if it's a reserved tag.
				bool isReservedTag = false;
				for (std::string str : SharedDataTable::mReservedKeys)
				{
					if (it.first == str)
					{
						isReservedTag = true;
					}
				}
				if (isReservedTag)
				{
					continue;
				}

				// Otherwise, throw the data into the scope object.
				if (it.second.GetType() == Datum::DatumType::Table)
				{
					// Have to manually do this for all tables in the data, in the event it's an array.
					for (std::uint32_t i = 0; i < it.second.GetSize(); ++i)
					{
						Scope* childTable = it.second.GetTable(i);
						if (childTable->Is("World") || childTable->Is("Sector") || childTable->Is("Entity"))
						{
							delete newEntity;
							throw std::runtime_error("Entities cannot contain worlds, sectors, or entities.");
						}
						else if (childTable->Is("Action"))
						{
							childTable->As<Action>()->SetOwner(newEntity);
						}
						else
						{
							// It's just a normal Scope.
							newEntity->Adopt(*childTable, it.first);
						}
					}
				}
				else
				{
					newEntity->Append(it.first) = it.second;
				}
			}

			// Pass it up towards the parent hashmap.
			auto& parent = shared->mStack.Top();
			parent[datName] = newEntity;
		}
	}

#pragma endregion

}