#pragma once

/*! \file HashMap.h
	\brief Contains the declarations for a templated HashMap, that uses chaining.
*/

#include "pch.h"

namespace Library
{
	template <typename T> class SList;
	template <typename T> class Vector;

	/*!	\brief Templated implementation of a default hashfunctor.
	*/
	template <typename T>
	class DefaultHash final
	{
	public:
		std::uint32_t operator()(const T& key) const;
	};

	/*!	\brief Templated implementation of a HashMap.

		When using this class, ownership of memory does not transfer to the hashmap. User is responsible for deleting previously allocated memory when finished with it.
	*/
	template <typename TKey, typename TData, typename HashFunctor = DefaultHash<TKey>>
	class HashMap
	{
	public:
		typedef std::pair<TKey, TData> PairType;

	private:
		typedef SList<PairType> ChainType;
		typedef Vector<ChainType> BucketType;

	public:
		/*!	\brief Used to simplify manipulating the HashMap.
		*/
		class Iterator
		{
			friend class HashMap;

		public:
			/*!	\brief Initializes the iterator to default values, not associated with any HashMap.
			*/
			Iterator();

			/*!	\brief Copy constructor that sets the iterator's values to be the same as the parameter's values.
				@param rhs The iterator to be copied.
				@see operator=()
			*/
			Iterator(const Iterator& rhs);

			/*!	\brief Sets the iterator's values to be the same as the paramter's. Performs a shallow copy.
				@param rhs The iterator to be ccopied.
				@return Reference to the new iterator.
				@see Iterator(const Iterator& rhs)
			*/
			Iterator& operator=(const Iterator& rhs);

			/*!	\brief Compares the two iterators to see if they are pointing to the same data.
				@param rhs The iterator to be compared to this one.
				@return True only if the two iterators are equivalent.
				@see operator!=()
			*/
			bool operator==(const Iterator& rhs) const;

			/*! \fn operator!=(const Iterator& rhs) const
				\brief Compares the two iterators to see if they are pointing to different data.
				@param rhs The iterator to be compared to this one.
				@return True only if the two iterators are inequivalent.
				@see operator==()
			*/
			bool operator!=(const Iterator& rhs) const;

			/*! \brief Pre-increment operator that moves the iterator to point to the next element in the hashmap, and returns the new iterator.
				@return The iterator after it has been incremented.
				@see operator++(int)
			*/
			Iterator& operator++();

			/*! \brief Post-increment operator that moves the iterator to point to the next element in the hashmap, and returns a copy of the old iterator.
				@return A copy of the iterator before it has been incremented.
				@see operator++()
			*/
			Iterator operator++(int);

			/*!	\brief Retrieves the data at the location pointed to by this iterator, and returns it.
				@return The data associated with this iterator.
				@exception std::runtime_error Thrown if called on an iterator not pointing to any data.
				@see operator->()
			*/
			PairType& operator*() const;

			/*!	\brief Retrieves the data at the location pointed to by this iterator, and returns a pointer to it.
				@return A pointer to the data associated with this iterator.
				@exception std::runtime_error Thrown if called on an iterator not pointing to any data.
				@see operator*()
			*/
			PairType* operator->() const;

		private:
			// Used to create a new iterator with the specified parameters.
			Iterator(const HashMap *owner, std::uint32_t index, const typename SList<PairType>::Iterator& chainIt);

			// Data to allow the iterator to be used.
			const HashMap* mOwner;
			std::uint32_t mIndex;
			typename ChainType::Iterator mChainIt;
		};

		/*! \brief Initializes the hashmap to be empty.

			Provides a default hashmap capacity if none is specified. Clamps the capacity to be at least 1.
			@param capacity The size of the hash table. Chaining allows more than this number of values to be stored in the hashmap.
		*/
		explicit HashMap(std::uint32_t capacity = 8);

		/*!	\brief Performs a deep copy of the hashmap.

			@param rhs The hashmap to be copied.
			@see operator=(const HashMap&)
		*/
		HashMap(const HashMap& rhs);

		/*!	\brief Performs a deep copy of the hashmap.

			@param rhs The hashmap to be copied.
			@return The copied hashmap.
			@see HashMap(const HashMap&)
		*/
		HashMap& operator=(const HashMap& rhs);

		/*! \brief Iterates through all elements in the HashMap and tests if they exist in the other HashMap.
			@param rhs The HashMap to compare against.
			@return True only if all elements exist in the other HashMap.
		*/
		bool operator==(const HashMap& rhs) const;

		/*! \brief Iterates through all elements in the HashMap and tests if at least one does not exist in the other HashMap.
			@param rhs The HashMap to compare against.
			@return True only if at least one element does not exist in the other HashMap.
		*/
		bool operator!=(const HashMap& rhs) const;

		/*!	\brief Takes a key value and returns an iterator to the value with the associated key.

			@param key The key to be searched with.
			@return An Iterator to the value that has the associated key.
			@see ContainsKey(const TKey&)
		*/
		Iterator Find(const TKey& key) const;

		/*!	\brief Adds the pairtype to the hashtable, provided the key doesn't exist.

			If the key supplied already exists in the hashmap, then the returned iterator points to the
			location of that matching key element. Otherwise, it adds the supplied pair and returns an
			iterator to that location.
			@param data The key/data pair to be stored.
			@param dataInserted Modified based on whether the insert actually added something, or not.
			@return An iterator to the location containing the matching key.
		*/
		Iterator Insert(const PairType& data, bool& dataInserted);

		/*!	\brief Adds the pairtype to the hashtable, provided the key doesn't exist.

			If the key supplied already exists in the hashmap, then the returned iterator points to the 
			location of that matching key element. Otherwise, it adds the supplied pair and returns an
			iterator to that location.
			@param data The key/data pair to be stored.
			@return An iterator to the location containing the matching key.
		*/
		Iterator Insert(const PairType& data);

		/*!	\brief Adds the pairtype to the hashtable, provided the key doesn't exist.

			If the key supplied already exists in the hashmap, then the returned iterator points to the
			location of that matching key element. Otherwise, it adds the supplied pair and returns an
			iterator to that location.
			@param key The key to be stored.
			@param value The value to be stored.
			@param dataInserted Modified based on whether the insert actually added something, or not.
			@return An iterator to the location containing the matching key.
		*/
		Iterator Insert(const TKey& key, const TData& value, bool& dataInserted);

		/*!	\brief Adds the pairtype to the hashtable, provided the key doesn't exist.

			If the key supplied already exists in the hashmap, then the returned iterator points to the
			location of that matching key element. Otherwise, it adds the supplied pair and returns an
			iterator to that location.
			@param key The key to be stored.
			@param value The value to be stored.
			@return An iterator to the location containing the matching key.
		*/
		Iterator Insert(const TKey& key, const TData& value);

		/*!	\brief Retrieves the TData associated with the TKey search element.

			If the key does not exist in the hashmap, a new entry for it will be made.
			@param key The key in the key/data pair to be accessed.
			@return The data associated with the key.
		*/
		TData& operator[](const TKey& key);

		/*! \brief Removes the element associated with the key, if it exists.
			@param The key whose key/value pair is to be removed from the hashmap.
		*/
		void Remove(const TKey& key);

		/*!	\brief Empties the table.
		*/
		void Clear();

		/*!	\brief Returns the population of the table.
			@return The population of the table.
		*/
		std::uint32_t GetSize() const;

		/*!	\brief Returns true if an entry for the key exists in the hashmap
			@param key The key to be searched for.
			@return True if the key is found, and false otherwise.
			@see Find(const TKey&)
		*/
		bool ContainsKey(const TKey& key) const;

		/*!	\brief Returns an iterator pointing at the beginning of the hashmap.
			@return Iterator pointing at the beginning of the hashmap.
			@see end()
		*/
		Iterator begin() const;

		/*!	\brief Returns an iterator pointing at the end of the hashmap.
			@return Iterator pointing at the end of the hashmap.
			@see end()
		*/
		Iterator end() const;

	private:
		std::uint32_t mBeginIndex;
		std::uint32_t mSize;
		BucketType mBuckets;
		HashFunctor mHashFunct;
	};
}

#include "HashMap.inl"