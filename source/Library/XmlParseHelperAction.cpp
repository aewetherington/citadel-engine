#include "pch.h"
#include "XmlParseHelperAction.h"
#include "Factory.h"
#include "Action.h"
#include "ActionList.h"

namespace Library
{

	IXmlParseHelper* XmlParseHelperAction::Clone() const
	{
		IXmlParseHelper * newHelper = new XmlParseHelperAction();

		return newHelper;
	}

	bool XmlParseHelperAction::StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mDataTypeKey)->second.Get<std::string>();
		}

		// Ensure this a valid tag.
		if (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mActionTag)
		{
			throw std::runtime_error("Action tags cannot be arrays.");
		}
		else if (name != SharedDataTable::mActionTag)
		{
			return false;
		}

		// Ensure this object has a class name.
		if (name != SharedDataTable::mValueTag && !attributes.ContainsKey(SharedDataTable::mClassAttribute))
		{
			throw std::runtime_error("Action tags must have a class attribute.");
		}

		// Ensure the user isn't trying to use a reserved name.
		for (auto str : SharedDataTable::mReservedKeys)
		{
			if (attributes.ContainsKey(str))
			{
				throw std::runtime_error("Attribute of type (" + name + ") attempted to use reserved name: " + str);
			}
		}

		// Add object onto the stack.
		shared->mStack.Push(HashMap<std::string, Datum>());

		// Prepare the character data string buffer to be filled.
		shared->mStack.Top()[SharedDataTable::mStringDataKey] = std::string();

		// Set the type strings.
		shared->mStack.Top()[SharedDataTable::mDataTypeKey] = name;
		shared->mStack.Top()[SharedDataTable::mParentTypeKey] = parentType;

		// Add all attributes to the pile of data known, and retain their string form.
		for (auto& val : attributes)
		{
			Datum tmpDat;
			tmpDat = val.second;
			shared->mStack.Top().Insert(val.first, tmpDat);
		}

		return true;
	}

	bool XmlParseHelperAction::EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Ensure this a correct tag.
		if (name != SharedDataTable::mActionTag)
		{
			return false;
		}

		// Begin actually handling the action.
		auto poppedMap = shared->mStack.Pop();

		// Check if the parent tag is invalid.
		if (shared->GetDepth() > 1)
		{
			std::string parentType = poppedMap[SharedDataTable::mParentTypeKey].Get<std::string>();
			if (parentType != SharedDataTable::mSectorTag
				&& parentType != SharedDataTable::mEntityTag
				&& parentType != SharedDataTable::mWorldTag
				&& parentType != SharedDataTable::mActionTag)
			{
				throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " can only have sector tags as parents.");
			}
		}
		else
		{
			throw std::runtime_error("Invalid depth for action tag.");
		}

		// Cache some data.
		std::string datName, className;
		if (poppedMap.ContainsKey(SharedDataTable::mNameAttribute))
		{
			datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();
		}
		else
		{
			datName = poppedMap[SharedDataTable::mClassAttribute].Get<std::string>();
		}
		className = poppedMap[SharedDataTable::mClassAttribute].Get<std::string>();

		Action* newAction = Factory<Action>::Create(className);
		if (newAction == nullptr)
		{
			throw std::runtime_error("Failed to create new Action of type " + className);
		}
		newAction->SetName(datName);

		// For all elements in this popped map.
		for (auto& it : poppedMap)
		{
			// Skip over this one if it's a reserved tag.
			bool isReservedTag = false;
			for (std::string str : SharedDataTable::mReservedKeys)
			{
				if (it.first == str)
				{
					isReservedTag = true;
				}
			}
			if (isReservedTag)
			{
				continue;
			}

			// Otherwise, add the data into this scope object.
			if (it.second.GetType() == Datum::DatumType::Table)
			{
				// Have to manually do this for all tables in the data, in the event it's an array.
				for (std::uint32_t i = 0; i < it.second.GetSize(); ++i)
				{
					Scope* childTable = it.second.GetTable(i);
					if (childTable->Is("World") || childTable->Is("Sector") || childTable->Is("Entity"))
					{
						delete newAction;
						throw std::runtime_error("Actions cannot contain worlds, sectors, or entities.");
					}
					else if (childTable->Is("Action"))
					{
						if (newAction->Is("ActionList"))
						{
							// Can own Actions, because this is an ActionList.
							childTable->As<Action>()->SetOwner(newAction->As<ActionList>());
						}
						else
						{
							delete newAction;
							throw std::runtime_error("Non-ActionList Actions cannot contain actions.");
						}
					}
					else
					{
						// It's just a normal Scope.
						newAction->Adopt(*childTable, it.first);
					}
				}
			}
			else
			{
				// It's a primitive Datum.
				newAction->Append(it.first) = it.second;
			}
		}

		// Pass it up towards the parent hashmap, allowing for duplicate names since the name attribute is optional.
		auto& parent = shared->mStack.Top();
		parent[datName].Set(newAction, parent[datName].GetSize());

		return true;
	}
}