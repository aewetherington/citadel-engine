#include "pch.h"
#include "Action.h"
#include "World.h"
#include "Sector.h"
#include "Entity.h"
#include "ActionList.h"

namespace Library
{
	RTTI_DEFINITIONS(Action)

	Action::Action(const std::string& name)
		: mName(name), mOwner(nullptr)
	{
		Populate();
	}

	Action::Action(const Action& rhs)
		: Attributed(rhs), mName(rhs.mName), mLocalID(UINT32_MAX), mOwner(nullptr)
	{
		// Name is an external attribute, so it needs to be updated.
		Find("name")->SetStorage(&mName, 1);
	}

	Action& Action::operator=(const Action& rhs)
	{
		Attributed::operator=(rhs);

		// Do some initialization
		mName = rhs.mName;
		mLocalID = UINT32_MAX;
		mOwner = nullptr;

		// Name is an external attribute, so it needs to be updated.
		Find("name")->SetStorage(&mName, 1);

		return *this;
	}

	void Action::Populate()
	{
		int isActive = 1;

		INIT_SIGNATURE();

		EXTERNAL_ATTRIBUTE("name", Datum::DatumType::String, &mName, 1);
		INTERNAL_ATTRIBUTE("isActive", Datum::DatumType::Integer, &isActive, 1);

		Attributed::Populate();
	}

	const std::string& Action::GetName() const
	{
		return mName;
	}

	void Action::SetName(const std::string& name)
	{
		Orphan();
		mName = name;
		if (mOwner != nullptr)
		{
			if (mOwner->Is("World"))
			{
				mLocalID = mOwner->As<World>()->GetActions()->Adopt(*this, mName);
			}
			else if (mOwner->Is("Sector"))
			{
				mLocalID = mOwner->As<Sector>()->GetActions()->Adopt(*this, mName);
			}
			else if (mOwner->Is("Entity"))
			{
				mLocalID = mOwner->As<Entity>()->GetActions()->Adopt(*this, mName);
			}
			else if (mOwner->Is("ActionList"))
			{
				mLocalID = mOwner->As<ActionList>()->GetActions()->Adopt(*this, mName);
			}
		}
		else
		{
			mLocalID = UINT32_MAX;
		}
	}

	std::string Action::GetGUID() const
	{
		if (mOwner != nullptr)
		{
			if (mOwner->Is("World"))
			{
				return mOwner->As<World>()->GetGUID() + "_" + mName + "_" + std::to_string(mLocalID);
			}
			else if (mOwner->Is("Sector"))
			{
				return mOwner->As<Sector>()->GetGUID() + "_" + mName + "_" + std::to_string(mLocalID);
			}
			else if (mOwner->Is("Entity"))
			{
				return mOwner->As<Entity>()->GetGUID() + "_" + mName + "_" + std::to_string(mLocalID);
			}
			else if (mOwner->Is("ActionList"))
			{
				return mOwner->As<ActionList>()->GetGUID() + "_" + mName + "_" + std::to_string(mLocalID);
			}
			else
			{
				throw std::runtime_error("Invalid owner type.");
			}
		}
		else
		{
			return mName + "_" + std::to_string(mLocalID);
		}
	}

	Scope* Action::GetOwner() const
	{
		return mOwner;
	}

	void Action::SetOwner(Scope* newOwner)
	{
		Orphan();
		mOwner = newOwner;
		if (mOwner != nullptr)
		{
			if (mOwner->Is("World"))
			{
				mLocalID = mOwner->As<World>()->GetActions()->Adopt(*this, mName);
			}
			else if (mOwner->Is("Sector"))
			{
				mLocalID = mOwner->As<Sector>()->GetActions()->Adopt(*this, mName);
			}
			else if (mOwner->Is("Entity"))
			{
				mLocalID = mOwner->As<Entity>()->GetActions()->Adopt(*this, mName);
			}
			else if (mOwner->Is("ActionList"))
			{
				mLocalID = mOwner->As<ActionList>()->GetActions()->Adopt(*this, mName);
			}
		}
		else
		{
			mLocalID = UINT32_MAX;
		}
	}

	void Action::SetActive(int state)
	{
		Find("isActive")->Set(state);
	}

	int Action::IsActive()
	{
		return Find("isActive")->Get<int>();
	}

}