#include "pch.h"
#include "ActionDestroyAction.h"

namespace Library
{
	RTTI_DEFINITIONS(ActionDestroyAction);

	const std::string ActionDestroyAction::sDeleteActionAttribute = "destroyActionName";

	ActionDestroyAction::ActionDestroyAction(const std::string& name)
		: Action(name)
	{
		Populate();
	}

	void ActionDestroyAction::Populate()
	{
		Action::Populate();

		std::string deleteAction = "";

		INIT_SIGNATURE();

		INTERNAL_ATTRIBUTE(sDeleteActionAttribute, Datum::DatumType::String, &deleteAction, 1);

		Attributed::Populate();
	}

	void ActionDestroyAction::SetDeleteAction(const std::string& deleteActionName)
	{
		Find(sDeleteActionAttribute)->Set(deleteActionName);
	}

	const std::string& ActionDestroyAction::GetDeleteAction() const
	{
		return Find(sDeleteActionAttribute)->Get<std::string>();
	}

	void ActionDestroyAction::Update(WorldState& state)
	{
		if (mOwner == nullptr)
		{
			throw std::runtime_error("Cannot delete an action without a parent to delete it from.");
		}

		Datum* findDeleteActionResult = Find(sDeleteActionAttribute);

		if (findDeleteActionResult == nullptr || findDeleteActionResult->Get<std::string>() == "")
		{
			throw std::runtime_error("Cannot delete an action without knowing its name.");
		}

		if (mName == findDeleteActionResult->Get<std::string>())
		{
			throw std::runtime_error("Cannot delete self.");
		}

		findDeleteActionResult = GetParent()->Find(findDeleteActionResult->Get<std::string>());

		if (findDeleteActionResult == nullptr)
		{
			// Couldn't find it, so exit early.
			return;
		}

		if (findDeleteActionResult->GetType() != Datum::DatumType::Table)
		{
			throw std::runtime_error("Attempted to destroy an object that was not an action.");
		}

		if (findDeleteActionResult->GetSize() > 0)
		{
			if (!findDeleteActionResult->GetTable()->Is("Action"))
			{
				throw std::runtime_error("Attempted to destroy an object that was not an action.");
			}

			if (!findDeleteActionResult->IsExternal())
			{
				for (std::uint32_t i = 0; i < findDeleteActionResult->GetSize(); ++i)
				{
					delete findDeleteActionResult->GetTable(i);
				}
			}

			findDeleteActionResult->Clear();
		}

	}

}