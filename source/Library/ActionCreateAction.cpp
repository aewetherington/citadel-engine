#include "pch.h"
#include "ActionCreateAction.h"

namespace Library
{
	RTTI_DEFINITIONS(ActionCreateAction);

	const std::string ActionCreateAction::sPrototypeNameAttribute = "prototypeName";
	const std::string ActionCreateAction::sPrototypeClassAttribute = "prototypeClass";

	ActionCreateAction::ActionCreateAction(const std::string& name)
		: Action(name)
	{
		Populate();
	}

	void ActionCreateAction::Populate()
	{
		Action::Populate();

		std::string nameAttrib = "", classAttrib = "";

		INIT_SIGNATURE();

		INTERNAL_ATTRIBUTE(sPrototypeNameAttribute, Datum::DatumType::String, &nameAttrib, 1);
		INTERNAL_ATTRIBUTE(sPrototypeClassAttribute, Datum::DatumType::String, &classAttrib, 1);

		Attributed::Populate();
	}

	void ActionCreateAction::SetPrototypeName(const std::string& prototypeName)
	{
		Find(sPrototypeNameAttribute)->Set(prototypeName);
	}

	const std::string& ActionCreateAction::GetPrototypeName() const
	{
		return Find(sPrototypeNameAttribute)->Get<std::string>();
	}

	void ActionCreateAction::SetPrototypeClass(const std::string& prototypeClass)
	{
		Find(sPrototypeClassAttribute)->Set(prototypeClass);
	}

	const std::string& ActionCreateAction::GetPrototypeClass() const
	{
		return Find(sPrototypeClassAttribute)->Get<std::string>();
	}

	void ActionCreateAction::Update(WorldState& state)
	{
		if (mOwner == nullptr)
		{
			throw std::runtime_error("Cannot create an action without a parent to attach it to.");
		}

		Datum* findNameResult = Find(sPrototypeNameAttribute);
		Datum* findClassResult = Find(sPrototypeClassAttribute);

		if (findNameResult == nullptr || findNameResult->Get<std::string>() == "")
		{
			throw std::runtime_error("Cannot create an action without a name to give it.");
		}

		if (findNameResult == nullptr || findClassResult->Get<std::string>() == "")
		{
			throw std::runtime_error("Cannot create an action without knowing the action class to make.");
		}

		Action* newAction = Factory<Action>::Create(findClassResult->Get<std::string>());

		if (newAction == nullptr)
		{
			throw std::runtime_error("Could not instantiate Action of type " + findClassResult->Get<std::string>());
		}

		newAction->SetName(findNameResult->Get<std::string>());
		newAction->SetOwner(mOwner);
	}

}