namespace Library
{

#pragma region Iterator

	template <typename T>
	Vector<T>::Iterator::Iterator()
		: mOwner(nullptr), mIndex(0)
	{
	}

	template <typename T>
	Vector<T>::Iterator::Iterator(const Iterator& rhs)
		: Iterator()
	{
		operator=(rhs);
	}

	template <typename T>
	Vector<T>::Iterator::Iterator(Vector *owner, std::uint32_t index)
		: Iterator()
	{
		mOwner = owner;
		mIndex = index;
	}

	template <typename T>
	typename Vector<T>::Iterator& Vector<T>::Iterator::operator=(const Iterator& rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		mOwner = rhs.mOwner;
		mIndex = rhs.mIndex;

		return *this;
	}

	template <typename T>
	bool Vector<T>::Iterator::operator==(const Iterator& rhs) const
	{
		return (mOwner == rhs.mOwner) && (mIndex == rhs.mIndex);
	}

	template <typename T>
	bool Vector<T>::Iterator::operator!=(const Iterator& rhs) const
	{
		return !operator==(rhs);
	}

	template <typename T>
	typename Vector<T>::Iterator& Vector<T>::Iterator::operator++()
	{
		if (mOwner != nullptr && mIndex < mOwner->mSize)
		{
			mIndex++;
		}

		return *this;
	}

	template <typename T>
	typename Vector<T>::Iterator Vector<T>::Iterator::operator++(int)
	{
		Iterator itCopy = *this;

		if (mOwner != nullptr && mIndex < mOwner->mSize)
		{
			mIndex++;
		}

		return itCopy;
	}

	template <typename T>
	T& Vector<T>::Iterator::operator*() const
	{
		if (mOwner != nullptr && mIndex < mOwner->mSize)
		{
			return (*mOwner)[mIndex];
		}
		else
		{
			throw std::runtime_error("Iterator dereferenced while not pointing to any data.");
		}
	}

#pragma endregion

	template <typename T>
	Vector<T>::Vector(std::uint32_t capacity)
		: mCapacity(0), mSize(0), mData(nullptr)
	{
		Reserve(capacity);
	}

	template <typename T>
	Vector<T>::Vector(const Vector& rhs)
		: Vector()
	{
		operator=(rhs);
	}

	template <typename T>
	Vector<T>::Vector(Vector&& rhs)
		: mCapacity(rhs.mCapacity), mSize(rhs.mSize), mData(rhs.mData)
	{
		rhs.mCapacity = 0;
		rhs.mSize = 0;
		rhs.mData = nullptr;
		rhs.Reserve(rhs.mCapacity);
	}

	template <typename T>
	Vector<T>::~Vector()
	{
		Clear();
	}

	template <typename T>
	Vector<T>& Vector<T>::operator=(const Vector& rhs)
	{
		// Do a self-test.
		if (this == &rhs)
		{
			return *this;
		}

		// First, clean up what might already exist.
		Clear();

		// Allocate the memory needed
		Reserve(rhs.mCapacity);

		// Copy the data
		for (std::uint32_t i = 0; i < rhs.mSize; ++i)
		{
			mData[i] = rhs.mData[i];
		}

		mSize = rhs.mSize;

		return *this;
	}

	template <typename T>
	Vector<T>& Vector<T>::operator=(Vector&& rhs)
	{
		// Do a self-test.
		if (this == &rhs)
		{
			return *this;
		}

		// First, clean up what might already exist.
		Clear();

		mCapacity = rhs.mCapacity;
		mSize = rhs.mSize;
		mData = rhs.mData;

		rhs.mCapacity = 0;
		rhs.mSize = 0;
		rhs.mData = nullptr;
		rhs.Reserve(rhs.mCapacity);

		return *this;
	}

	template <typename T>
	T& Vector<T>::operator[](std::uint32_t index)
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of bounds.");
		}
		else
		{
			return mData[index];
		}
	}

	template <typename T>
	const T& Vector<T>::operator[](std::uint32_t index) const
	{
		if (index >= mSize)
		{
			throw std::runtime_error("Index out of bounds.");
		}
		else
		{
			return mData[index];
		}
	}

	template <typename T>
	T& Vector<T>::PopBack()
	{
		if (mSize == 0)
		{
			throw std::runtime_error("PopBack() called on an empty vector.");
		}
		else
		{
			mSize--;
			return mData[mSize];
		}
	}

	template <typename T>
	bool Vector<T>::IsEmpty() const
	{
		return mSize == 0;
	}

	template <typename T>
	T& Vector<T>::Front()
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Front() called on an empty vector.");
		}
		else
		{
			return mData[0];
		}
	}

	template <typename T>
	const T& Vector<T>::Front() const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Front() called on an empty vector.");
		}
		else
		{
			return mData[0];
		}
	}

	template <typename T>
	T& Vector<T>::Back()
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Back() called on an empty vector.");
		}
		else
		{
			return mData[mSize-1];
		}
	}

	template <typename T>
	const T& Vector<T>::Back() const
	{
		if (mSize == 0)
		{
			throw std::runtime_error("Back() called on an empty vector.");
		}
		else
		{
			return mData[mSize - 1];
		}
	}

	template <typename T>
	std::uint32_t Vector<T>::GetSize() const
	{
		return mSize;
	}

	template <typename T>
	typename Vector<T>::Iterator Vector<T>::begin() const
	{
		Iterator it(const_cast<Vector<T>*>(this), 0);

		return it;
	}

	template <typename T>
	typename Vector<T>::Iterator Vector<T>::end() const
	{
		Iterator it(const_cast<Vector<T>*>(this), mSize);

		return it;
	}

	template <typename T>
	void Vector<T>::PushBack(const T& data)
	{
		if (mSize == mCapacity)
		{
			Reserve(mCapacity * 2);
		}

		mData[mSize] = data;
		++mSize;
	}

	template <typename T>
	T& Vector<T>::At(std::uint32_t index)
	{
		if (index >= mCapacity)
		{
			throw std::runtime_error("Index reaches past vector capacity.");
		}

		if (index >= mSize)
		{
			mSize = index + 1;
		}

		return mData[index];
	}

	template <typename T>
	void Vector<T>::Reserve(std::uint32_t capacity)
	{
		// Always reserve at least one.
		capacity = std::max(capacity, 1U);

		if (capacity <= mCapacity)
		{
			return;
		}

		T* newData;
		if (capacity > 0)
		{
			newData = new T[capacity]();
		}
		else
		{
			newData = nullptr;
		}

		// Copy the data. Should not run if mData is null, because mSize is 0.
		for (uint32_t i = 0; i < mSize; ++i)
		{
			newData[i] = mData[i];
		}

		if (mData != nullptr)
		{
			delete[] mData;
		}

		mData = newData;
		mCapacity = capacity;
	}

	template <typename T>
	typename Vector<T>::Iterator Vector<T>::Find(const T& data) const
	{
		Iterator it = begin();
		for (; it != end(); ++it)
		{
			if (data == *it)
			{
				return it;
			}
		}

		// Will return end() if it wasn't found.
		return it;
	}

	template <typename T>
	void Vector<T>::Clear()
	{
		if (mData != nullptr)
		{
			delete[] mData;
			mData = nullptr;
		}
		mCapacity = 0;
		mSize = 0;
	}
}