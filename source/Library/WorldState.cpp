#include "pch.h"
#include "WorldState.h"

namespace Library
{

	WorldState::WorldState()
		: mTime()
	{
		
	}

	WorldState::~WorldState()
	{
		// TODO
	}

	GameTime& WorldState::GetGameTime()
	{
		return mTime;
	}

	void WorldState::SetGameTime(const GameTime& time)
	{
		mTime.SetTotalGameTime(time.GetTotalGameTime());
		mTime.SetElapsedGameTime(time.GetElapsedGameTime());
	}

}