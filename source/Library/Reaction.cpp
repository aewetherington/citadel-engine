#include "pch.h"
#include "Reaction.h"

using namespace std;

namespace Library
{
	RTTI_DEFINITIONS(Reaction);

	Reaction::Reaction(const std::string& name)
		: ActionList(name)
	{

	}

	void Reaction::Populate()
	{
		// Just here to keep the hierarchy of Populate calls consistent.
	}

	void Reaction::Update(WorldState& state)
	{
		// Do nothing. Disallows standard update calls from doing anything.
	}

	void Reaction::Notify(shared_ptr<EventPublisher> eventPublisher)
	{

	}
}