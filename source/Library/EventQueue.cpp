#include "pch.h"
#include "EventQueue.h"
#include "EventPublisher.h"
#include <list>

using namespace std;
using namespace std::chrono;

namespace Library
{
	void EventQueue::Enqueue(std::shared_ptr<EventPublisher> eventPublisher, const high_resolution_clock::time_point& currentTime, const std::chrono::milliseconds& delay)
	{
		if (delay == std::chrono::milliseconds(0))
		{
			Send(eventPublisher);
		}
		else
		{
			eventPublisher->SetTime(currentTime, delay);
			{
				lock_guard<mutex> lock(mQueueMutex);
				mQueue.PushBack(eventPublisher);
			}
		}
	}

	void EventQueue::Send(std::shared_ptr<EventPublisher> eventPublisher)
	{
		eventPublisher->Deliver(eventPublisher);
	}

	void EventQueue::Update(const high_resolution_clock::time_point& currentTime)
	{
		// Only allow one instance of the Update function to run at any given time.
		if (!bIsUpdating.exchange(true))
		{
			list<future<void>> futures;

			{
				lock_guard<mutex> lock(mQueueMutex);
				SList<std::shared_ptr<EventPublisher>> deleteFromQueue;

				// For all queued events
				for (auto& publisher : mQueue)
				{
					if (publisher->IsPrimed(currentTime))
					{
						// Deliver and set to be cleared later.
						//publisher->Deliver(publisher);
						futures.emplace_back(async(&EventPublisher::Deliver, publisher, publisher));
						deleteFromQueue.PushBack(publisher);
					}
				}

				// Clean up anything that was delivered from the queue.
				for (auto& publisher : deleteFromQueue)
				{
					mQueue.Remove(publisher);
				}
			}

			// Now wait for threads to exit, re-throwing exceptions to the current context.
			for (auto& f : futures)
			{
				f.get();
			}

			// Signal the end of the update.
			bIsUpdating.store(false);
		}
	}

	void EventQueue::Clear()
	{
		// Send any primed events.
		Update(high_resolution_clock::now());

		// Empty the events not ready to be sent.
		{
			lock_guard<mutex> lock(mQueueMutex);
			mQueue.Clear();
		}
	}

	bool EventQueue::IsEmpty() const
	{
		lock_guard<mutex> lock(mQueueMutex);
		return mQueue.IsEmpty();
	}

	uint32_t EventQueue::GetSize() const
	{
		lock_guard<mutex> lock(mQueueMutex);
		return mQueue.GetSize();
	}

}