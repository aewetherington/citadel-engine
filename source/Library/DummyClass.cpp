#include "pch.h"

#include "DummyClass.h"

bool DummyClass::isPositive(int val) {
	return val >= 0;
}

bool DummyClass::isEven(int val) {
	return val % 2 == 0;
}