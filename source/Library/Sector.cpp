#include "pch.h"
#include "Sector.h"
#include "Entity.h"
#include "World.h"
#include "WorldState.h"
#include "Factory.h"

namespace Library
{
	RTTI_DEFINITIONS(Sector);

	Sector::Sector(const std::string& name)
		: Attributed(), mName(name), mEntities(new Scope()), mActions(new Scope()), mLocalID(UINT32_MAX)
	{
		Populate();
	}

	const std::string& Sector::GetName() const
	{
		return mName;
	}

	void Sector::Populate()
	{
		int isActive = 1;
		INIT_SIGNATURE();

		EXTERNAL_ATTRIBUTE("name", Datum::DatumType::String, &mName, 1);

		INTERNAL_ATTRIBUTE("entities", Datum::DatumType::Table, &mEntities, 1);
		INTERNAL_ATTRIBUTE("actions", Datum::DatumType::Table, &mActions, 1);
		INTERNAL_ATTRIBUTE("isActive", Datum::DatumType::Integer, &isActive, 1);

		Attributed::Populate();
	}

	Scope* Sector::GetActions()
	{
		return mActions;
	}

	Action* Sector::CreateAction(const std::string& actionClassName, const std::string& actionInstanceName)
	{
		Action* newAction = Factory<Action>::Create(actionClassName);

		if (newAction == nullptr)
		{
			throw std::runtime_error("Failed to instantiate Action of type " + actionClassName);
		}

		newAction->SetName(actionInstanceName);
		newAction->SetOwner(this);
		return newAction;
	}

	void Sector::SetName(const std::string& name)
	{
		Orphan();
		mName = name;
		if (mWorld != nullptr)
		{
			mLocalID = mWorld->GetSectors()->Adopt(*this, mName);
		}
		else
		{
			mLocalID = UINT32_MAX;
		}
	}

	std::string Sector::GetGUID() const
	{
		if (mWorld != nullptr)
		{
			return mWorld->GetGUID() + "_" + mName + "_" + std::to_string(mLocalID);
		}
		else
		{
			return mName + "_" + std::to_string(mLocalID);
		}
	}

	Scope* Sector::GetEntities() const
	{
		return mEntities;
	}

	Entity* Sector::CreateEntity(const std::string& entityClassName, const std::string& entityInstanceName)
	{
		Entity* newEntity = Factory<Entity>::Create(entityClassName);

		if (newEntity == nullptr)
		{
			throw std::runtime_error("Failed to instantiate Entity of type " + entityClassName);
		}

		newEntity->SetName(entityInstanceName);
		newEntity->SetSector(this);
		return newEntity;
	}

	void Sector::SetWorld(World* newWorld)
	{
		Orphan();
		mWorld = newWorld;
		if (mWorld != nullptr)
		{
			mLocalID = mWorld->GetSectors()->Adopt(*this, mName);
		}
		else
		{
			mLocalID = UINT32_MAX;
		}
	}

	World* Sector::GetWorld() const
	{
		return mWorld;
	}

	void Sector::SetActive(int state)
	{
		Find("isActive")->Set(state);
	}

	int Sector::IsActive()
	{
		return Find("isActive")->Get<int>();
	}

	void Sector::Update(WorldState& state)
	{
		// For each set of entities with matching names
		for (std::uint32_t i = 0; i != mEntities->GetSize(); ++i)
		{
			Datum& sameNameEntities = (*mEntities)[i];

			// For each entity with a given matching name
			for (std::uint32_t j = 0; j != sameNameEntities.GetSize(); ++j)
			{
				// Call their update function.
				state.Entity = sameNameEntities.GetTable(j)->As<Entity>();
				if (state.Entity == nullptr)
				{
					throw std::runtime_error("Non-Entity found in " + GetGUID() + "'s Entity list.");
				}
				if (state.Entity->Find("isActive")->Get<int>())
				{
					state.Entity->Update(state);
				}
			}
		}

		state.Entity = nullptr;

		// For each set of actions with matching names
		for (std::uint32_t i = 0; i != mActions->GetSize(); ++i)
		{
			Datum& sameNameActions = (*mActions)[i];

			// For each action with a given matching name
			for (std::uint32_t j = 0; j != sameNameActions.GetSize(); ++j)
			{
				// Call their update function.
				state.Action = sameNameActions.GetTable(j)->As<Action>();
				if (state.Action == nullptr)
				{
					throw std::runtime_error("Non-Action found in " + GetGUID() + "'s Action list.");
				}
				if (state.Action->Find("isActive")->Get<int>())
				{
					state.Action->Update(state);
				}
			}
		}

		state.Action = nullptr;
	}
}