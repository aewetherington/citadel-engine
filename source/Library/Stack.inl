namespace Library
{
	template <typename T>
	Stack<T>::Stack()
		: mList()
	{

	}

	template <typename T>
	Stack<T>::~Stack()
	{
	}

	template <typename T>
	Stack<T>::Stack(const Stack& rhs)
		: mList(rhs.mList)
	{
	}

	template <typename T>
	Stack<T>& Stack<T>::operator=(const Stack& rhs)
	{
		// Do a self-test.
		if (this == &rhs)
		{
			return *this;
		}

		// First, clean up what might already exist.
		Clear();

		// Copy the data.
		mList = rhs.mList;

		return *this;
	}

	template <typename T>
	void Stack<T>::Push(const T& data)
	{
		mList.PushFront(data);
	}

	template <typename T>
	T Stack<T>::Pop()
	{
		try
		{
			return mList.PopFront();
		}
		catch (std::runtime_error e)
		{
			throw std::runtime_error("Stack is empty.");
		}
	}

	template <typename T>
	T& Stack<T>::Top()
	{
		try
		{
			return mList.Front();
		}
		catch (std::runtime_error e)
		{
			throw std::runtime_error("Stack is empty.");
		}
	}

	template <typename T>
	const T& Stack<T>::Top() const
	{
		try
		{
			return mList.Front();
		}
		catch (std::runtime_error e)
		{
			throw std::runtime_error("Stack is empty.");
		}
	}

	template <typename T>
	std::uint32_t Stack<T>::GetSize() const
	{
		return mList.GetSize();
	}

	template <typename T>
	bool Stack<T>::IsEmpty() const
	{
		return mList.IsEmpty();
	}

	template <typename T>
	void Stack<T>::Clear()
	{
		mList.Clear();
	}
}