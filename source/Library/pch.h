#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <iostream>
#include <cstdint>
#include <cassert>
#include <algorithm>
#include <chrono>
#include <memory>
#include <thread>
#include <mutex>
#include <future>
#include <atomic>

#include "RTTI.h"
#include "Types.h"
#include "GameTime.h"
#include "SList.h"
#include "Stack.h"
#include "Vector.h"
#include "HashMap.h"
#include "Datum.h"
//#include "Scope.h" // Adding these breaks things. Why?
//#include "Factory.h" 

/** \namespace Library
	\brief Citadel Engine's library namespace.
*/
namespace Library
{
}