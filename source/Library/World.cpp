#include "pch.h"
#include "World.h"
#include "Sector.h"
#include "WorldState.h"
#include "Factory.h"

namespace Library
{
	RTTI_DEFINITIONS(World);

	World::World(const std::string& name)
		: Attributed(), mName(name), mSectors(new Scope()), mActions(new Scope()), mClock(), mEventQueue()
	{
		Populate();
	}

	void World::Populate()
	{
		INIT_SIGNATURE();

		EXTERNAL_ATTRIBUTE("name", Library::Datum::DatumType::String, &mName, 1);

		INTERNAL_ATTRIBUTE("sectors", Datum::DatumType::Table, &mSectors, 1);
		INTERNAL_ATTRIBUTE("actions", Datum::DatumType::Table, &mActions, 1);

		Attributed::Populate();
	}

	Scope* World::GetActions()
	{
		return mActions;
	}

	Action* World::CreateAction(const std::string& actionClassName, const std::string& actionInstanceName)
	{
		Action* newAction = Factory<Action>::Create(actionClassName);

		if (newAction == nullptr)
		{
			throw std::runtime_error("Failed to instantiate Action of type " + actionClassName);
		}

		newAction->SetName(actionInstanceName);
		newAction->SetOwner(this);
		return newAction;
	}

	const std::string& World::GetName() const
	{
		return mName;
	}

	void World::SetName(const std::string& name)
	{
		mName = name;
	}

	std::string World::GetGUID() const
	{
		return mName;
	}

	Scope* World::GetSectors() const
	{
		return mSectors;
	}

	Sector* World::CreateSector(const std::string& sectorName)
	{
		Sector* newSector = new Sector(sectorName);
		newSector->SetWorld(this);
		return newSector;
	}

	EventQueue& World::GetEventQueue()
	{
		return mEventQueue;
	}

	void World::Update(WorldState& state)
	{
		mWorldState = &state;

		// Update what's being processed.
		state.World = this;

		// Update the game clock.
		auto& gameTime = state.GetGameTime();
		mClock.UpdateGameTime(gameTime);

		// Update the event queue!
		mEventQueue.Update(gameTime.GetCurrentTime());

		// For each set of sectors with matching names
		for (std::uint32_t i = 0; i != mSectors->GetSize(); ++i)
		{
			Datum& sameNameSectors = (*mSectors)[i];

			// For each entity with a given matching name
			for (std::uint32_t j = 0; j != sameNameSectors.GetSize(); ++j)
			{
				// Call their update function.
				state.Sector = sameNameSectors.GetTable(j)->As<Sector>();
				if (state.Sector == nullptr)
				{
					throw std::runtime_error("Non-Sector found in " + GetGUID() + "'s Sector list.");
				}
				if (state.Sector->Find("isActive")->Get<int>())
				{
					state.Sector->Update(state);
				}
			}
		}

		state.Sector = nullptr;

		// For each set of actions with matching names
		for (std::uint32_t i = 0; i != mActions->GetSize(); ++i)
		{
			Datum& sameNameActions = (*mActions)[i];

			// For each action with a given matching name
			for (std::uint32_t j = 0; j != sameNameActions.GetSize(); ++j)
			{
				// Call their update function.
				state.Action = sameNameActions.GetTable(j)->As<Action>();
				if (state.Action == nullptr)
				{
					throw std::runtime_error("Non-Action found in " + GetGUID() + "'s Action list.");
				}
				if (state.Action->Find("isActive")->Get<int>())
				{
					state.Action->Update(state);
				}
			}
		}

		state.Action = nullptr;
	}

	WorldState* World::GetWorldState() const
	{
		return mWorldState;
	}

}