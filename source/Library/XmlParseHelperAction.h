#pragma once

/*!	\file XmlParseHelperAction.h
	\brief Contains the declaration for XmlParseHelperAction, which parses the action tag in XML files.
*/

#include "IXmlParseHelper.h"
#include "SharedDataTable.h"

namespace Library
{
	/*!	\brief Class that can parse the action tag in XML files.
	*/
	class XmlParseHelperAction : public IXmlParseHelper
	{
	public:

		/*!	\brief Initializes this instance to a default state.
		*/
		XmlParseHelperAction() = default;

		/*!	\brief Cleans up any owned data.
		*/
		virtual ~XmlParseHelperAction() = default;

		/*!	\brief Handler for XML object start tags, called by the XmlParseMaster that is using this helper.
			The name tag for Actions is optional. If it is not provided, the action will have its default name.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper. If no SharedData object is associated, the value is nullptr.
			@param name The name of the tag.
			@param atttributes A HashMap containing name/value pairs of attributes listed in the XML start tag.
			@throws std::runtime_error Thrown if a reserved term conflights with usage in the XML.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes) override;

		/*!	\brief Handler for XML object end tags, called by the XmlParseMaster that is using this helper. If no SharedData object is associated, the value is nullptr.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper.
			@param name The name of the tag.
			@throws std::runtime_error Thrown if the depth is invalid for a given tag.
			@throws std::runtime_error Thrown if a given tag's parent tag is invalid.
			@throws std::runtime_error Thrown if no data is provided for a type that requires data.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name) override;

		/*!	\brief Generates a deep copy of this XmlParseHelperTable on the heap, prepares it for usage, and returns a pointer to it.

			User is responsible for calling delete on the returned value once finished with it, unless it is owned by a cloned XmlParseMaster.
			@return Address of a new parse helper.
		*/
		virtual IXmlParseHelper* Clone() const override;

	};

}