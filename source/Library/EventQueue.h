#pragma once

/*!	\file EventQueue.h
	\brief Contains the declaration for an event queue.
*/

#include "pch.h"
#include "WorldState.h"
#include "SList.h"

namespace Library
{
	class EventPublisher;

	/*!	\brief An event queue that manages events and delivers them to their subscribers.
	*/
	class EventQueue final
	{
	public:
		/*!	\brief Enqueues an event based on the parameter list.
			If no delay is specified, delivers the event immediately.
			@param eventPublisher The event to enqueue.
			@param currentTime The current timestamp.
			@param delay The amount of time to delay the delivery.
		*/
		void Enqueue(std::shared_ptr<EventPublisher> eventPublisher, const std::chrono::high_resolution_clock::time_point& currentTime, const std::chrono::milliseconds& delay = std::chrono::milliseconds(0));

		/*!	\brief Immediately sends an event to all of its subscribers.
			@param eventPublisher The event to publish.
		*/
		void Send(std::shared_ptr<EventPublisher> eventPublisher);

		/*!	\brief Updates the queue based on the timestamp, delivering any events that become deliverable.
			@param currentTime The current timestamp.
		*/
		void Update(const std::chrono::high_resolution_clock::time_point& currentTime);

		/*!	\brief Empties out the event queue, delivering any events that are deliverable at the time of the call.
		*/
		void Clear();

		/*!	\brief Returns whether the event queue is empty or not.
			@return True if the queue is empty.
		*/
		bool IsEmpty() const;

		/*!	\brief Returns the number of items in the event queue.
			@return The number of items in the event queue.
		*/
		std::uint32_t GetSize() const;

	private:
		// The event queue.
		SList<std::shared_ptr<EventPublisher>> mQueue;
		mutable std::mutex mQueueMutex;

		std::atomic<bool> bIsUpdating;
	};

}