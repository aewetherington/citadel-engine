#pragma once

/*!	\file Vector.h
	\brief Contains the declarations for a templated Vector class.
*/

#include "pch.h"

namespace Library
{
	/*!	\class Vector
		\brief Templated implementation of a vector.

		When using this class, ownership of memory does not transfer to the vector. User is responsible for deleting previously allocated memory when finished with it.
	*/
	template <typename T>
	class Vector final
	{
	public:
		/*!	\class Iterator
			\brief Used to simplify manipulating the Vector.
		*/
		class Iterator
		{
			friend class Vector;

		public:
			/*!	\fn Iterator()
				\brief Initializes the iterator to default values, not associated with any list.
			*/
			Iterator();

			/*! \fn Iterator(const Iterator& rhs)
				\brief Copy constructor that sets the iterator's values to be the same as the paramater's. Performs a shallow copy.
				@param rhs The iterator to be copied.
				@see operator=()
			*/
			Iterator(const Iterator& rhs);

			/*! \fn Iterator& operator=(const Iterator& rhs)
				\brief Sets the iterator's values to be the same as the paramter's. Performs a shallow copy.
				@param rhs The iterator to be copied.
				@return Reference to the new iterator.
				@see Iterator(const Iterator& rhs)
			*/
			Iterator& operator=(const Iterator& rhs);

			/*!	\fn operator==(const Iterator& rhs) const
				\brief Compares the two iterators to see if they are pointing to the same data.
				@param rhs The iterator to be compared to this one.
				@return True only if the two iterators are equivalent.
				@see operator!=()
			*/
			bool operator==(const Iterator& rhs) const;

			/*! \fn operator!=(const Iterator& rhs) const
				\brief Compares the two iterators to see if they are pointing to different data.
				@param rhs The iterator to be compared to this one.
				@return True only if the two iterators are inequivalent.
				@see operator==()
			*/
			bool operator!=(const Iterator& rhs) const;

			/*! \fn operator++()
				\brief Pre-increment operator that moves the iterator to point to the next element in the list, and returns the new iterator.
				@return The iterator after it has been incremented.
				@see operator++(int)
			*/
			Iterator& operator++();

			/*! \fn operator++(int)
				\brief Post-increment operator that moves the iterator to point to the next element in the list, and returns a copy of the old iterator.
				@return A copy of the iterator before it has been incremented.
				@see operator++()
			*/
			Iterator operator++(int);

			/*!	\fn operator*() const
				\brief Retrieves the data at the location pointed to by this iterator, and returns it.
				@return The data associated with this iterator.
				@exception std::runtime_error Thrown if called on an iterator not pointing to any data.
			*/
			T& operator*() const;

		private:
			Iterator(Vector *owner, std::uint32_t index);
			Vector* mOwner;
			std::uint32_t mIndex;
		};

		/*! \fn Vector(std::uint32_t capacity = 32)
			\brief Initializes the vector to be empty.

			Provides a default vector capacity if none is specified.

			@param capacity The size of elements the vector is capable of holding.
		*/
		explicit Vector(std::uint32_t capacity = 32);

		/*!	\fn Vector(const Vector& rhs)
			\brief Performs a deep copy of the vector.

			@param rhs The vector to be copied.
			@see operator=(const Vector&)
		*/
		Vector(const Vector& rhs);

		/*!	\fn Vector(Vector&& rhs)
			\brief Performs a move of the vector contents.
				
			@param rhs The vector to be moved.
			@see operator=(Vector&&)
		*/
		Vector(Vector&& rhs);

		/*!	\fn ~Vector()
			\brief Empties the vector and frees associated memory.

			Clears the vector of all data. User is responsible for any neccessary memory management brought on by their choice of T.
			@see Clear()
		*/
		~Vector();

		/*!	\fn operator=(const Vector& rhs)
			\brief Performs a deep copy of the vector.

			@param rhs The vector to be copied.
			@return The copied list.
			@see Vector(const Vector&);
		*/
		Vector& operator=(const Vector& rhs);

		/*!	\fn operator=(Vector&& rhs)
			\brief Performs a move of the vector contents.

			@param rhs The vector to be moved.
			@return The moved list.
			@see Vector(Vector&&);
		*/
		Vector& operator=(Vector&& rhs);

		/*!	\fn operator[](std::uint32_t index)
			\brief Accesses and returns a reference to a T at the location specified in the vector.

			@param index The location at which to find the T object.
			@return A reference to the data at the specified index.
			@exception std::runtime_error Thrown if index is outside [0, vectorSize).
			@see operator[](std::uint32_t) const
			@see At(std::uint32_t)
		*/
		T& operator[](std::uint32_t index);

		/*!	\fn operator[](std::uint32_t index) const
			\brief Accesses and returns a reference to a T at the location specified in the vector.

			@param index The location at which to find the T object.
			@return A reference to the data at the specified index.
			@exception std::runtime_error Thrown if index is outside [0, vectorSize).
			@see operator[](std::uint32_t)
			@see At(std::uint32_t)
		*/
		const T& operator[](std::uint32_t index) const;

		/*!	\fn PopBack()
			\brief Removes the last item in the vector, and returns it.

			User is responsible for any neccessary memory management brought on by their choice of T.

			@return Last data member in the vector.
			@exception std::runtime_error Thrown if the function is called with no elements in the vector.
			@see PushBack()
			@see Back()
			@see Back() const
			@see Front()
			@see Front() const
		*/
		T& PopBack();

		/*!	\fn IsEmpty() const
			\brief Returns whether vector is empty or not.

			@return True if size is zero, and false otherwise.
			@see GetSize()
		*/
		bool IsEmpty() const;

		/*!	\fn Front()
			\brief Returns the first element in the vector.

			@return First element in the vector.
			@exception std::runtime_error Thrown if the size of the vector is zero.
			@see Front() const
			@see Back()
			@see Back() const
		*/
		T& Front();

		/*!	\fn Front() const
			\brief Returns the first element in the vector.

			@return First element in the vector.
			@exception std::runtime_error Thrown if the size of the vector is zero.
			@see Front()
			@see Back()
			@see Back() const
		*/
		const T& Front() const;

		/*!	\fn Back()
			\brief Returns the last element in the vector.

			@return Last element in the vector.
			@exception std::runtime_error Thrown if the size of the vector is zero.
			@see Back() const
			@see Front()
			@see Front() const
		*/
		T& Back();

		/*!	\fn Back() const
			\brief Returns the last element in the vector.

			@return Last element in the vector.
			@exception std::runtime_error Thrown if the size of the vector is zero.
			@see Back()
			@see Front()
			@see Front() const
		*/
		const T& Back() const;

		/*!	\fn GetSize() const
			\brief Returns the number of elements currently initialized in the vector.
			
			@return The number of initialized elements.
			@see IsEmpty()
		*/
		std::uint32_t GetSize() const;

		/*! \fn begin() consnt
			\brief Returns an iterator to the beginning of the vector.

			@return Iterator to the beginning of the vector.
		*/
		Iterator begin() const;

		/*! \fn end() const
			\brief Returns an iterator one element past the end of the vector.

			@return Iterator to one element past the end of the vector.
		*/
		Iterator end() const;

		/*!	\fn PushBack(const T& data)
			\brief Appends the given data to the end of the vector.

			Will increase the capacity of the vector if necessary.
			@param data The data to be appended.
			@see PopBack()
			@see Back()
			@see Back() const
		*/
		void PushBack(const T& data);

		/*!	\fn At(std::uint32_t index)
			\brief Retrieve the data at the specified index.

			Will increase the size of the vector if necessay and initialize any new elements created as a result, but will not increase the capacity.
			@param index The location of the data to retrieve. 
			@return The data at the specified index.
			@exception std::runtime_error Thrown if the index is outside the range [0, capacity).
			@see operator[](std::uint32_t)
			@see operator[](std::uint32_t) const
		*/
		T& At(std::uint32_t index);

		/*!	\fn Reserve(std::uint32_t capacity)
			\brief Reserves memory for the number of elements specified by the capacity parameter.

			Will preserve any elements currently existing in the vector.
			@param capacity Number of elements to reserve memory for.
			@see Clear()
		*/
		void Reserve(std::uint32_t capacity);

		/*!	\fn Find(const T& data) const
			\brief Returns an iterator at the location of the first occurence of the specified data.

			If the data is not found, returns an iterator at the end of the list.
			@param data The data to search for.
			@return Iterator at the first occurence of the search key, or at the end of the list if the search fails.
		*/
		Iterator Find(const T& data) const;

		/*!	\fn Clear()
			\brief Clears all of the data in the vector.

			Shrinks the vector capacity back to its initial capacity. User is responsible for managing any memory brought on by their choice of T.
			@see ~Vector()
			@see Reserve(std::uint32_t)
		*/
		void Clear();

	private:
		std::uint32_t mCapacity;
		std::uint32_t mSize;
		T* mData;
	};
}

#include "Vector.inl"