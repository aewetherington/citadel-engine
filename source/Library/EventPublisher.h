#pragma once

/*!	\file EventPublisher.h
*/

#include "pch.h"

namespace Library
{
	class EventSubscriber;

	/*!	\brief Base class for events, which is responsible for actually publishing events.
	*/
	class EventPublisher : public RTTI
	{
		RTTI_DECLARATIONS(EventPublisher, RTTI);

	public:
		/*!	\brief Constructs a new EventPublisher based on the parameters.
			@param subscribers A pointer to the list of subscribers for this event.
		*/
		explicit EventPublisher(SList<EventSubscriber*>& subscribers, std::mutex& subscribersMutex);

		/*!	\brief Destructs the EventPublisher, freeing any owned memory.
		*/
		virtual ~EventPublisher() = default;
		
		/*!	\brief Sets the time information for this EventPublisher.
			@param currentTime The current timestamp.
			@param delay The amount of time this EventPublisher waits before being published.
		*/
		void SetTime(const std::chrono::high_resolution_clock::time_point& currentTime, const std::chrono::milliseconds& delay = std::chrono::milliseconds(0));

		/*!	\brief Returns the time at which point this event was enqueued.
			@return The time that this event was enqueued.
		*/
		const std::chrono::high_resolution_clock::time_point GetTimeEnqueued() const;

		/*!	\brief Returns the delay for this event's publication.
			@return The delay for this event's publication.
		*/
		const std::chrono::milliseconds GetDelay() const;

		/*!	\brief Returns whether or not this EventPublisher is ready to be published.
			@param currentTime The current timestamp
			@return Returns true if ready to be published.
		*/
		bool IsPrimed(const std::chrono::high_resolution_clock::time_point& currentTime) const;

		/*!	\brief Delivers this event to all subscribed classes.
			@param thisEventPublisher The shared pointer that references this class, so it can be passed along to subscribers.
		*/
		void Deliver(std::shared_ptr<EventPublisher> thisEventPublisher) const;

	private:
		mutable std::mutex mTimeMutex;
		// Time when this event was enqueued
		std::chrono::high_resolution_clock::time_point mEnqueuedTime;
		// Delay before this event is delivered
		std::chrono::milliseconds mDelay;

		// Pointer to the list of subscribers.
		SList<EventSubscriber*>* mSubscribersReference;
		std::mutex* mSubscribersMutexReference;
	};

}