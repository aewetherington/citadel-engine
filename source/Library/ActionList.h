#pragma once

/*!	\file ActionList.h
	\brief Contains the declaration for ActionList, which is the parent of all ActionList and can contain and update other Actions.
*/

#include "Action.h"
#include "Factory.h"

namespace Library
{
	/*!	\brief The parent of all ActionList and can contain and update other Actions.
	*/
	class ActionList : public Action
	{
		RTTI_DECLARATIONS(ActionList, Action);

	public:
		/*!	\brief Sets initial values based on the passed parameters
			@param mName Initial name to give this ActionList.
		*/
		ActionList(const std::string& name = "ActionList");

		/*!	\brief Destroys any owned data.
		*/
		virtual ~ActionList() = default;

		/*!	\brief Initializes prescribed members.
		*/
		void Populate();

		/*!	\brief Returns a pointer to the Scope that holds the Actions for this object.
		*/
		Scope* GetActions();

		/*!	\brief Creates a new Action based on the name and instance strings within this ActionList.
			@param actionClassName The classname of the Action to be created
			@param actionInstanceName The name to give the Action
			@return A pointer to the newly created Action.
			@throws std::runtime_error Thrown when the Action fails to be created.
		*/
		Action* CreateAction(const std::string& actionClassName, const std::string& actionInstanceName);

		/*!	\brief Per-frame update function that calls the function on all of its owned Actions.
			@param state The state of the world, to be considered per-frame.
		*/
		virtual void Update(WorldState& state) override;

	private:
		Scope* mActions;
	};

	ActionFactory(ActionList);

}