#pragma once

/*!	\file ActionCreateAction.h
	\brief Contains the declaration for ActionCreateAction, which can create other actions contained by the same object, based on parameters.
*/

#include "Action.h"
#include "Factory.h"

namespace Library
{
	/*!	\brief Can create other actions on update, within the same container, based on its known parameters.
	*/
	class ActionCreateAction : public Action
	{
		RTTI_DECLARATIONS(ActionCreateAction, Action);

	public:

		/*!	\brief Initializes default values.
			@param name The name to give the action.
		*/
		ActionCreateAction(const std::string& name = "ActionCreateAction");

		/*!	\brief Deallocates owned memory.
		*/
		virtual ~ActionCreateAction() = default;

		/*!	\brief Populates the prescribed attributes for this object.
		*/
		void Populate();

		/*!	\brief Sets the prototype name for the Action to be created on update.
			@param prototypeName The name to use.
		*/
		void SetPrototypeName(const std::string& prototypeName);

		/*!	\brief Returns the prototype name for the Action to be created on update.
			@return The name to use.
		*/
		const std::string& GetPrototypeName() const;

		/*!	\brief Sets the prototype class for the Action to be created on update.
			@param prototypeClass The name of the class to instantiate.
		*/
		void SetPrototypeClass(const std::string& prototypeClass);

		/*!	\brief Returns the prototype class for the Action to be created on update.
			@return The name of the class to instantiate.
		*/
		const std::string& GetPrototypeClass() const;

		/*!	\brief To be called on every frame.
			Attempts to create a new Action based on its contained data.
			@param state The game state, which is to be considered every update.
			@throws std::runtime_error Thrown if this action currently has no owner.
			@throws std::runtime_error Thrown if either the prototype name or class isn't set.
			@throws std::runtime_error Thrown if creation of the object failed.
		*/
		virtual void Update(WorldState& state) override;

		/*!	\brief The string key used to directly access the prototype name attribute.
		*/
		static const std::string sPrototypeNameAttribute;

		/*!	\brief The string key used to directly access the prototype class attribute.
		*/
		static const std::string sPrototypeClassAttribute;
	};

	ActionFactory(ActionCreateAction);

}