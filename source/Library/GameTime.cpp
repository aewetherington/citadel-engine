#include "pch.h"

namespace Library
{
	GameTime::GameTime()
		: mCurrentTime(), mTotalGameTime(), mElapsedGameTime()
	{

	}

	const std::chrono::high_resolution_clock::time_point& GameTime::GetCurrentTime() const
	{
		return mCurrentTime;
	}

	void GameTime::SetCurrentTime(const std::chrono::high_resolution_clock::time_point& currentTime)
	{
		mCurrentTime = currentTime;
	}

	const std::chrono::milliseconds& GameTime::GetTotalGameTime() const
	{
		return mTotalGameTime;
	}

	void GameTime::SetTotalGameTime(const std::chrono::milliseconds& totalGameTime)
	{
		mTotalGameTime = totalGameTime;
	}

	const std::chrono::milliseconds& GameTime::GetElapsedGameTime() const
	{
		return mTotalGameTime;
	}

	void GameTime::SetElapsedGameTime(const std::chrono::milliseconds& elapsedGameTime)
	{
		mElapsedGameTime = elapsedGameTime;
	}


}