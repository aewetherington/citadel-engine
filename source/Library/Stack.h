#pragma once

/*!	\file SList.h
	\brief Contains Stack, a templated stack.
*/

#include "pch.h"

namespace Library
{
	template <typename T> class SList;

	/*!	\class Stack
		\brief Templated implementation of a stack.

		When using this class, ownership of memory does not transfer to the stack.
		User is responsible for deleting previously allocated memory when finished with it.
	*/
	template <typename T>
	class Stack final
	{
	public:
		/*!	\fn Stack()
			\brief Initializes the stack to be empty.
		*/
		Stack();

		/*!	\fn ~Stack()
			\brief Empties the stack and frees assocated memory.

			Clears the stack of all data. User is responsible for any necessary memory management brought on by their choice of T.

			@see Clear()
		*/
		~Stack();

		/*!	\fn Stack(const Stack& rhs)
			\brief Performs a deep copy of the stack.

			@param rhs The stack to be copied.
			@see operator=()
		*/
		Stack(const Stack& rhs);
		
		/*!	\fn Stack& operator=(const Stack& rhs)
			\brief Performs a deep copy of the stack.

			@param rhs The stack to be copied
			@return The copied stack
			@see Stack(const Stack&)
		*/
		Stack& operator=(const Stack& rhs);
		
		/*!	\fn Push(const T& data);
			\brief Appends an item to the top of the stack.

			@param data The item to be added to the stack.
			@see Pop()
			@see Top()
			@see Top() const
		*/
		void Push(const T& data);

		/*!	\fn Pop()
			\brief Removes the top item in the stack, and returns it.

			User is responsible for any necessary memory management brought on by their choice of T.

			@return Top data member in the stack.
			@exception std::runtime_error Thrown if the function is called with no elements in the stack.
			@see Push(const T&)
			@see Top()
			@see Top() const
		*/
		T Pop();

		/*!	\fn Top()
			\brief Returns the top item in the stack.

			@return The top value stored in the stack.
			@exception std::runtime_error Thrown if the function is called with no elements in the stack.
			@see Push(const T&)
			@see Pop()
			@see Top() const
		*/
		T& Top();

		/*!	\fn Top() const
			\brief Returns the top item in the stack.

			@return The top value stored in the stack.
			@exception std::runtime_error Thrown if the function is called with no elements in the stack.
			@see Push(const T&)
			@see Pop()
			@see Top()
		*/
		const T& Top() const;

		/*!	\fn GetSize() const
			\brief Returns the number of items in the stack.

			@return The number of items in the stack.
		*/
		std::uint32_t GetSize() const;

		/*!	\fn bool IsEmpty() const
			\brief Returns true when the stack contains no items, and false otherwise.

			@return True only if the stack has no items.
		*/
		bool IsEmpty() const;

		/*!	\fn Clear()
			\brief Empties the stack.

			Clears the stack of all data. User is responsible for any necessary memory management brought on by their choice of T.
			@see ~Stack()
		*/
		void Clear();

	private:
		SList<T> mList;
	};
}

#include "Stack.inl"