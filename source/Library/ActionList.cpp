#include "pch.h"
#include "Factory.h"
#include "ActionList.h"
#include "Action.h"

namespace Library
{
	RTTI_DEFINITIONS(ActionList);

	ActionList::ActionList(const std::string& name)
		: Action(name), mActions(new Scope())
	{
		// REFACTOR : Will potentially have issues on construction because of the populate calls in ctor.
		Populate();
	}

	void ActionList::Populate()
	{
		Action::Populate();

		INIT_SIGNATURE();

		INTERNAL_ATTRIBUTE("actions", Datum::DatumType::Table, &mActions, 1);

		Attributed::Populate();
	}

	Scope* ActionList::GetActions()
	{
		return mActions;
	}

	Action* ActionList::CreateAction(const std::string& actionClassName, const std::string& actionInstanceName)
	{
		Action* newAction = Factory<Action>::Create(actionClassName);

		if (newAction == nullptr)
		{
			throw std::runtime_error("Failed to instantiate Action of type " + actionClassName);
		}

		newAction->SetName(actionInstanceName);
		newAction->SetOwner(this);
		return newAction;
	}

	void ActionList::Update(WorldState& state)
	{
		// For each set of actions with matching names
		for (std::uint32_t i = 0; i != mActions->GetSize(); ++i)
		{
			Datum& sameNameActions = (*mActions)[i];

			// For each action with a given matching name
			for (std::uint32_t j = 0; j != sameNameActions.GetSize(); ++j)
			{
				// Call their update function.
				state.Action = sameNameActions.GetTable(j)->As<Action>();
				if (state.Action == nullptr)
				{
					throw std::runtime_error("Non-Action found in " + GetGUID() + "'s Action list.");
				}
				if (state.Action->Find("isActive")->Get<int>())
				{
					state.Action->Update(state);
				}
			}
		}

		state.Action = nullptr;
	}

}