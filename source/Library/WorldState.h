#pragma once

#include "GameTime.h"

namespace Library
{
	class World;
	class Sector;
	class Entity;
	class Action;

	class WorldState final
	{

		friend class World;
	public:
		WorldState();
		~WorldState();
		GameTime& GetGameTime();
		void SetGameTime(const GameTime& time);

		// TODO : Not sure if these should actually be public...
		// Objects currently being processed.
		World* World;
		Sector* Sector;
		Entity* Entity;
		Action* Action;

	private:
		GameTime mTime;
	};

}
