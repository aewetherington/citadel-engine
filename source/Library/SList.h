#pragma once

/*! \file SList.h
	\brief Contains SList, a templated singly-linked list.
*/

#include "pch.h"

namespace Library
{
	/*!	\class SList
		\brief Templated implementation of a singly-linked list.

		When using this class, ownership of memory does not transfer to the list. User is responsible for deleting previously allocated memory when finished with it.
	*/
	template <typename T>
	class SList final
	{

	private:
		// A single element in the list.
		class Node
		{
		public:
			Node(const T& data, Node* next = nullptr);	// Creates a new node with the given data.
			T mData;		// Data stored in the node
			Node *mNext;	// Reference to the next node in the list.

			Node(const Node& rhs) = delete; // Copy constructor implementation disallowed by this. Does not matter whether this is public or private.
			Node& operator=(const Node& rhs) = delete; // Assignment operator implementation disallowed by this. Does not matter whether this is public or private.
		};

	public:
		/*!	\class Iterator
			\brief Used to simplify manipulating the SList.
		*/
		class Iterator
		{
			friend class SList;

		public: 

			/*!	\fn Iterator()
				\brief Initializes the iterator to default values, not associated with any list.
			*/
			Iterator();

			/*! \fn Iterator(const Iterator& rhs)
				\brief Copy constructor that sets the iterator's values to be the same as the paramater's. Performs a shallow copy.
				@param rhs The iterator to be copied.
				@see operator=()
			*/
			Iterator(const Iterator& rhs);

			/*! \fn Iterator& operator=(const Iterator& rhs)
				\brief Sets the iterator's values to be the same as the paramter's. Performs a shallow copy.
				@param rhs The iterator to be copied.
				@return Reference to the new iterator.
				@see Iterator(const Iterator& rhs)
			*/
			Iterator& operator=(const Iterator& rhs);

			/*!	\fn bool operator==(const Iterator& rhs) const
				\brief Compares the two iterators to see if they are pointing to the same data.
				@param rhs The iterator to be compared to this one.
				@return True only if the two iterators are equivalent.
				@see operator!=()
			*/
			bool operator==(const Iterator& rhs) const;

			/*! \fn bool operator!=(const Iterator& rhs) const
				\brief Compares the two iterators to see if they are pointing to different data.
				@param rhs The iterator to be compared to this one.
				@return True only if the two iterators are inequivalent.
				@see operator==()
			*/
			bool operator!=(const Iterator& rhs) const;

			/*! \fn Iterator& operator++()
				\brief Pre-increment operator that moves the iterator to point to the next element in the list, and returns the new iterator.
				@return The iterator after it has been incremented.
				@see operator++(int)
			*/
			Iterator& operator++();

			/*! \fn Iterator operator++(int)
				\brief Post-increment operator that moves the iterator to point to the next element in the list, and returns a copy of the old iterator.
				@return A copy of the iterator before it has been incremented.
				@see operator++()
			*/
			Iterator operator++(int);

			/*!	\fn T& operator*() const
				\brief Retrieves the data at the location pointed to by this iterator, and returns it.
				@return The data associated with this iterator.
				@exception std::runtime_error Thrown if called on an iterator not pointing to any data.
			*/
			T& operator*() const;

		private:
			Iterator(const SList *owner, Node *node);
			const SList* mOwner;
			Node* mNode;
		};

		/*! \fn SList()
			\brief Initializes the list to be empty.

		*/
		SList();

		/*! \fn ~SList()
			\brief Empties the list and frees associated memory.

			Clears the list of all data. User is responsible for any necessary memory management brought on by their choice of T.

			@see Clear()
		*/
		~SList();

		/*! \fn SList(const SList& rhs)
			\brief Performs a deep copy of the list.

			@param rhs The list to be copied.
			@see operator=()
		*/
		SList(const SList& rhs);

		/*! \fn SList(SList&& rhs)
			\brief Performs a move of the list data.
				
			@param rhs The list to be moved.
			@see operator=()
		*/
		SList(SList&& rhs);

		/*! \fn SList& operator=(const SList& rhs)
			\brief Performs a deep copy of the list.

			@param rhs The list to be copied.
			@return The copied list.
			@see SList(const SList&)
		*/
		SList& operator=(const SList& rhs);

		/*! \fn SList& operator=(SList&& rhs)
			\brief Performs a move of the list data.

			@param rhs The list to be moved.
			@return The moved list.
			@see SList(SList&&)
		*/
		SList& operator=(SList&& rhs);

		/*! \fn void PushFront(const T& data)
			\brief Appends an item to the front of the list.

			@param data The item to be added to the list.
			@see PushBack()
			@see PopFront()
			@see Front()
		*/
		void PushFront(const T& data);

		/*! \fn void PushBack(const T& data)
			\brief Appends an item to the back of the list.

			@param data The item to be added to the list.
			@see PushFront()
			@see Back()
		*/
		void PushBack(const T& data);

		/*!	\fn T PopFront()
			\brief Removes the first item in the list, and returns it.

			User is responsible for any necessary memory management brought on by their choice of T.

			@return First data member in the list.
			@exception std::runtime_error Thrown if the function is called with no elements in the list.
			@see PushFront()
			@see Front()
		*/
		T PopFront();

		/*! \fn T& Front() const
			\brief Returns the first item in the list.

			@return The first value stored in the list.
			@exception std::runtime_error Thrown if the function is called with no elements in the list.
			@see PushFront()
			@see PopFront()
			@see Back()
		*/
		T& Front() const;

		/*!	\fn T& Back() const
			\brief Returns the last item in the list.

			@return The last value stored in the list.
			@exception std::runtime_error Thrown if the function is called with no elements in the list.
			@see PushBack()
			@see Front()
		*/
		T& Back() const;

		/*!	\fn void Clear()
			\brief Empties the list.

			Clears the list of all data. User is responsible for any necessary memory management brought on by their choice of T.

			@see ~SList()
		*/
		void Clear();

		/*! \fn std::uint32_t GetSize() const
			\brief Returns the current number of nodes in the list.

			@return Number of nodes in the list.
		*/
		std::uint32_t GetSize() const;

		/*! \fn bool IsEmpty() const
			\brief Returns true when the list contains no nodes, and false if it contains at least one.

			@return True only if the list has no nodes.
		*/
		bool IsEmpty() const;

		/*! \fn Iterator begin() const
			\brief Returns an iterator pointing at the beginning of the list.
			@return Iterator pointing at the beginning of the list.
			@see end()
		*/
		Iterator begin() const;

		/*!	\fn Iterator end() const
			\brief Returns an iterator pointing past the end of the list.
			@return Iterator pointing past the beginning of the list.
			@see begin()
		*/
		Iterator end() const;

		/*!	\fn void InsertAfter(const Iterator& itLoc, const T& data)
			\brief Inserts the given data at the location after where the passed iterator is pointing.
			@param itLoc An iterator at the location the data is to be inserted after.
			@param data The data to be inserted in the list.
			@exception std::runtime_error Thrown if itStart is not owned by this SListt
		*/
		void InsertAfter(const Iterator& itLoc, const T& data);

		/*!	\fn Iterator FindNext(const Iterator& itStart, const T& value) const
			\brief Finds the next occurrence of value, beginning inclusively from itStart. 
			@param itStart An iterator pointing at the location to start searching.
			@param value The data to try and find a match for.
			@return An iterator at the location of the data found. If no matches are made, the iterator points past the end of the list.
			@exception std::runtime_error Thrown if itStart is not owned by this SList.
			@see Find()
			@see end()
		*/
		Iterator FindNext(const Iterator& itStart, const T& value) const;

		/*!	\fn Iterator Find(const T& value) const
			\brief Finds the first occurrence of value.
			@param value The data to try and find a match for.
			@return An iterator at the location of the data found. If no matches are made, the iterator points past the end of the list.
			@see FindNext()
			@see end()
		*/
		Iterator Find(const T& value) const;

		/*!	\fn void Remove(const T& value)
			\brief Removes the first instance of the value specified, if it exists in the list.

			User is resonsible for any memory management brought on by their choice of T.
			@param value The value to be removed from the list.
			@see RemoveAll()
		*/
		void Remove(const T& value);

		/*!	\fn void RemoveAll(const T& value)
			\brief Removes all instances of the value specified, if they exist in the list.

			User is resonsible for any memory management brought on by their choice of T.
			@param value The value to be removed from the list.
			@see Remove()
		*/
		void RemoveAll(const T& value);

	private:
		Node *mFront;			// Pointer to the first node in the list.
		Node *mBack;			// Pointer to the last node in the list.
		std::uint32_t mSize;	// Number of elements in the list.

	};

}

#include "SList.inl"