#pragma once

/*! \file Datum.h
	\brief Contains the declaration for a runtime-determined generic data type
*/

#include "pch.h"
#include "glm/glm.hpp"

namespace Library
{
	class RTTI;
	class Scope;
	class Attributed;

	/*!	\brief Runtime-determined generic data type.

		The data passed in to this function may or may not be claimed as internal by the class.
		If it is claimed as internal, Datum will take responsibility for cleaning it up when destructed.
		Data is typically considered internally owned. Data is determined as externally owned when
		it is set using SetStorage() or by assignment/copy from another Datum that has external storage.
	*/
	class Datum
	{
		friend class Attributed;

	public:
		/*!	\brief Enum of types able to be set at runtime.
		*/
		enum class DatumType
		{
			Unknown = 0,
			Integer,
			Float,
			Vector,
			Matrix,
			String,
			Table,
			Pointer,
			MAX
		};

		/*!	\brief Initializes the Datum to default values.
		*/
		Datum();

		/*!	\brief Copies the Datum argument, based on whether the argument has internal or external storage.

			If the argument has internal storage, does a deep copy. Otherwise, does a shallow copy.
			@param rhs The Datum to be copied.
			@see operator=(const Datum&)
		*/
		Datum(const Datum& rhs);

		/*!	\brief Moves the ownership of rhs Datum's data to this Datum.
			@param rhs The Datum to be moved.
			@see operator=(const Datum&&)
		*/
		Datum(Datum&& rhs);

		/*!	\brief Initializes a Datum based on the parameters.
			@param type The DatumType to give this Datum
			@param capacity The size of elements to be able to hold.
		*/
		explicit Datum(DatumType type, std::uint32_t size = 1);

		/*!	\brief Cleans up the Datum, based on whether the contained data is internally stored, or externally.

			If internal storage is set, the Datum will delete the memory. If external, the memory will remain intact after destruction.
		*/
		~Datum();

		/*!	\brief Copies the Datum argument, based on whether the argument has internal or external storage.

			If the argument has internal storage, does a deep copy. Otherwise, does a shallow copy.
			@param rhs The Datum to be copied.
			@see Datum(const Datum&)
		*/
		Datum& operator=(const Datum& rhs);

		/*!	\brief Moves the ownership of rhs Datum's data to this Datum.
			@param rhs The Datum to be moved.
			@see Datum(const Datum&&)
		*/
		Datum& operator=(Datum&& rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(const std::int32_t& rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(const float& rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(const glm::vec4& rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(const glm::mat4& rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(const std::string& rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(Scope* rhs);

		/*!	\brief Treats this Datum as a scalar, and copies the right-hand side to index 0.
			@param rhs The value to be copied.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		Datum& operator=(RTTI* rhs);

		/*!	\brief Returns the current type of the Datum.
			@return The type of the Datum.
		*/
		DatumType GetType() const;

		/*!	\brief Sets the current type of the Datum, if allowed.

			Type may not be set unless the current type is unknown.
			@param type The type to set this Datum to.
			@exception std::runtime_error Thrown if the type is unable to be changed at the time of the call.
		*/
		void SetType(DatumType type);

		/*!	\brief Return the current number of elements in the Datum object.
			@return The number of elements in the Datum.

		*/
		std::uint32_t GetSize() const;

		/*!	\brief Sets the size of the Datum, if allowed.

			Size may not be set if the storage type is external.
			@param size The size to set this Datum to.
			@exception std::runtime_error Thrown if actually attempting to change the size, and the storage type is external.
			@exception std::runtime_error Thrown if the type of the Datum is unknown.
		*/
		void SetSize(std::uint32_t size);

		/*!	\brief Returns whether or not the data storage mode is external.
			@return True if Datum has external storage. False if it's internal.
		*/
		bool IsExternal() const;

		/*!	\brief Clears out the Datum object, ready to be reused.

			If the data storage is set to external, does not free the referenced memory.
			If the data storage is set to internal, frees the referenced memory.
			Sets the Datum type to unknown.
		*/
		void Clear();

		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(std::int32_t* dataArray, std::uint32_t size);

		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(float* dataArray, std::uint32_t size);

		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(glm::vec4* dataArray, std::uint32_t size);

		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(glm::mat4* dataArray, std::uint32_t size);

		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(std::string* dataArray, std::uint32_t size);

		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(Scope** dataArray, std::uint32_t size);
		
		/*!	\brief Sets the storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(RTTI** dataArray, std::uint32_t size);

	private:
		/*!	\brief Sets tthe storage provided to Datum, as external storage.
			@param dataArray The data array to be copied as external storage.
			@param dataType The type of the data in dataArray.
			@param size The size of the provided array.
			@exception std::runtime_error Thrown if the provided type is invalid.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void SetStorage(void* dataArray, DatumType dataType, std::uint32_t size);

	public:
		/*!	\brief Compares entire Datum contents to the parameter provided.
			@param rhs The Datum to be compared with.
			@return True only if the comparison of all elements show they are equal. 
		*/
		bool operator==(const Datum& rhs) const;

		/*!	\brief Compares entire Datum contents to the parameter provided.
			@param rhs The Datum to be compared with.
			@return True only if the comparison of all elements show they are not equal.
		*/
		bool operator!=(const Datum& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const std::int32_t& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const std::int32_t& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const float& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const float& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const glm::vec4& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const glm::vec4& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const glm::mat4& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const glm::mat4& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const std::string& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const std::string& rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const Scope* rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const Scope* rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator==(const RTTI* rhs) const;

		/*!	\brief Compares the Datum element at index 0 with the parameter.
			@param rhs The value to be compared with.
			@return True only if the 0-th element and the paramter are not equal.
			@exception std::runtime_error Thrown if there are no elements in the Datum.
		*/
		bool operator!=(const RTTI* rhs) const;

		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(const std::int32_t& data, std::uint32_t index = 0);

		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(const float& data, std::uint32_t index = 0);

		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(const glm::vec4& data, std::uint32_t index = 0);

		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(const glm::mat4& data, std::uint32_t index = 0);

		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(const std::string& data, std::uint32_t index = 0);

		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(Scope* data, std::uint32_t index = 0);
		
		/*!	\brief Sets the provided data object at the index specified.
			@param data The value to be copied.
			@param index The index location at which to copy the value.
			@exception std::runtime_error Thrown if the index is out of range and the storage is external.
			@exception std::runtime_error Thrown when the assigned value does not match the Datum type, if it has been set already.
		*/
		void Set(RTTI* data, std::uint32_t index = 0);

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown when this generic function is called. That is, there is no support for the specified type.
		*/
		template <typename T> T& Get(std::uint32_t index = 0) const;

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown when the index is out of range.
		*/
		template <> std::int32_t& Get<std::int32_t>(std::uint32_t index) const;

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown when the index is out of range.
		*/
		template <> float& Get<float>(std::uint32_t index) const;

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown when the index is out of range.
		*/
		template <> glm::vec4& Get<glm::vec4>(std::uint32_t index) const;

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown when the index is out of range.
		*/
		template <> glm::mat4& Get<glm::mat4>(std::uint32_t index) const;

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown when the index is out of range.
		*/
		template <> std::string& Get<std::string>(std::uint32_t index) const;

		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown if index goes out of range.
		*/
		Scope* GetTable(std::uint32_t index = 0) const;
		
		/*!	\brief Gets the value at the provided index
			@param index The index at which to retrieve data.
			@return The data at the specified index.
			@exception std::runtime_error Thrown if index goes out of range.
		*/
		RTTI* GetPointer(std::uint32_t index = 0) const;

		/*!	\brief Attempts to parse the string for a value to place at the provided index.

			Requires that the type of the Datum has already been set.
			@param data The string to be parsed for a value.
			@param index The indexed location at which to store the parsed value.
			@exception std::runtime_error Thrown if the type of Datum has not been set already.
			@exception std::runtime_error Thrown if a value was unable to be parsed.
			@exception std::runtime_error Thrown if provided index resulte.
		*/
		void SetFromString(const std::string& data, std::uint32_t index = 0);

		/*!	\brief Turns the data at the provided index into string format.
			@param index The indexed location at which value to convert lies.
			@return The converted string.
			@exception std::runtime_error Thrown if the type of Datum has not been set already.
			@exception std::runtime_error Thrown if the index is out of range.
		*/
		std::string ToString(std::uint32_t index = 0) const;

		// Notes:
		/*	Store values in array, initially unallocated.
			Keep num elements in array (capacity) and num of values (population) in the array.
			This means: dtor deletes array, deep copy constructor, deep copy assignment
			CONSIDER C++11 move semantics. (If I get time, this would be a good idea.)
		*/

	private:
		union DatumValue
		{
			std::int32_t* i;
			float* f;
			glm::vec4* v;
			glm::mat4* m;
			std::string* s;
			Scope** t;
			RTTI** r;

			void* vp;

			DatumValue() : vp(nullptr) {}
		};

		typedef void(*AllocFunction)(Datum&, std::uint32_t);
		typedef void(*DeallocFunction)(Datum&);
		typedef void(*CopyFunction)(Datum&, DatumValue, std::uint32_t);
		typedef bool(*CompareFunction)(const Datum&, DatumValue, std::uint32_t);
		typedef std::int32_t(*FromStringFunction)(Datum&, const std::string&, std::uint32_t);
		typedef std::string(*ToStringFunction)(const Datum&, std::uint32_t);

		// Function pointers for allocating.
		static void AllocUnknown(Datum& d, std::uint32_t size);
		static void AllocInteger(Datum& d, std::uint32_t size);
		static void AllocFloat(Datum& d, std::uint32_t size);
		static void AllocVector(Datum& d, std::uint32_t size);
		static void AllocMatrix(Datum& d, std::uint32_t size);
		static void AllocString(Datum& d, std::uint32_t size);
		static void AllocTable(Datum& d, std::uint32_t size);
		static void AllocPointer(Datum& d, std::uint32_t size);
		static const AllocFunction sAllocFuncts[static_cast<int>(DatumType::MAX)];

		// Function pointers for deallocating.
		static void DeallocUnknown(Datum& d);
		static void DeallocInteger(Datum& d);
		static void DeallocFloat(Datum& d);
		static void DeallocVector(Datum& d);
		static void DeallocMatrix(Datum& d);
		static void DeallocString(Datum& d);
		static void DeallocTable(Datum& d);
		static void DeallocPointer(Datum& d);
		static const DeallocFunction sDeallocFuncts[static_cast<int>(DatumType::MAX)];

		// Function pointers for copying.
		static void CopyUnknown(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyInteger(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyFloat(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyVector(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyMatrix(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyString(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyTable(Datum& d, DatumValue data, std::uint32_t size);
		static void CopyPointer(Datum& d, DatumValue data, std::uint32_t size);
		static const CopyFunction sCopyFuncts[static_cast<int>(DatumType::MAX)];

		// Function pointers for comparing a list of data to that compared in the passed Datum.
		static bool CompareUnknown(const Datum& d, DatumValue data, std::uint32_t size);
		static bool CompareInteger(const Datum& d, DatumValue data, std::uint32_t size);
		static bool CompareFloat(const Datum& d, DatumValue data, std::uint32_t size);
		static bool CompareVector(const Datum& d, DatumValue data, std::uint32_t size);
		static bool CompareMatrix(const Datum& d, DatumValue data, std::uint32_t size);
		static bool CompareString(const Datum& d, DatumValue data, std::uint32_t size);
		static bool CompareTable(const Datum& d, DatumValue data, std::uint32_t size);
		static bool ComparePointer(const Datum& d, DatumValue data, std::uint32_t size);
		static const CompareFunction sCompareFuncts[static_cast<int>(DatumType::MAX)];

		// Function pointers used by SetFromString.
		static std::int32_t FromStringUnknown(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringInteger(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringFloat(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringVector(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringMatrix(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringString(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringTable(Datum& d, const std::string& str, std::uint32_t index);
		static std::int32_t FromStringPointer(Datum& d, const std::string& str, std::uint32_t index);
		static const FromStringFunction sFromStringFuncts[static_cast<int>(DatumType::MAX)];

		// Function pointers used by ToString (?).
		static std::string ToStringUnknown(const Datum& d, std::uint32_t index);
		static std::string ToStringInteger(const Datum& d, std::uint32_t index);
		static std::string ToStringFloat(const Datum& d, std::uint32_t index);
		static std::string ToStringVector(const Datum& d, std::uint32_t index);
		static std::string ToStringMatrix(const Datum& d, std::uint32_t index);
		static std::string ToStringString(const Datum& d, std::uint32_t index);
		static std::string ToStringTable(const Datum& d, std::uint32_t index);
		static std::string ToStringPointer(const Datum& d, std::uint32_t index);
		static const ToStringFunction sToStringFuncts[static_cast<int>(DatumType::MAX)];

		DatumType mType;
		DatumValue mData;
		std::uint32_t mSize;
		std::uint32_t mCapacity;
		bool mIsExternal;
	};

}