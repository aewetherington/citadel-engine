#pragma once

/*!	\file ReactionAttributed.h
	\brief Contains the declaration for ReactionAttributed, which can react to all types of Event with an EventMessageAttributed message, with some constraints.
*/

#include "pch.h"
#include "Reaction.h"

namespace Library
{
	/*!	\brief A reaction that can react to all types of Event with an EventMessageAttributed message, with some constraints. 
		The constraints are a list of subtypes that it is capable of reacting to.
		EventMessageAttributed objects have a subtype, which is used to check if this is one that can be reacted to.
	*/
	class ReactionAttributed : public Reaction
	{
		RTTI_DECLARATIONS(ReactionAttributed, Reaction);

	public:
		/*!	\brief Sets initial values based on the passed parameters
			@param mName Initial name to give this ReactionAttributed.
		*/
		ReactionAttributed(const std::string& name = "ReactionAttributed");

		/*!	\brief Frees any allocated data.
		*/
		virtual ~ReactionAttributed();

		/*!	\brief Populates the prescribed attributes of ReactionAttributed.
		*/
		void Populate();

		/*!	\brief Callback function for when an event get primed and delivered to the class, if it has previously subscribed to that type of event.
			If the event is of a known subtype, then this call will result in an update call to this ReactionAttributed's children.
			@param eventPublisher The event that was fired off and now delivered.
		*/
		virtual void Notify(std::shared_ptr<EventPublisher> eventPublisher) override;

		/*!	\brief A string used to locate the subtype reactions parameter.
		*/
		const static std::string sSubtypeReactionsTag;
	};

	ActionFactory(ReactionAttributed);

}