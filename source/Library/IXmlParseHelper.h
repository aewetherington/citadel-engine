#pragma once

/*! \file IXmlParseHelper.h
	\brief Contains an interface for XmlParseHelper objects. 
*/

#include "pch.h"
#include "HashMap.h"
#include "XmlParseMaster.h"

namespace Library
{
	/*!	\brief The interface for XmlParseHelper objects.
	*/
	class IXmlParseHelper
	{
	public:
		/*! \brief Default constructor. Initializes data members.
		*/
		IXmlParseHelper() = default;

		/*!	\brief Virtual destructor. Cleans up any contained data.
		*/
		virtual ~IXmlParseHelper() = default;

		/*!	\brief Initialization of the parse helper.

			Gets called right for every parse helper right before an individual file is parsed.
		*/
		virtual void Initialize() { };

		/*!	\brief Handler for XML object start tags, called by the XmlParseMaster that is using this helper.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper. If no SharedData object is associated, the value is nullptr.
			@param name The name of the tag.
			@param atttributes A HashMap containing name/value pairs of attributes listed in the XML start tag.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool StartElementHandler(XmlParseMaster::SharedData* sharedData, const std::string& name, const HashMap<std::string, std::string>& attributes) = 0;

		/*!	\brief Handler for XML object end tags, called by the XmlParseMaster that is using this helper. If no SharedData object is associated, the value is nullptr.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper.
			@param name The name of the tag.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool EndElementHandler(XmlParseMaster::SharedData* sharedData, std::string name) = 0;

		/*!	\brief Handler for XML object character data, called by the XmlParseMaster that is using this helper. If no SharedData object is associated, the value is nullptr.
			@param sharedData The SharedData object associated with the XmlParseMaster calling this helper.
			@param data A null-terminated C-string of data to be handled.
			@param length The length of data.
			@return True if this handler handled the request, and false otherwise.
		*/
		virtual bool CharElementHandler(XmlParseMaster::SharedData* sharedData, const char* data, int length) { return false; };

		/*!	\brief Generates a deep copy of this parse helper on the heap, prepares it for usage, and returns a pointer to it.

			User is responsible for calling delete on the returned value once finished with it, unless it is owned by a cloned XmlParseMaster.
			@return Address of a new parse helper.
		*/
		virtual IXmlParseHelper* Clone() const = 0;

	private:
		IXmlParseHelper(const IXmlParseHelper& rhs) = delete;
		IXmlParseHelper& operator=(const IXmlParseHelper& rhs) = delete;
	};
}