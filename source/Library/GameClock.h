#pragma once
/*!	\file GameClock.h
	\brief Contains declarations for GameClock class.
*/

#include <chrono>

namespace Library
{
	/*!	\brief Class used to maintain gametime data on each frame
	*/
	class GameClock final
	{
	public:
		/*!	\brief Sets default values for class.
		*/
		GameClock();

		const std::chrono::high_resolution_clock::time_point& GetStartTime() const;
		const std::chrono::high_resolution_clock::time_point& GetCurrentTime() const;
		const std::chrono::high_resolution_clock::time_point& GetLastTime() const;

		void Reset();

		void UpdateGameTime(GameTime& gameTime);

	private:
		std::chrono::high_resolution_clock::time_point mStartTime;
		std::chrono::high_resolution_clock::time_point mCurrentTime;
		std::chrono::high_resolution_clock::time_point mLastTime;

	};

}