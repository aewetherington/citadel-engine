#include "pch.h"
#include "SharedDataTable.h"
#include "Scope.h"

namespace Library
{

#pragma region SharedDataTable

	RTTI_DEFINITIONS(SharedDataTable);

	SharedDataTable::SharedDataTable(Scope& root)
		: mRoot(&root), mRootName(""), mStack()
	{

	}

	SharedDataTable::~SharedDataTable()
	{
		// If clone, delete owned data.
		if (mIsClone)
		{
			delete mRoot;
		}
	}

	SharedDataTable::SharedData* SharedDataTable::Clone() const
	{
		Scope* newRoot = new Scope();

		SharedDataTable* newSharedData = new SharedDataTable(*newRoot);
		newSharedData->mIsClone = true;

		return reinterpret_cast<SharedData*>(newSharedData);
	}

	Scope* SharedDataTable::GetRoot() const
	{
		return mRoot;
	}

	std::string SharedDataTable::GetRootName() const
	{
		return mRootName;
	}

#pragma endregion

#pragma region Tags&Attributes&Keys

	const std::string SharedDataTable::mStringDataKey = "XMLStringDataKey";
	const std::string SharedDataTable::mDataTypeKey = "XMLDataTypeKey";
	const std::string SharedDataTable::mParentTypeKey = "XMLParentTypeKey";
	const std::string SharedDataTable::mNumArrayElementsKey = "XMLNumArrayElementsKey";
	const std::string SharedDataTable::mArrayElementsKey = "XMLArrayElementsKey";
	const std::string SharedDataTable::mIsScalarKey = "XMLIsScalarKey";

	const std::string SharedDataTable::mReservedKeys[] = {
		mStringDataKey,
		mDataTypeKey,
		mParentTypeKey,
		mNumArrayElementsKey,
		mArrayElementsKey,
		mIsScalarKey
	};

	const std::string SharedDataTable::mNameAttribute = "name";
	const std::string SharedDataTable::mSizeAttribute = "size";
	const std::string SharedDataTable::mClassAttribute = "class";

	const std::string SharedDataTable::mValueTag = "value";
	const std::string SharedDataTable::mIntegerTag = "int";
	const std::string SharedDataTable::mFloatTag = "float";
	const std::string SharedDataTable::mVectorTag = "vec";
	const std::string SharedDataTable::mMatrixTag = "mat";
	const std::string SharedDataTable::mStringTag = "str";
	const std::string SharedDataTable::mTableTag = "table";
	const std::string SharedDataTable::mEntityTag = "entity";
	const std::string SharedDataTable::mSectorTag = "sector";
	const std::string SharedDataTable::mWorldTag = "world";
	const std::string SharedDataTable::mActionTag = "action";

#pragma endregion

}
