namespace Library
{

#pragma region HashFunctor

	template <typename T>
	std::uint32_t DefaultHash<T>::operator()(const T& key) const
	{
		const std::uint32_t HashMult = 65U;
		std::uint32_t hash = 0;

		const byte* data = reinterpret_cast<const byte*>(&key);
		std::uint32_t size = sizeof(T);
		for (std::uint32_t i = 0; i < size; ++i)
		{
			hash = HashMult * hash + data[i];
		}

		return hash;
	}

	/*!	\brief Templated implementation of a default hashfunctor for pointers.
	*/
	template <typename T>
	class DefaultHash<T*>
	{
	public:
		std::uint32_t operator()(const T* key) const
		{
			const std::uint32_t HashMult = 65U;
			std::uint32_t hash = 0;

			const byte* data = reinterpret_cast<const byte*>(key);
			std::uint32_t size = sizeof(T);
			for (std::uint32_t i = 0; i < size; ++i)
			{
				hash = HashMult * hash + data[i];
			}

			return hash;
		}
	};

	/*!	\brief Template specialization for a default hashfunctor.
	*/
	template<>
	class DefaultHash<std::string>
	{
	public:
		std::uint32_t operator()(const std::string& key) const
		{
			const std::uint32_t HashMult = 65U;
			std::uint32_t hash = 0;

			std::uint32_t size = key.length();
			for (std::uint32_t i = 0; i < size; ++i)
			{
				hash = HashMult * hash + key[i];
			}

			return hash;
		}
	};

#pragma endregion

#pragma region Iterator

	template <typename TKey, typename TData, typename HashFunctor>
	HashMap<TKey, TData, HashFunctor>::Iterator::Iterator()
		: mOwner(nullptr), mIndex(0), mChainIt()
	{

	}

	template <typename TKey, typename TData, typename HashFunctor>
	HashMap<TKey, TData, HashFunctor>::Iterator::Iterator(const Iterator& rhs)
		: mOwner(rhs.mOwner), mIndex(rhs.mIndex), mChainIt(rhs.mChainIt)
	{

	}

	template <typename TKey, typename TData, typename HashFunctor>
	HashMap<TKey, TData, HashFunctor>::Iterator::Iterator(const HashMap *owner, std::uint32_t index, const typename SList<PairType>::Iterator& chainIt)
		: mOwner(owner), mIndex(index), mChainIt(chainIt)
	{
		
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator& HashMap<TKey, TData, HashFunctor>::Iterator::operator=(const Iterator& rhs)
	{
		if (this != &rhs)
		{
			mOwner = rhs.mOwner;
			mIndex = rhs.mIndex;
			mChainIt = rhs.mChainIt;
		}

		return *this;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	bool HashMap<TKey, TData, HashFunctor>::Iterator::operator==(const Iterator& rhs) const
	{
		return (mOwner == rhs.mOwner) && (mIndex == rhs.mIndex) && (mChainIt == rhs.mChainIt);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	bool HashMap<TKey, TData, HashFunctor>::Iterator::operator!=(const Iterator& rhs) const
	{
		return !operator==(rhs);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator& HashMap<TKey, TData, HashFunctor>::Iterator::operator++()
	{
		// Test if not at the end of the hashmap
		if (mOwner != nullptr && *this != mOwner->end())
		{
			// Test if not at the end of this chain
			if (mChainIt != mOwner->mBuckets[mIndex].end())
			{
				// Increment the iterator down the list.
				++mChainIt;
			}

			// If we're at the end of the list, gotta change buckets.
			if (mChainIt == mOwner->mBuckets[mIndex].end())
			{
				// Move towards the beginning of a chain that contains things.
				while (mChainIt == mOwner->mBuckets[mIndex].end() && *this != mOwner->end())
				{
					++mIndex;
					mChainIt = mOwner->mBuckets[mIndex].begin();
				}
			}
		}

		return *this;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::Iterator::operator++(int)
	{
		Iterator it = *this;

		operator++();

		return it;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::PairType& HashMap<TKey, TData, HashFunctor>::Iterator::operator*() const
	{
		if (mOwner == nullptr || *this == mOwner->end())
		{
			throw std::runtime_error("Iterator dereferenced while not pointing to any data.");
		}
		else
		{
			return *mChainIt;
		}
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::PairType* HashMap<TKey, TData, HashFunctor>::Iterator::operator->() const
	{
		return &operator*();
	}

#pragma endregion

#pragma region HashMap

	template <typename TKey, typename TData, typename HashFunctor>
	HashMap<TKey, TData, HashFunctor>::HashMap(std::uint32_t capacity)
		: mBuckets(capacity), mSize(0), mBeginIndex(capacity-1)
	{
		if (capacity < 1)
		{
			capacity = 1;
			mBeginIndex = capacity - 1;
		}

		mBuckets.Reserve(capacity);
		
		// Tell the vector to use entire capacity.
		mBuckets.At(capacity-1);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	HashMap<TKey, TData, HashFunctor>::HashMap(const HashMap& rhs)
		: mBuckets(rhs.mBuckets), mSize(rhs.mSize), mBeginIndex(rhs.mBeginIndex)
	{

	}

	template <typename TKey, typename TData, typename HashFunctor>
	HashMap<TKey, TData, HashFunctor>& HashMap<TKey, TData, HashFunctor>::operator=(const HashMap& rhs)
	{
		// Self-test
		if (this == &rhs)
		{
			return *this;
		}

		// Copy things. Vector assignment and SList dtor handle data clearing for us.
		mBuckets = rhs.mBuckets;
		mSize = rhs.mSize;
		mBeginIndex = rhs.mBeginIndex;

		return *this;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	bool HashMap<TKey, TData, HashFunctor>::operator==(const HashMap& rhs) const
	{
		// Do the number of elements differ?
		if (mSize != rhs.mSize)
		{
			return false;
		}

		// Check that all elements in lhs exist in rhs.
		for (Iterator it = begin(); it != end(); ++it)
		{
			Iterator it2 = rhs.Find(it->first);
			if (it2 == rhs.end() || it->second != it2->second)
			{
				return false;
			}
		}

		return true;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	bool HashMap<TKey, TData, HashFunctor>::operator!=(const HashMap& rhs) const
	{
		return !operator==(rhs);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::Find(const TKey& key) const
	{
		std::uint32_t index = mHashFunct(key) % mBuckets.GetSize();

		const ChainType& chain = mBuckets[index];
		auto it = chain.begin();
		while (it != chain.end())
		{
			// Did we find it?
			if ((*it).first == key)
			{
				break;
			}
			++it;
		}

		// If we failed to find it...
		if (it == chain.end())
		{
			return end();
		}
		else
		{
			// We found the object we were looking for.
			return Iterator(this, index, it);
		}
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::Insert(const PairType& data, bool& dataInserted)
	{
		// REFACTOR : Should reuse Find().

		std::uint32_t index = mHashFunct(data.first) % mBuckets.GetSize();

		ChainType::Iterator it = mBuckets[index].begin();
		while (it != mBuckets[index].end())
		{
			// Did we find an object with a matching key?
			if ((*it).first == data.first)
			{
				dataInserted = false;
				break;
			}
			++it;
		}

		// Did we fail to find one already existing?
		if (it == mBuckets[index].end())
		{
			dataInserted = true;

			// Let's push the current value.
			mBuckets[index].PushFront(data);

			++mSize;

			// Should we update the beginning index?
			if (index < mBeginIndex)
			{
				mBeginIndex = index;
			}

			// Update the iterator to the new value.
			it = mBuckets[index].begin();
		}

		// Return the iterator to the object with a matching key.
		return Iterator(this, index, it);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::Insert(const PairType& data)
	{
		bool dataInserted;
		return Insert(data, dataInserted);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::Insert(const TKey& key, const TData& value, bool& dataInserted)
	{
		return Insert(PairType(key, value), dataInserted);
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::Insert(const TKey& key, const TData& value)
	{
		return Insert(PairType(key, value));
	}

	template <typename TKey, typename TData, typename HashFunctor>
	TData& HashMap<TKey, TData, HashFunctor>::operator[](const TKey& key)
	{
		// Can reuse Insert to get the desired behavior.
		return Insert(PairType(key, TData()))->second;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	void HashMap<TKey, TData, HashFunctor>::Remove(const TKey& key)
	{
		// REFACTOR : Should reuse Find().

		std::uint32_t index = mHashFunct(key) % mBuckets.GetSize();

		ChainType::Iterator it = mBuckets[index].begin();
		while (it != mBuckets[index].end())
		{
			// Did we find an object with a matching key?
			if ((*it).first == key)
			{
				// Tell the chain to remove it.
				mBuckets[index].Remove(*it);
				--mSize;

				// Do we need to update the begin index?
				if (index == mBeginIndex && mBuckets[index].GetSize() == 0)
				{
					// Move it up to the next location that has content, if one exists.
					while (mBuckets[mBeginIndex].GetSize() == 0 && mBeginIndex < mBuckets.GetSize()-1)
					{
						++mBeginIndex;
					}
				}

				break;
			}
			++it;
		}

		return;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	void HashMap<TKey, TData, HashFunctor>::Clear()
	{
		for (std::uint32_t i = 0; i < mBuckets.GetSize(); ++i)
		{
			mBuckets[i].Clear();
		}
		mSize = 0;
		mBeginIndex = mBuckets.GetSize() - 1;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	std::uint32_t HashMap<TKey, TData, HashFunctor>::GetSize() const
	{
		return mSize;
	}

	template <typename TKey, typename TData, typename HashFunctor>
	bool HashMap<TKey, TData, HashFunctor>::ContainsKey(const TKey& key) const
	{
		return Find(key) != end();
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::begin() const
	{
		return Iterator(this, mBeginIndex, mBuckets[mBeginIndex].begin());
	}

	template <typename TKey, typename TData, typename HashFunctor>
	typename HashMap<TKey, TData, HashFunctor>::Iterator HashMap<TKey, TData, HashFunctor>::end() const
	{
		return Iterator(this, mBuckets.GetSize() - 1, mBuckets[mBuckets.GetSize() - 1].end());
	}

#pragma endregion

}