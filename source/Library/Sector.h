#pragma once

/*!	\file Sector.h
	\brief Contains the declaration for the Sector class, which defines a slice of a world.
*/

#include "Attributed.h"
#include "Factory.h"
#include "Action.h"

namespace Library
{
	class Entity;
	class World;
	class WorldState;

	/*!	\brief Class that defines a slice of a world.
	*/
	class Sector final : public Attributed
	{
		RTTI_DECLARATIONS(Sector, Attributed);

	public:
		/*!	\brief Sets initial values based on the passed parameters
			@param mName Initial name to give this Sector.
		*/
		Sector(const std::string& mName = "Sector");

		/*!	\brief Destroys any owned data.
		*/
		virtual ~Sector() = default;

		/*!	\brief Initializes prescribed members.
		*/
		void Populate();

		/*!	\brief Returns a pointer to the Scope that holds the Actions for this object.
		*/
		Scope* GetActions();

		/*!	\brief Creates a new Action based on the name and instance strings within this Sector.
			@param actionClassName The classname of the Action to be created
			@param actionInstanceName The name to give the Action
			@return A pointer to the newly created Action.
			@throws std::runtime_error Thrown when the Action fails to be created.
		*/
		Action* CreateAction(const std::string& actionClassName, const std::string& actionInstanceName);

		/*!	\brief Returns the name of this sector
			@return The name of this sector.
		*/
		const std::string& GetName() const;

		/*!	\brief Sets the name of this sector
			@param name The name to set this sector's name to.
		*/
		void SetName(const std::string& name);

		/*!	\brief Retrieves a GUID string, which is only unique at the precision of the highest point in this object's hierarchy.
			@return A GUID string.
		*/
		std::string GetGUID() const;

		/*!	\brief Returns a pointer to the Entities Scope that this Sector owns.
			@return A pointer to the entities scope.
		*/
		Scope* GetEntities() const;

		/*!	\brief Creates a new entity based on the name and instance strings within this Sector.
			@param entityClassName The classname of the Entity to be created
			@param entityInstanceName The name to give the Entity
			@return A pointer to the newly created entity.
			@throws std::runtime_error Thrown when the Entity fails to be created.
		*/
		Entity* CreateEntity(const std::string& entityClassName, const std::string& entityInstanceName);

		/*!	\brief Sets the world that this Sector is contained in.
			@param newWorld The world to set this one to be contained within.
		*/
		void SetWorld(World* newWorld);

		/*!	\brief Returns a pointer to the world that contains this Sector.
			@return The world that contains this Sector.
		*/
		World* GetWorld() const;

		/*!	\brief Sets the active state of this object, based on the parameter.
			@param The active state to set this object to.
		*/
		void SetActive(int state);

		/*!	\brief Gets the active state of this object, and returns it.
			@return The active state of this object.
		*/
		int IsActive();

		/*!	\brief Per-frame update function that calls the function on all of its owned Entities.
			@param state The state of the world, to be considered per-frame.
		*/
		void Update(WorldState& state);
		
	private:
		std::string mName;
		std::uint32_t mLocalID;
		World* mWorld;
		Scope* mEntities;
		Scope* mActions;
	};

}