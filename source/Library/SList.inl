namespace Library
{

#pragma region Node
	template <typename T>
	SList<T>::Node::Node(const T& data, Node *next)
		: mData(data), mNext(next)
	{
	}
#pragma endregion

#pragma region Iterator
	template <typename T>
	SList<T>::Iterator::Iterator()
		: mOwner(nullptr), mNode(nullptr)
	{
	}

	template <typename T>
	SList<T>::Iterator::Iterator(const Iterator& rhs)
		: Iterator()
	{
		operator=(rhs);
	}

	template <typename T>
	SList<T>::Iterator::Iterator(const SList *owner, Node *node)
		: Iterator()
	{
		mOwner = owner;
		mNode = node;
	}

	template <typename T>
	typename SList<T>::Iterator& SList<T>::Iterator::operator=(const Iterator& rhs)
	{
		if (*this == rhs)
		{
			return *this;
		}

		mOwner = rhs.mOwner;
		mNode = rhs.mNode;

		return *this;
	}

	template <typename T>
	bool SList<T>::Iterator::operator==(const Iterator& rhs) const
	{
		return (mOwner == rhs.mOwner) && (mNode == rhs.mNode);
	}

	template <typename T>
	bool SList<T>::Iterator::operator!=(const Iterator& rhs) const
	{
		return !operator==(rhs);
	}

	template <typename T>
	typename SList<T>::Iterator& SList<T>::Iterator::operator++()
	{
		if (mNode != nullptr)
		{
			mNode = mNode->mNext;
		}

		return *this;
	}

	template <typename T>
	typename SList<T>::Iterator SList<T>::Iterator::operator++(int)
	{
		Iterator itCopy = *this;

		if (mNode != nullptr)
		{
			mNode = mNode->mNext;
		}

		return itCopy;
	}

	template <typename T>
	T& SList<T>::Iterator::operator*() const
	{
		if (mOwner != nullptr && mNode != nullptr)
		{
			return mNode->mData;
		}
		else 
		{
			throw std::runtime_error("Iterator dereferenced while not pointing to any data.");
		}
	}
#pragma endregion

	template <typename T>
	SList<T>::SList()
		: mFront(nullptr), mBack(nullptr), mSize(0)
	{

	}

	template <typename T>
	SList<T>::~SList()
	{
		Clear();
	}

	template <typename T>
	SList<T>::SList(const SList& rhs)
		: SList()
	{
		operator=(rhs);
	}

	template <typename T>
	SList<T>::SList(SList&& rhs)
		: mFront(rhs.mFront), mBack(rhs.mBack), mSize(rhs.mSize)
	{
		rhs.mFront = nullptr;
		rhs.mBack = nullptr;
		rhs.mSize = 0;
	}

	template <typename T>
	SList<T>& SList<T>::operator=(const SList& rhs)
	{
		// Do a self-test.
		if (this == &rhs)
		{
			return *this;
		}

		// First, clean up what might already exist.
		Clear();

		// Copy the list over, node by node.
		for (auto& value : rhs)
		{
			PushBack(value);
		}


		return *this;
	}

	template <typename T>
	SList<T>& SList<T>::operator=(SList&& rhs)
	{
		// Do a self-test.
		if (this == &rhs)
		{
			return *this;
		}

		// First, clean up what might already exist.
		Clear();

		// Move ownership of the data.
		mFront = rhs.mFront;
		mBack = rhs.mBack;
		mSize = rhs.mSize;
		rhs.mFront = nullptr;
		rhs.mBack = nullptr;
		mSize = 0;

		return *this;
	}

	template <typename T>
	void SList<T>::PushFront(const T& data)
	{
		// Construct the new node
		Node* newNode = new Node(data);
		
		// Prepend to the list
		newNode->mNext = mFront;
		mFront = newNode;
		mSize++;

		// Handle this being the only node in the list.
		if (mSize == 1)
		{
			mBack = newNode;
		}
	}

	template <typename T>
	void SList<T>::PushBack(const T& data)
	{
		// Construct the new node.
		Node* newNode = new Node(data);

		// Append to the list.
		if (mSize > 0)
		{
			mBack->mNext = newNode;
		}
		mBack = newNode;
		newNode->mNext = nullptr;
		mSize++;

		// Handle this being the only node in the list.
		if (mSize == 1)
		{
			mFront = newNode;
		}
	}

	template <typename T>
	T SList<T>::PopFront()
	{
		if (mSize == 0)
		{
			// Throw exception. Replace later with custom exception.
			throw std::runtime_error("SList is empty.");
		}

		// Temporarily save the first element information
		T data = mFront->mData;
		Node* toBeDeleted = mFront;

		// Remove the first element.
		mFront = toBeDeleted->mNext; // The last node's mNext == nullptr
		mSize--;
		delete toBeDeleted;

		// Did we empty the list on this one?
		if (mSize == 0)
		{
			mBack = nullptr;
		}

		// Return the data that was contained in the first node.
		return data;
	}

	template <typename T>
	T& SList<T>::Front() const
	{
		if (mSize == 0)
		{
			// Throw exception. Replace later with custom exception.
			throw std::runtime_error("SList is empty.");
		}

		return mFront->mData;
	}

	template <typename T>
	T& SList<T>::Back() const
	{
		if (mSize == 0)
		{
			// Throw exception. Replace later with custom exception.
			throw std::runtime_error("SList is empty.");
		}

		return mBack->mData;
	}

	template <typename T>
	void SList<T>::Clear()
	{
		while (mSize > 0)
		{
			PopFront();
		}
	}

	template <typename T>
	std::uint32_t SList<T>::GetSize() const
	{
		return mSize;
	}

	template <typename T>
	bool SList<T>::IsEmpty() const
	{
		return mSize == 0;
	}

	template <typename T>
	typename SList<T>::Iterator SList<T>::begin() const
	{
		return Iterator(this, mFront);
	}

	template <typename T>
	typename SList<T>::Iterator SList<T>::end() const
	{
		return Iterator(this, nullptr);
	}

	template <typename T>
	void SList<T>::InsertAfter(const Iterator& itLoc, const T& data)
	{
		// Be sure that this list can use the iterator.
		if (this != itLoc.mOwner)
		{
			throw std::runtime_error("Iterator parameter does not belong to called list.");
		}
		else
		{
			if (*itLoc == Back() || itLoc.mNode == nullptr)
			{
				// If past the end of the list, just append.
				PushBack(data);
			}
			else
			{
				// Insert the node in the list
				Node* newNode = new Node(data, itLoc.mNode->mNext);
				itLoc.mNode->mNext = newNode;
				mSize++;
			}
		}
	}

	template <typename T>
	typename SList<T>::Iterator SList<T>::FindNext(const Iterator& itLoc, const T& value) const
	{
		// Be sure that this list can use the iterator.
		if (this != itLoc.mOwner)
		{
			throw std::runtime_error("Iterator parameter does not belong to called list.");
		}
		else
		{
			Iterator nextLoc = itLoc;
			// Try to find an occurrence of the value.
			while (nextLoc != end() && nextLoc.mNode->mData != value)
			{
				++nextLoc;
			}

			// Return our findings.
			return nextLoc;
		}
	}

	template <typename T>
	typename SList<T>::Iterator SList<T>::Find(const T& value) const
	{
		return FindNext(begin(), value);
	}

	template <typename T>
	void SList<T>::Remove(const T& value)
	{
		Iterator prevLoc, thisLoc;

		thisLoc = begin();

		// Is the list empty?
		if (mSize == 0)
		{
			return;
		}
		// Test if it's at the beginning of the list.
		else if (thisLoc.mNode->mData == value)
		{
			PopFront();
			return;
		}
		// Search the list
		else
		{
			prevLoc = thisLoc;
			++thisLoc;
			while (thisLoc != end() && thisLoc.mNode->mData != value)
			{
				++prevLoc;
				++thisLoc;
			}

			// Did we find it?
			if (thisLoc != end())
			{
				prevLoc.mNode->mNext = thisLoc.mNode->mNext;
				delete thisLoc.mNode;
				mSize--;
				return;
			}
			else
			{
				return;
			}
		}
	}

	template <typename T>
	void SList<T>::RemoveAll(const T& value)
	{
		Iterator prevLoc, thisLoc;

		thisLoc = begin();

		// Is the list empty?
		if (mSize == 0)
		{
			return;
		}

		// Take off the one(s) at the beginning of the list.
		while (thisLoc.mNode->mData == value)
		{
			mFront = thisLoc.mNode->mNext;
			delete thisLoc.mNode;
			mSize--;
			thisLoc = begin();
		}

		prevLoc = thisLoc;
		++thisLoc;

		// Search the list for any remaining ones
		while (thisLoc != end())
		{
			while (thisLoc != end() && thisLoc.mNode->mData != value)
			{
				++prevLoc;
				++thisLoc;
			}

			// Did we find one?
			if (thisLoc != end())
			{
				prevLoc.mNode->mNext = thisLoc.mNode->mNext;
				delete thisLoc.mNode;
				mSize--;
				thisLoc = prevLoc;
				++thisLoc;
			}
		}
	}
}