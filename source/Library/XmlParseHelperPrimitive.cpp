#include "pch.h"
#include "XmlParseHelperPrimitive.h"
#include "SharedDataTable.h"
#include "Factory.h"
#include "Entity.h"
#include "Sector.h"
#include "World.h"

namespace Library
{

#pragma region Tors

	IXmlParseHelper* XmlParseHelperPrimitive::Clone() const
	{
		IXmlParseHelper* newHelper = new XmlParseHelperPrimitive();

		return newHelper;
	}

#pragma endregion

#pragma region Handlers

	bool XmlParseHelperPrimitive::StartElementHandler(SharedDataTable::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mDataTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mIntegerTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mIntegerTag)) &&
			!(name == SharedDataTable::mFloatTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mFloatTag)) && 
			!(name == SharedDataTable::mVectorTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mVectorTag)) &&
			!(name == SharedDataTable::mMatrixTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mMatrixTag)) &&
			!(name == SharedDataTable::mStringTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mStringTag)))
		{
			return false;
		}

		// Ensure this object at least has a name, assuming it's not a value tag.
		if (name != SharedDataTable::mValueTag && !attributes.ContainsKey(SharedDataTable::mNameAttribute))
		{
			throw std::runtime_error("New data types must have a name attribute. Offending XML Tag type: " + name);
		}

		// Ensure the user isn't trying to use a reserved name.
		for (auto str : SharedDataTable::mReservedKeys)
		{
			if (attributes.ContainsKey(str))
			{
				throw std::runtime_error("Attribute of type (" + name + ") attempted to use reserved name: " + str);
			}
		}

		if (sharedData->GetDepth() > 1)
		{
			// If this is a value tag, mark parent as an array by ensuring that key is contained in their hashmap.
			if (name == SharedDataTable::mValueTag)
			{
				shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			}
			// Otherwise, give it a scalar flag key.
			else
			{
				shared->mStack.Top()[SharedDataTable::mIsScalarKey];
			}
		}

		// Add object onto the stack.
		shared->mStack.Push(HashMap<std::string, Datum>());

		// Prepare the character data string buffer to be filled.
		shared->mStack.Top()[SharedDataTable::mStringDataKey] = std::string();

		// Set the type strings.
		shared->mStack.Top()[SharedDataTable::mDataTypeKey] = name;
		shared->mStack.Top()[SharedDataTable::mParentTypeKey] = parentType;

		// Add all attributes to the pile of data known, and retain their string form.
		for (auto& val : attributes)
		{
			Datum tmpDat;
			tmpDat = val.second;
			shared->mStack.Top().Insert(val.first, tmpDat);
		}

		// Preallocate an array, if specified.
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mSizeAttribute))
		{
			std::string sizeNumStr = shared->mStack.Top()[SharedDataTable::mSizeAttribute].Get<std::string>();

			std::uint32_t size = std::strtoul(sizeNumStr.c_str(), nullptr, 0);
			if (size == 0)
			{
				throw std::runtime_error("Invalid value in type (" + name + ") given for array size: " + sizeNumStr);
			}

			Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
			valuesArray.SetType(Datum::DatumType::String);
			valuesArray.SetSize(size);
			shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey] = 0;
		}

		return true;
	}

	bool XmlParseHelperPrimitive::EndElementHandler(SharedDataTable::SharedData* sharedData, std::string name)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent tag.
		std::string parentType = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mParentTypeKey)->second.Get<std::string>();
		}

		// Ensure this a correct tag.
		if (!(name == SharedDataTable::mIntegerTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mIntegerTag)) &&
			!(name == SharedDataTable::mFloatTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mFloatTag)) &&
			!(name == SharedDataTable::mVectorTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mVectorTag)) &&
			!(name == SharedDataTable::mMatrixTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mMatrixTag)) &&
			!(name == SharedDataTable::mStringTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mStringTag)))
		{
			return false;
		}

		// Finish handling based on the type.
		if (name == SharedDataTable::mValueTag)
		{
			ParseValue(shared, name);
		}
		else if (name == SharedDataTable::mIntegerTag)
		{
			ParsePrimitive(shared, name, Datum::DatumType::Integer);
		}
		else if (name == SharedDataTable::mFloatTag)
		{
			ParsePrimitive(shared, name, Datum::DatumType::Float);
		}
		else if (name == SharedDataTable::mVectorTag)
		{
			ParsePrimitive(shared, name, Datum::DatumType::Vector);
		}
		else if (name == SharedDataTable::mMatrixTag)
		{
			ParsePrimitive(shared, name, Datum::DatumType::Matrix);
		}
		else
		{
			ParsePrimitive(shared, name, Datum::DatumType::String);
		}

		return true;
	}

	bool XmlParseHelperPrimitive::CharElementHandler(SharedDataTable::SharedData* sharedData, const char* data, int length)
	{
		// Test if this is a recognized type of SharedData.
		if (!sharedData->Is("SharedDataTable"))
		{
			return false;
		}
		SharedDataTable* shared = reinterpret_cast<SharedDataTable*>(sharedData);

		// Get the parent and name tags.
		std::string parentType = "";
		std::string name = "";
		if (shared->mStack.GetSize() > 0)
		{
			parentType = shared->mStack.Top().Find(SharedDataTable::mParentTypeKey)->second.Get<std::string>();
			name = shared->mStack.Top().Find(SharedDataTable::mDataTypeKey)->second.Get<std::string>();
		}

		// Ensure this is a string related to a known tag.
		if (!(name == SharedDataTable::mIntegerTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mIntegerTag)) &&
			!(name == SharedDataTable::mFloatTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mFloatTag)) &&
			!(name == SharedDataTable::mVectorTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mVectorTag)) &&
			!(name == SharedDataTable::mMatrixTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mMatrixTag)) &&
			!(name == SharedDataTable::mStringTag || (name == SharedDataTable::mValueTag && parentType == SharedDataTable::mStringTag)))
		{
			return false;
		}

		// Test that this element wasn't previously marked as an array.
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Ambiguous declaration between scalar and array detected at string: " + std::string(data));
		}
		else
		{
			// Mark this as a scalar.
			shared->mStack.Top()[SharedDataTable::mIsScalarKey];
		}

		// Add this data to the string buffer, because it might not be a complete string.
		shared->mStack.Top()[SharedDataTable::mStringDataKey].Get<std::string>() += std::string(data);

		return true;
	}

#pragma endregion

#pragma region Helpers

	void XmlParseHelperPrimitive::ParseValue(SharedDataTable* shared, std::string name)
	{
		// Ensure that this is a potentially valid depth.
		if (shared->GetDepth() <= 1)
		{
			throw std::runtime_error("Invalid depth: " + name);
		}

		// Go ahead and pop it off.
		auto poppedMap = shared->mStack.Pop();

		// Check if the parent tag is invalid.
		if (shared->mStack.Top()[SharedDataTable::mDataTypeKey] == SharedDataTable::mValueTag)
		{
			throw std::runtime_error("Tags of type " + poppedMap[SharedDataTable::mDataTypeKey].Get<std::string>() + " cannot have tags of type " + SharedDataTable::mValueTag + " as parents.");
		}

		// Add it to the parent's values array.
		Datum& valuesArray = shared->mStack.Top()[SharedDataTable::mArrayElementsKey];
		std::int32_t index;
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mNumArrayElementsKey))
		{
			index = shared->mStack.Top()[SharedDataTable::mNumArrayElementsKey].Get<int>()++;
		}
		else
		{
			index = shared->mStack.Top()[SharedDataTable::mArrayElementsKey].GetSize();
		}

		valuesArray.Set(poppedMap[SharedDataTable::mStringDataKey].Get<std::string>(), index);
	}

	void XmlParseHelperPrimitive::ParsePrimitive(SharedDataTable* shared, std::string name, Datum::DatumType type)
	{
		// Ensure that this is a potentially valid depth.
		if (shared->GetDepth() <= 1)
		{
			throw std::runtime_error("Invalid depth: " + name);
		}

		// Check for ambiguity on whether scalar or array.
		if (shared->mStack.Top().ContainsKey(SharedDataTable::mIsScalarKey) && shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Ambiguous declaration between scalar and array: " + name);
		}

		// Ensure data was provided.
		if (!shared->mStack.Top().ContainsKey(SharedDataTable::mIsScalarKey) && !shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Must provide data: " + name);
		}

		auto poppedMap = shared->mStack.Pop();

		// Check that parent XML tag is not a table array begin tag.
		if (shared->mStack.Top()[SharedDataTable::mDataTypeKey] == SharedDataTable::mTableTag && shared->mStack.Top().ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			throw std::runtime_error("Attempted to place non-table data in table array.");
		}

		// Handle if array
		if (poppedMap.ContainsKey(SharedDataTable::mArrayElementsKey))
		{
			// Cache some data.
			Datum& datArray = poppedMap[SharedDataTable::mArrayElementsKey];
			auto& parent = shared->mStack.Top();
			std::string& datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();

			parent[datName].SetType(type);
			for (std::uint32_t i = 0; i < datArray.GetSize(); ++i)
			{
				parent[datName].SetFromString(datArray.Get<std::string>(i), i);
			}
		}
		// Handle if scalar
		else
		{
			// Cache some data.
			auto& parent = shared->mStack.Top();
			std::string& datName = poppedMap[SharedDataTable::mNameAttribute].Get<std::string>();

			parent[datName].SetType(type);
			parent[datName].SetFromString(poppedMap[SharedDataTable::mStringDataKey].Get<std::string>());
		}
	}

#pragma endregion

}