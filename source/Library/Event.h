#pragma once

/*!	\file Event.h
	\brief Contains a template for events.
*/

#include "pch.h"
#include "EventPublisher.h"
#include "EventSubscriber.h"

namespace Library
{

	/*!	\brief Template for events that carry messages which are expected to ultimately derive from RTTI.
	*/
	template <typename ConcreteMessageT>
	class Event : public EventPublisher
	{
		RTTI_DECLARATIONS(Event<ConcreteMessageT>, EventPublisher);

	public:
		/*!	\brief Constructs a new Event based on the parameter.
			@param message The RTTI message for this Event to carry.
		*/
		explicit Event(const ConcreteMessageT& message);

		/*!	\brief Destructs any owned data.
		*/
		virtual ~Event() = default;

		/*!	\brief Adds a new EventSubscriber to the list of subscribers for this event type.
			@param subscriber The subscriber to add.
		*/
		static void Subscribe(EventSubscriber& subscriber);

		/*!	\brief Removes an EventSubscriber from the list of subscribers for this event type.
			@param subscriber The subscriber to remove.
		*/
		static void Unsubscribe(EventSubscriber& subscriber);

		/*!	\brief Empties the EventSubscriber list for this event type.
		*/
		static void UnsubscribeAll();

		/*!	\brief Gets the message carried around by this Event.
		*/
		const ConcreteMessageT& GetMessage();

	private:
		// The list of subscribers for this type of Event.
		static SList<EventSubscriber*> mSubscribers;
		static std::mutex mSubscribersMutex;

		// The message this Event carries.
		ConcreteMessageT mMessage;
	};

}

#include "Event.inl"