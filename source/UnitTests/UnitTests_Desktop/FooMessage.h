#pragma once

#include "RTTI.h"

namespace UnitTests
{

	class FooMessage : public Library::RTTI
	{
		RTTI_DECLARATIONS(FooMessage, RTTI);

	public:
		FooMessage();
		FooMessage(const FooMessage& rhs);
		virtual ~FooMessage() = default;
		FooMessage& operator=(const FooMessage& rhs);

		void SetValue(int val);
		int GetValue() const;
	private:
		int mVal;
	};

}