#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include <iostream>
#include <atomic>
#include <random>

#include "FooMessage.h"
#include "Event.h"
#include "EventQueue.h"

using namespace Library;
using namespace std;
using namespace std::chrono;
using namespace UnitTests;

class EventAsyncTestSuite : public CxxTest::TestSuite
{
private:
	class FooSubscriber : public EventSubscriber
	{
	public:
		virtual ~FooSubscriber() = default;
		virtual void Notify(shared_ptr<EventPublisher> eventPublisher) = 0;
		void SetQueue(EventQueue& queue)
		{
			mQueue = &queue;
		}
		void SetTimeNow(const high_resolution_clock::time_point& time)
		{
			mTime = time;
		}

		atomic<int> mNumTimesNotified = 0;

	protected:
		EventQueue* mQueue;
		high_resolution_clock::time_point mTime;
	};

	class FooSubscriber1 : public FooSubscriber
	{
	public:
		virtual ~FooSubscriber1() = default;
		virtual void Notify(shared_ptr<EventPublisher> eventPublisher)
		{
			const FooMessage& message = eventPublisher->As<Event<FooMessage>>()->GetMessage();
			if (message.GetValue() == 1)
			{
				++mNumTimesNotified;
				mQueue->Update(mTime);
			}
		}
	};

	class FooSubscriber2 : public FooSubscriber
	{
	public:
		virtual ~FooSubscriber2() = default;
		virtual void Notify(shared_ptr<EventPublisher> eventPublisher)
		{
			const FooMessage& message = eventPublisher->As<Event<FooMessage>>()->GetMessage();
			if (message.GetValue() == 2)
			{
				++mNumTimesNotified;
				mQueue->Enqueue(eventPublisher, mTime, minutes(10));
			}
		}
	};

	class FooSubscriber3 : public FooSubscriber
	{
	public:
		virtual ~FooSubscriber3() = default;
		virtual void Notify(shared_ptr<EventPublisher> eventPublisher)
		{
			const FooMessage& message = eventPublisher->As<Event<FooMessage>>()->GetMessage();
			if (message.GetValue() == 3)
			{
				// Multiple Notify calls may already exist at this point before unsubscription.
				if (bSubscribed.exchange(false))
				{
					++mNumTimesNotified;
					Event<FooMessage>::Unsubscribe(*this);
				}
			}
		}
	private:
		atomic<bool> bSubscribed = true;
	};

public:

	void tearDown()
	{
		Event<FooMessage>::UnsubscribeAll();
	}

	void TestEventAsync()
	{
		default_random_engine generator;
		uniform_int_distribution<int> distribution(1, 3);
		auto fooVal = bind(distribution, generator);

		EventQueue queue;
		FooSubscriber1 fooSub1;
		FooSubscriber2 fooSub2;
		FooSubscriber3 fooSub3;
		FooMessage foo;
		high_resolution_clock::time_point time = high_resolution_clock::now();

		int fooSub1ExpectedCount = 0;
		int fooSub2ExpectedCount = 0;
		int fooSub3ExpectedCount = 0;

		// Prepare the subscribers.
		TS_ASSERT_THROWS_NOTHING(fooSub1.SetQueue(queue));
		TS_ASSERT_THROWS_NOTHING(fooSub2.SetQueue(queue));
		TS_ASSERT_THROWS_NOTHING(fooSub3.SetQueue(queue));
		TS_ASSERT_THROWS_NOTHING(fooSub1.SetTimeNow(time));
		TS_ASSERT_THROWS_NOTHING(fooSub2.SetTimeNow(time));
		TS_ASSERT_THROWS_NOTHING(fooSub3.SetTimeNow(time));
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub1));
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub2));
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub3));

		// Load down the event queue.
		for (int i = 0; i < 3000; ++i)
		{
			int nextFoo = fooVal();
			switch (nextFoo)
			{
			case 1:
				++fooSub1ExpectedCount;
				break;
			case 2:
				++fooSub2ExpectedCount;
				break;
			case 3:
				++fooSub3ExpectedCount;
				break;
			default:
				break;
			}
			TS_ASSERT_THROWS_NOTHING(foo.SetValue(nextFoo));

			auto fooEvent = make_shared<Event<FooMessage>>(foo);
			TS_ASSERT_THROWS_NOTHING(queue.Enqueue(fooEvent, time, minutes(10)));
		}

		// Unleash the beast.
		time += minutes(11);
		TS_ASSERT_THROWS_NOTHING(queue.Update(time));

		// Ensure all happened as expected.
		TS_ASSERT_EQUALS(fooSub1.mNumTimesNotified.load(), fooSub1ExpectedCount);

		TS_ASSERT_EQUALS(fooSub2.mNumTimesNotified.load(), fooSub2ExpectedCount);
		TS_ASSERT_EQUALS(queue.GetSize(), fooSub2ExpectedCount);

		TS_ASSERT_EQUALS(fooSub3.mNumTimesNotified.load(), 1);
	}

};