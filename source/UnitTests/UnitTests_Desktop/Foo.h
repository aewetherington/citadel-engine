#pragma once
#include <cstdint>
#include "RTTI.h"

class Foo : public Library::RTTI
{
	RTTI_DECLARATIONS(Foo, Library::RTTI)
public:
	Foo(const float val = 0.0f);
	Foo(const Foo& rhs);
	virtual ~Foo();

	void setValue(float val);
	float getValue() const;

	Foo& operator=(const Foo& rhs);
	bool operator==(const Foo& rhs) const;
	bool operator!=(const Foo& rhs) const;

	float *mValue;
};

class FooHash
{
public:
	std::uint32_t operator()(const Foo& key) const;
};