#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include "Datum.h"
#include "Scope.h"
#include "Foo.h"

using namespace Library;

class DatumTestSuite : public CxxTest::TestSuite
{
public:

	void TestDatumMove()
	{
		Datum dExt, dInt, dTmp, dTmp2;
		std::int32_t tmp[] = { 1, 2, 3, 4, 5 };
		dExt.SetStorage(tmp, 5);
		dInt.Set(5, 0);
		dInt.Set(4, 1);
		dInt.Set(3, 2);
		dInt.Set(2, 3);
		dInt.Set(1, 4);

		// Move internal storage ownership.
		TS_ASSERT_EQUALS(dInt.GetSize(), 5);
		TS_ASSERT_EQUALS(dInt.GetType(), Datum::DatumType::Integer);
		TS_ASSERT(!dInt.IsExternal());
		TS_ASSERT_EQUALS(dTmp.GetSize(), 0);
		TS_ASSERT_EQUALS(dTmp.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dTmp.IsExternal());
		dTmp = std::move(dInt);
		TS_ASSERT_EQUALS(dInt.GetSize(), 0);
		TS_ASSERT_EQUALS(dInt.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dInt.IsExternal());
		TS_ASSERT_EQUALS(dTmp.GetSize(), 5);
		TS_ASSERT_EQUALS(dTmp.GetType(), Datum::DatumType::Integer);
		TS_ASSERT(!dTmp.IsExternal());

		// Move external storage ownership.
		TS_ASSERT_EQUALS(dExt.GetSize(), 5);
		TS_ASSERT_EQUALS(dExt.GetType(), Datum::DatumType::Integer);
		TS_ASSERT(dExt.IsExternal());
		TS_ASSERT_EQUALS(dTmp2.GetSize(), 0);
		TS_ASSERT_EQUALS(dTmp2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dTmp2.IsExternal());
		dTmp2 = std::move(dExt);
		TS_ASSERT_EQUALS(dExt.GetSize(), 0);
		TS_ASSERT_EQUALS(dExt.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dExt.IsExternal());
		TS_ASSERT_EQUALS(dTmp2.GetSize(), 5);
		TS_ASSERT_EQUALS(dTmp2.GetType(), Datum::DatumType::Integer);
		TS_ASSERT(dTmp2.IsExternal());
	}

	void TestDatumCopy()
	{
		Datum dExt, dInt, dTmp;
		std::int32_t tmp[] = { 1, 2, 3, 4, 5 };
		dExt.SetStorage(tmp, 5);
		dInt.Set(5, 0);
		dInt.Set(4, 1);
		dInt.Set(3, 2);
		dInt.Set(2, 3);
		dInt.Set(1, 4);

		TS_ASSERT(dExt.IsExternal());
		TS_ASSERT(!dInt.IsExternal());

		TS_ASSERT(!dTmp.IsExternal());
		// Internal = Internal
		dTmp.Set(0, 0);
		dTmp = dInt;
		TS_ASSERT_EQUALS(dTmp.GetSize(), 5);
		TS_ASSERT_EQUALS(dTmp.Get<std::int32_t>(0), 5);
		TS_ASSERT(!dTmp.IsExternal());

		// Internal = External
		dTmp.Set(0, 0);
		dTmp = dExt;
		TS_ASSERT_EQUALS(dTmp.GetSize(), 5);
		TS_ASSERT_EQUALS(dTmp.Get<std::int32_t>(0), 1);
		TS_ASSERT(dTmp.IsExternal());

		// External = External
		dTmp.Set(0, 0);
		dTmp = dExt;
		TS_ASSERT_EQUALS(dTmp.GetSize(), 5);
		TS_ASSERT_EQUALS(dTmp.Get<std::int32_t>(0), 0);
		TS_ASSERT(dTmp.IsExternal());

		// External = Internal
		dTmp.Set(1, 0);
		dTmp = dInt;
		TS_ASSERT_EQUALS(dTmp.GetSize(), 5);
		TS_ASSERT_EQUALS(dTmp.Get<std::int32_t>(0), 5);
		TS_ASSERT(!dTmp.IsExternal());
		TS_ASSERT_EQUALS(tmp[0], 1);
	}

	void TestDatumIntegerSet()
	{
		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = 1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Integer);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(), 1);
		TS_ASSERT_EQUALS(dat1, 1);
		dat1 = 2;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(), 2);
		TS_ASSERT_EQUALS(dat1, 2);

		// Test growing.
		dat1.Set(3, 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(0), 2);
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(2), 3);
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(0), 2);
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(2), 3);

		// Test shrinking
		dat1.Set(4, 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(0), 2);
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(1), 4);

		// Test scalar reassignment
		dat1 = 5;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, 5);
	}

	void TestDatumIntegerSetStorage()
	{
		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::Integer);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Integer);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		std::int32_t tmp1[] = { 1, 2, 3 };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], 2);
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(1), 2);
		TS_ASSERT_EQUALS(dat2.Get<std::int32_t>(1), 2);
		dat1.Set(20, 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], 20);
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(1), 20);
		TS_ASSERT_EQUALS(dat2.Get<std::int32_t>(1), 20);

		// Test comparison
		std::int32_t tmp2[] = { 1, 2, 3 };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 != dat2);
		tmp2[1] = 20;
		TS_ASSERT(dat1 == dat2);

		// Test FromString, and failing to enlarge external storage.
		TS_ASSERT_THROWS_ANYTHING(dat1.SetFromString("10", 3));
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT_THROWS_NOTHING(dat1.SetFromString("10", 0));
		TS_ASSERT_EQUALS(dat1.Get<std::int32_t>(0), 10);
		TS_ASSERT(dat1.IsExternal());

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));
	}

	void TestDatumFloatSet()
	{
		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = 1.0f;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Float);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<float>(), 1.0f);
		TS_ASSERT_EQUALS(dat1, 1.0f);
		dat1 = 2.0f;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<float>(), 2.0f);
		TS_ASSERT_EQUALS(dat1, 2.0f);

		// Test growing.
		dat1.Set(3.0f, 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<float>(0), 2.0f);
		TS_ASSERT_EQUALS(dat1.Get<float>(2), 3.0f);
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<float>(0), 2.0f);
		TS_ASSERT_EQUALS(dat1.Get<float>(2), 3.0f);

		// Test shrinking
		dat1.Set(4.0f, 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<float>(0), 2.0f);
		TS_ASSERT_EQUALS(dat1.Get<float>(1), 4.0f);

		// Test scalar reassignment
		dat1 = 5.0f;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, 5.0f);
	}

	void TestDatumFloatSetStorage()
	{
		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::Float);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Float);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		float tmp1[] = { 1.0f, 2.0f, 3.0f };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], 2.0f);
		TS_ASSERT_EQUALS(dat1.Get<float>(1), 2.0f);
		TS_ASSERT_EQUALS(dat2.Get<float>(1), 2.0f);
		dat1.Set(20.0f, 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], 20.0f);
		TS_ASSERT_EQUALS(dat1.Get<float>(1), 20.0f);
		TS_ASSERT_EQUALS(dat2.Get<float>(1), 20.0f);

		// Test comparison
		float tmp2[] = { 1.0f, 2.0f, 3.0f };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 != dat2);
		tmp2[1] = 20.0f;
		TS_ASSERT(dat1 == dat2);

		// Test FromString, and failing to enlarge external storage.
		TS_ASSERT_THROWS_ANYTHING(dat1.SetFromString("10.0", 3));
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT_THROWS_NOTHING(dat1.SetFromString("10.0", 0));
		TS_ASSERT_EQUALS(dat1.Get<float>(0), 10.0f);
		TS_ASSERT(dat1.IsExternal());

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));

	}

	void TestDatumVectorSet()
	{
		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = glm::vec4(1.0f);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Vector);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(), glm::vec4(1.0f));
		TS_ASSERT_EQUALS(dat1, glm::vec4(1.0f));
		dat1 = glm::vec4(2.0f);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(), glm::vec4(2.0f));
		TS_ASSERT_EQUALS(dat1, glm::vec4(2.0f));

		// Test growing.
		dat1.Set(glm::vec4(3.0f), 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(0), glm::vec4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(2), glm::vec4(3.0f));
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(0), glm::vec4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(2), glm::vec4(3.0f));

		// Test shrinking
		dat1.Set(glm::vec4(4.0f), 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(0), glm::vec4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(1), glm::vec4(4.0f));

		// Test scalar reassignment
		dat1 = glm::vec4(5.0f);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, glm::vec4(5.0f));
	}

	void TestDatumVectorSetStorage()
	{
		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::Vector);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Vector);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		glm::vec4 tmp1[] = { glm::vec4(1.0f), glm::vec4(2.0f), glm::vec4(3.0f) };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], glm::vec4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(1), glm::vec4(2.0f));
		TS_ASSERT_EQUALS(dat2.Get<glm::vec4>(1), glm::vec4(2.0f));
		dat1.Set(glm::vec4(20.0f), 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], glm::vec4(20.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(1), glm::vec4(20.0f));
		TS_ASSERT_EQUALS(dat2.Get<glm::vec4>(1), glm::vec4(20.0f));

		// Test comparison
		glm::vec4 tmp2[] = { glm::vec4(1.0f), glm::vec4(2.0f), glm::vec4(3.0f) };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 != dat2);
		tmp2[1] = glm::vec4(20.0f);
		TS_ASSERT(dat1 == dat2);

		// Test FromString, and failing to enlarge external storage.
		TS_ASSERT_THROWS_ANYTHING(dat1.SetFromString("(10.0, 10.0, 10.0, 10.0)", 3));
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT_THROWS_NOTHING(dat1.SetFromString("(10.0, 10.0, 10.0, 10.0)", 0));
		TS_ASSERT_EQUALS(dat1.Get<glm::vec4>(0), glm::vec4(10.0f));
		TS_ASSERT(dat1.IsExternal());

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));
	}

	void TestDatumMatrixSet()
	{
		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = glm::mat4(1.0f);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Matrix);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(), glm::mat4(1.0f));
		TS_ASSERT_EQUALS(dat1, glm::mat4(1.0f));
		dat1 = glm::mat4(2.0f);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(), glm::mat4(2.0f));
		TS_ASSERT_EQUALS(dat1, glm::mat4(2.0f));

		// Test growing.
		dat1.Set(glm::mat4(3.0f), 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(0), glm::mat4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(2), glm::mat4(3.0f));
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(0), glm::mat4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(2), glm::mat4(3.0f));

		// Test shrinking
		dat1.Set(glm::mat4(4.0f), 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(0), glm::mat4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(1), glm::mat4(4.0f));

		// Test scalar reassignment
		dat1 = glm::mat4(5.0f);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, glm::mat4(5.0f));
	}

	void TestDatumMatrixSetStorage()
	{
		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::Matrix);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Matrix);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		glm::mat4 tmp1[] = { glm::mat4(1.0f), glm::mat4(2.0f), glm::mat4(3.0f) };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], glm::mat4(2.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(1), glm::mat4(2.0f));
		TS_ASSERT_EQUALS(dat2.Get<glm::mat4>(1), glm::mat4(2.0f));
		dat1.Set(glm::mat4(20.0f), 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], glm::mat4(20.0f));
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(1), glm::mat4(20.0f));
		TS_ASSERT_EQUALS(dat2.Get<glm::mat4>(1), glm::mat4(20.0f));

		// Test comparison
		glm::mat4 tmp2[] = { glm::mat4(1.0f), glm::mat4(2.0f), glm::mat4(3.0f) };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 != dat2);
		tmp2[1] = glm::mat4(20.0f);
		TS_ASSERT(dat1 == dat2);

		// Test FromString, and failing to enlarge external storage.
		TS_ASSERT_THROWS_ANYTHING(dat1.SetFromString("[10.0 0.0 0.0 0.0, 0.0 10.0 0.0 0.0, 0.0 0.0 10.0 0.0, 0.0 0.0 0.0 10.0]", 3));
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT_THROWS_NOTHING(dat1.SetFromString("[10.0 0.0 0.0 0.0, 0.0 10.0 0.0 0.0, 0.0 0.0 10.0 0.0, 0.0 0.0 0.0 10.0]", 0));
		TS_ASSERT_EQUALS(dat1.Get<glm::mat4>(0), glm::mat4(10.0f));
		TS_ASSERT(dat1.IsExternal());

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));

	}

	void TestDatumStringSet()
	{
		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = "1";
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::String);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::string>(), "1");
		TS_ASSERT_EQUALS(dat1, "1");
		dat1 = "2";
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::string>(), "2");
		TS_ASSERT_EQUALS(dat1, "2");

		// Test growing.
		dat1.Set("3", 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::string>(0), "2");
		TS_ASSERT_EQUALS(dat1.Get<std::string>(2), "3");
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::string>(0), "2");
		TS_ASSERT_EQUALS(dat1.Get<std::string>(2), "3");

		// Test shrinking
		dat1.Set("4", 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.Get<std::string>(0), "2");
		TS_ASSERT_EQUALS(dat1.Get<std::string>(1), "4");

		// Test scalar reassignment
		dat1 = "5";
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, "5");
	}

	void TestDatumStringSetStorage()
	{
		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::String);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::String);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		std::string tmp1[] = { "1", "2", "3" };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], "2");
		TS_ASSERT_EQUALS(dat1.Get<std::string>(1), "2");
		TS_ASSERT_EQUALS(dat2.Get<std::string>(1), "2");
		dat1.Set("20", 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], "20");
		TS_ASSERT_EQUALS(dat1.Get<std::string>(1), "20");
		TS_ASSERT_EQUALS(dat2.Get<std::string>(1), "20");

		// Test comparison
		std::string tmp2[] = { "1", "2", "3" };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 != dat2);
		tmp2[1] = "20";
		TS_ASSERT(dat1 == dat2);

		// Test FromString, and failing to enlarge external storage.
		TS_ASSERT_THROWS_ANYTHING(dat1.SetFromString("Ten!", 3));
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT_THROWS_NOTHING(dat1.SetFromString("Ten!", 0));
		TS_ASSERT_EQUALS(dat1.Get<std::string>(0), "Ten!");
		TS_ASSERT(dat1.IsExternal());

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));

	}

	void TestDatumPointerSet()
	{
		RTTI* foo1 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo2 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo3 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo4 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo5 = reinterpret_cast<RTTI*>(new Foo());

		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = foo1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Pointer);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(), foo1);
		TS_ASSERT_EQUALS(dat1, foo1);
		dat1 = foo2;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(), foo2);
		TS_ASSERT_EQUALS(dat1, foo2);

		// Test growing.
		dat1.Set(foo3, 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(0), foo2);
		TS_ASSERT_EQUALS(dat1.GetPointer(2), foo3);
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(0), foo2);
		TS_ASSERT_EQUALS(dat1.GetPointer(2), foo3);

		// Test shrinking
		dat1.Set(foo4, 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(0), foo2);
		TS_ASSERT_EQUALS(dat1.GetPointer(1), foo4);

		// Test scalar reassignment
		dat1 = foo5;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, foo5);

		delete reinterpret_cast<Foo*>(foo1);
		delete reinterpret_cast<Foo*>(foo2);
		delete reinterpret_cast<Foo*>(foo3);
		delete reinterpret_cast<Foo*>(foo4);
		delete reinterpret_cast<Foo*>(foo5);
	}

	void TestDatumPointerSetStorage()
	{
		RTTI* foo0 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo1 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo2 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo3 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo4 = reinterpret_cast<RTTI*>(new Foo());
		RTTI* foo5 = reinterpret_cast<RTTI*>(new Foo());

		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::Pointer);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Pointer);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		RTTI* tmp1[] = { foo1, foo2, foo3 };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], foo2);
		TS_ASSERT_EQUALS(dat1.GetPointer(1), foo2);
		TS_ASSERT_EQUALS(dat2.GetPointer(1), foo2);
		dat1.Set(foo0, 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], foo0);
		TS_ASSERT_EQUALS(dat1.GetPointer(1), foo0);
		TS_ASSERT_EQUALS(dat2.GetPointer(1), foo0);

		// Test comparison
		RTTI* tmp2[] = { foo1, foo2, foo3 };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 != dat2);
		tmp2[1] = foo0;
		TS_ASSERT(dat1 == dat2);

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));

		delete reinterpret_cast<Foo*>(foo0);
		delete reinterpret_cast<Foo*>(foo1);
		delete reinterpret_cast<Foo*>(foo2);
		delete reinterpret_cast<Foo*>(foo3);
		delete reinterpret_cast<Foo*>(foo4);
		delete reinterpret_cast<Foo*>(foo5);
	}

	void TestDatumTableSet()
	{
		Scope* scope1 = new Scope();
		Scope* scope2 = new Scope();
		Scope* scope3 = new Scope();
		Scope* scope4 = new Scope();
		Scope* scope5 = new Scope();

		// Test scalar assignment
		Datum dat1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT_EQUALS(dat1.GetSize(), 0);
		TS_ASSERT(!dat1.IsExternal());
		dat1 = scope1;
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Table);
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(), scope1);
		TS_ASSERT_EQUALS(dat1, scope1);
		dat1 = scope2;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(), scope2);
		TS_ASSERT_EQUALS(dat1, scope2);

		// Test growing.
		dat1.Set(scope3, 2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 3);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(0), scope2);
		TS_ASSERT_EQUALS(dat1.GetPointer(2), scope3);
		dat1.SetSize(5);
		TS_ASSERT_EQUALS(dat1.GetSize(), 5);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(0), scope2);
		TS_ASSERT_EQUALS(dat1.GetPointer(2), scope3);

		// Test shrinking
		dat1.Set(scope4, 1);
		dat1.SetSize(2);
		TS_ASSERT_EQUALS(dat1.GetSize(), 2);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1.GetPointer(0), scope2);
		TS_ASSERT_EQUALS(dat1.GetPointer(1), scope4);

		// Test scalar reassignment
		dat1 = scope5;
		TS_ASSERT_EQUALS(dat1.GetSize(), 1);
		TS_ASSERT(!dat1.IsExternal());
		TS_ASSERT_EQUALS(dat1, scope5);

		delete scope1;
		delete scope2;
		delete scope3;
		delete scope4;
		delete scope5;
	}

	void TestDatumTableSetStorage()
	{
		Scope* scope0 = new Scope();
		Scope* scope1 = new Scope();
		Scope* scope2 = new Scope();
		Scope* scope3 = new Scope();
		Scope* scope4 = new Scope();
		Scope* scope5 = new Scope();

		// Set the type.
		Datum dat1, dat2;
		dat1.SetType(Datum::DatumType::Table);
		TS_ASSERT_EQUALS(dat1.GetType(), Datum::DatumType::Table);
		TS_ASSERT_EQUALS(dat2.GetType(), Datum::DatumType::Unknown);
		TS_ASSERT(!dat1.IsExternal());

		// Test SetStorage
		Scope* tmp1[] = { scope1, scope2, scope3 };
		dat1.SetStorage(tmp1, 3);
		TS_ASSERT(dat1.IsExternal());
		dat2.SetStorage(tmp1, 3);
		TS_ASSERT_EQUALS(tmp1[1], scope2);
		TS_ASSERT_EQUALS(dat1.GetPointer(1), scope2);
		TS_ASSERT_EQUALS(dat2.GetPointer(1), scope2);
		dat1.Set(scope0, 1);
		TS_ASSERT(dat1.IsExternal());
		TS_ASSERT_EQUALS(tmp1[1], scope0);
		TS_ASSERT_EQUALS(dat1.GetPointer(1), scope0);
		TS_ASSERT_EQUALS(dat2.GetPointer(1), scope0);

		// Test comparison
		Scope* tmp2[] = { scope1, scope2, scope3 };
		TS_ASSERT(dat1 == dat2);
		dat2.SetStorage(tmp2, 3);
		TS_ASSERT(dat1 == dat2);
		tmp2[1] = scope0;
		TS_ASSERT(dat1 == dat2);

		// Test ToString
		TS_ASSERT_THROWS_NOTHING(dat1.ToString(2));

		delete scope0;
		delete scope1;
		delete scope2;
		delete scope3;
		delete scope4;
		delete scope5;
	}
};