#pragma once

#include "EventSubscriber.h"
#include "EventPublisher.h"

namespace UnitTests
{
	class FooSubscriber : public Library::EventSubscriber
	{
	public:
		FooSubscriber();
		virtual ~FooSubscriber() = default;

		virtual void Notify(std::shared_ptr<Library::EventPublisher> eventPublisher) override;

		int GetValue() const;

	private:
		int mVal;
	};

}