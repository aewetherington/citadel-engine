#pragma once

#include "XmlParseMaster.h"

namespace UnitTests
{

	class FooSharedData : public Library::XmlParseMaster::SharedData
	{
	public:
		FooSharedData();
		virtual ~FooSharedData() = default;
		virtual SharedData* Clone() const;
		virtual void IncrementDepth() override;
		virtual void DecrementDepth() override;

		std::uint32_t mMaxDepth;
		bool mIsClone;
	};

}
