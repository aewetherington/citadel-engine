#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include "Factory.h"
#include "Scope.h"
#include "Foo.h"

using namespace Library;

ConcreteFactory(Scope, RTTI);
ConcreteFactory(Foo, RTTI);

class FactoryTestSuite : public CxxTest::TestSuite
{
public:
	void TestFactoryName()
	{
		FooFactory fooFact;
		TS_ASSERT_EQUALS(fooFact.GetClassName(), "Foo");
	}

	void TestFactoryFind()
	{
		TS_ASSERT_EQUALS(Factory<RTTI>::Find("Foo"), nullptr);
		{
			FooFactory fooFact;
			TS_ASSERT_DIFFERS(Factory<RTTI>::Find("Foo"), nullptr);
		}
		TS_ASSERT_EQUALS(Factory<RTTI>::Find("Foo"), nullptr);
	}

	void TestFactoryCreate()
	{
		FooFactory fooFact;
		RTTI* rt = Factory<RTTI>::Create("Bar");
		TS_ASSERT_EQUALS(rt, nullptr);
		rt = Factory<RTTI>::Create("Foo");
		TS_ASSERT_DIFFERS(rt, nullptr);
		Foo* foo = rt->As<Foo>();
		TS_ASSERT_EQUALS(foo->getValue(), 0.0f);
		delete foo;
	}

	void TestFactoryIterate()
	{
		ScopeFactory scopeFact;
		FooFactory fooFact;

		int i = 0;
		bool scopeFound = false;
		bool fooFound = false;
		for (auto it = Factory<RTTI>::begin(); it != Factory<RTTI>::end(); ++it)
		{
			++i;
			if (it->first == "Foo")
			{
				scopeFound = true;
			}
			else if (it->second->GetClassName() == "Scope")
			{
				fooFound = true;
			}
		}

		TS_ASSERT_EQUALS(i, 2);
		TS_ASSERT(scopeFound);
		TS_ASSERT(fooFound);

	}
};