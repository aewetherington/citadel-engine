#include <cxxtest/TestSuite.h>
#include "Vector.h"
#include "Foo.h"

using namespace Library;

class VectorTestSuite : public CxxTest::TestSuite
{
public:
	void setUp()
	{
		mIntVec.Reserve(32);
		TS_ASSERT(mIntVec.IsEmpty());
		mFooVec.Reserve(32);
		TS_ASSERT(mFooVec.IsEmpty());
		mPtrVec.Reserve(32);
		TS_ASSERT(mPtrVec.IsEmpty());
	}

	void tearDown()
	{
		mIntVec.Clear();
		mFooVec.Clear();
		mPtrVec.Clear();
	}

	void TestVectorIterators()
	{
		Vector<int>::Iterator intIt1, intIt2;
		intIt1 = mIntVec.begin();
		TS_ASSERT(intIt1 == intIt1);
		TS_ASSERT(intIt1 == mIntVec.end());
		mIntVec.PushBack(1);
		TS_ASSERT(mIntVec.GetSize() == 1);
		intIt2 = intIt1 = mIntVec.begin();
		TS_ASSERT(intIt1 != mIntVec.end());
		TS_ASSERT(intIt2 != mIntVec.end());
		TS_ASSERT(*intIt1 == 1);
		intIt1++;
		TS_ASSERT(intIt1 == mIntVec.end());
		TS_ASSERT(intIt1 != intIt2);
		++intIt2;
		TS_ASSERT(intIt2 == mIntVec.end());
		TS_ASSERT(intIt1 == intIt2);
		++intIt2;
		TS_ASSERT(intIt2 == mIntVec.end());
		TS_ASSERT(intIt1 == intIt2);
	}

	void TestVectorCopy()
	{
		mIntVec.PushBack(1);
		mIntVec.PushBack(2);
		mIntVec.PushBack(3);
		Vector<int> mIntVec1 = mIntVec, mIntVec2;
		mIntVec2 = mIntVec;
		TS_ASSERT_EQUALS(mIntVec.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntVec1.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntVec2.GetSize(), 3);
		mIntVec.PopBack();
		TS_ASSERT_EQUALS(mIntVec.GetSize(), 2);
		TS_ASSERT_EQUALS(mIntVec1.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntVec2.GetSize(), 3);

		Foo foo;
		foo.setValue(1);
		mFooVec.PushBack(foo);
		foo.setValue(2);
		mFooVec.PushBack(foo);
		foo.setValue(2);
		mFooVec.PushBack(foo);
		Vector<Foo> mFooVec1 = mFooVec, mFooVec2;
		mFooVec2 = mFooVec;
		TS_ASSERT_EQUALS(mFooVec.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooVec1.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooVec2.GetSize(), 3);
		mFooVec.PopBack();
		TS_ASSERT_EQUALS(mFooVec.GetSize(), 2);
		TS_ASSERT_EQUALS(mFooVec1.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooVec2.GetSize(), 3);
		TS_ASSERT_DIFFERS(mFooVec.Front().mValue, mFooVec1.Front().mValue);	// Test deep copy
		TS_ASSERT_DIFFERS(mFooVec.Front().mValue, mFooVec2.Front().mValue);

		int tmpPtrs[] = { 1, 2, 3 };
		mPtrVec.PushBack(&tmpPtrs[0]);
		mPtrVec.PushBack(&tmpPtrs[1]);
		mPtrVec.PushBack(&tmpPtrs[2]);
		Vector<int*> mPtrVec1 = mPtrVec, mPtrVec2;
		mPtrVec2 = mPtrVec;
		TS_ASSERT_EQUALS(mPtrVec.GetSize(), 3);
		TS_ASSERT_EQUALS(mPtrVec1.GetSize(), 3);
		TS_ASSERT_EQUALS(mPtrVec2.GetSize(), 3);
		mPtrVec.PopBack();
		TS_ASSERT_EQUALS(mPtrVec.GetSize(), 2);
		TS_ASSERT_EQUALS(mPtrVec1.GetSize(), 3);
		TS_ASSERT_EQUALS(mPtrVec2.GetSize(), 3);
	}

	void TestVectorResize()
	{
		Vector<int> mIntVec1(3);
		mIntVec1.PushBack(1);
		mIntVec1.PushBack(2);
		mIntVec1.PushBack(3);
		TS_ASSERT_THROWS(mIntVec1.At(3), std::runtime_error);
		mIntVec1.PushBack(4);
		TS_ASSERT_THROWS_NOTHING(mIntVec1.At(4));
		TS_ASSERT_EQUALS(mIntVec1.GetSize(), 5);
		mIntVec1.Reserve(10);
		TS_ASSERT_EQUALS(mIntVec1.GetSize(), 5);
		TS_ASSERT_EQUALS(mIntVec1[2], 3);

		Vector<Foo> mFooVec1(3);
		Foo foo;
		foo.setValue(1);
		mFooVec1.PushBack(1);
		foo.setValue(2);
		mFooVec1.PushBack(2);
		foo.setValue(3);
		mFooVec1.PushBack(3);
		TS_ASSERT_THROWS(mFooVec1.At(3), std::runtime_error);
		mFooVec1.PushBack(4);
		TS_ASSERT_THROWS_NOTHING(mFooVec1.At(4));
		TS_ASSERT_EQUALS(mFooVec1.GetSize(), 5);
		mFooVec1.Reserve(10);
		TS_ASSERT_EQUALS(mFooVec1.GetSize(), 5);
		TS_ASSERT_EQUALS(mFooVec1[2].getValue(), 3);

		Vector<int*> mPtrVec1(3);
		int tmpPtrs[] = { 1, 2, 3, 4 };
		mPtrVec1.PushBack(&tmpPtrs[0]);
		mPtrVec1.PushBack(&tmpPtrs[1]);
		mPtrVec1.PushBack(&tmpPtrs[2]);
		TS_ASSERT_THROWS(mPtrVec1.At(3), std::runtime_error);
		mPtrVec1.PushBack(&tmpPtrs[3]);
		TS_ASSERT_THROWS_NOTHING(mPtrVec1.At(4));
		TS_ASSERT_EQUALS(mPtrVec1.GetSize(), 5);
		mPtrVec1.Reserve(10);
		TS_ASSERT_EQUALS(mPtrVec1.GetSize(), 5);
		TS_ASSERT_EQUALS(*mPtrVec1[2], 3);
	}

	void TestVectorAccess()
	{
		const Vector<int>& tmpIntVec = mIntVec;
		TS_ASSERT_THROWS(mIntVec.Front(), std::runtime_error);
		TS_ASSERT_THROWS(mIntVec.Back(), std::runtime_error);
		TS_ASSERT_THROWS(tmpIntVec.Front(), std::runtime_error);
		TS_ASSERT_THROWS(tmpIntVec.Back(), std::runtime_error);
		mIntVec.PushBack(1);
		mIntVec.PushBack(2);
		mIntVec.PushBack(3);
		TS_ASSERT_EQUALS(mIntVec.Front(), 1);
		TS_ASSERT_EQUALS(mIntVec[1], 2);
		TS_ASSERT_EQUALS(mIntVec.Back(), 3);
		TS_ASSERT_THROWS(mIntVec[3], std::runtime_error);
		TS_ASSERT_EQUALS(tmpIntVec.Front(), 1);
		TS_ASSERT_EQUALS(tmpIntVec[1], 2);
		TS_ASSERT_EQUALS(tmpIntVec.Back(), 3);
		TS_ASSERT_THROWS(tmpIntVec[3], std::runtime_error);

		const Vector<Foo>& tmpFooVec = mFooVec;
		TS_ASSERT_THROWS(mFooVec.Front(), std::runtime_error);
		TS_ASSERT_THROWS(mFooVec.Back(), std::runtime_error);
		TS_ASSERT_THROWS(tmpFooVec.Front(), std::runtime_error);
		TS_ASSERT_THROWS(tmpFooVec.Back(), std::runtime_error);
		Foo foo;
		foo.setValue(1);
		mFooVec.PushBack(foo);
		foo.setValue(2);
		mFooVec.PushBack(foo);
		foo.setValue(3);
		mFooVec.PushBack(foo);
		TS_ASSERT_EQUALS(mFooVec.Front().getValue(), 1);
		TS_ASSERT_EQUALS(mFooVec[1].getValue(), 2);
		TS_ASSERT_EQUALS(mFooVec.Back().getValue(), 3);
		TS_ASSERT_THROWS(mFooVec[3], std::runtime_error);
		TS_ASSERT_EQUALS(tmpFooVec.Front().getValue(), 1);
		TS_ASSERT_EQUALS(tmpFooVec[1].getValue(), 2);
		TS_ASSERT_EQUALS(tmpFooVec.Back().getValue(), 3);
		TS_ASSERT_THROWS(tmpFooVec[3], std::runtime_error);

		const Vector<int*>& tmpPtrVec = mPtrVec;
		TS_ASSERT_THROWS(mPtrVec.Front(), std::runtime_error);
		TS_ASSERT_THROWS(mPtrVec.Back(), std::runtime_error);
		TS_ASSERT_THROWS(tmpPtrVec.Front(), std::runtime_error);
		TS_ASSERT_THROWS(tmpPtrVec.Back(), std::runtime_error);
		int tmpPtrs[] = { 1, 2, 3 };
		mPtrVec.PushBack(&tmpPtrs[0]);
		mPtrVec.PushBack(&tmpPtrs[1]);
		mPtrVec.PushBack(&tmpPtrs[2]);
		TS_ASSERT_EQUALS(*mPtrVec.Front(), 1);
		TS_ASSERT_EQUALS(*mPtrVec[1], 2);
		TS_ASSERT_EQUALS(*mPtrVec.Back(), 3);
		TS_ASSERT_THROWS(*mPtrVec[3], std::runtime_error);
		TS_ASSERT_EQUALS(*tmpPtrVec.Front(), 1);
		TS_ASSERT_EQUALS(*tmpPtrVec[1], 2);
		TS_ASSERT_EQUALS(*tmpPtrVec.Back(), 3);
		TS_ASSERT_THROWS(*tmpPtrVec[3], std::runtime_error);
	}

	void TestVectorFind()
	{
		mIntVec.PushBack(1);
		mIntVec.PushBack(2);
		mIntVec.PushBack(3);
		mIntVec.PushBack(4);
		Vector<int>::Iterator itInt = mIntVec.Find(2);
		TS_ASSERT_EQUALS(*itInt, 2);
		itInt = mIntVec.Find(5);
		TS_ASSERT(itInt == mIntVec.end());

		Foo foo;
		foo.setValue(1);
		mFooVec.PushBack(1);
		foo.setValue(2);
		mFooVec.PushBack(2);
		foo.setValue(3);
		mFooVec.PushBack(3);
		foo.setValue(4);
		mFooVec.PushBack(4);
		foo.setValue(2);
		Vector<Foo>::Iterator itFoo = mFooVec.Find(2);
		TS_ASSERT_EQUALS((*itFoo).getValue(), 2);
		foo.setValue(5);
		itFoo = mFooVec.Find(5);
		TS_ASSERT(itFoo == mFooVec.end());

		int tmpPtrs[] = { 1, 2, 3, 4, 5 };
		mPtrVec.PushBack(&tmpPtrs[0]);
		mPtrVec.PushBack(&tmpPtrs[1]);
		mPtrVec.PushBack(&tmpPtrs[2]);
		mPtrVec.PushBack(&tmpPtrs[3]);
		Vector<int*>::Iterator itPtr = mPtrVec.Find(&tmpPtrs[1]);
		TS_ASSERT_EQUALS(**itPtr, 2);
		itPtr = mPtrVec.Find(&tmpPtrs[4]);
		TS_ASSERT(itPtr == mPtrVec.end());
	}

private:
	Vector<int> mIntVec;
	Vector<Foo> mFooVec;
	Vector<int*> mPtrVec;
};