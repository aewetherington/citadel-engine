#include "FooMessage.h"

namespace UnitTests
{
	RTTI_DEFINITIONS(FooMessage);

	FooMessage::FooMessage()
		: mVal(0)
	{

	}

	FooMessage::FooMessage(const FooMessage& rhs)
		: mVal(rhs.mVal)
	{

	}

	FooMessage& FooMessage::operator=(const FooMessage& rhs)
	{
		mVal = rhs.mVal;

		return *this;
	}

	void FooMessage::SetValue(int val)
	{
		mVal = val;
	}

	int FooMessage::GetValue() const
	{
		return mVal;
	}
}
