#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>

#include "Sector.h"
#include "Entity.h"

using namespace Library;

class SectorTestSuite : public CxxTest::TestSuite
{
public:
	void TestSectorInstantiate()
	{
		Sector mySector("Ireland");

		TS_ASSERT_EQUALS(mySector.GetName(), "Ireland");
		TS_ASSERT_EQUALS(mySector.GetGUID(), "Ireland_" + std::to_string(UINT32_MAX));
		TS_ASSERT_EQUALS(mySector.GetWorld(), nullptr);
		TS_ASSERT_EQUALS(mySector.GetEntities()->GetSize(), 0);
	}

	void TestSectorCreateEntity()
	{
		Sector mySector("Ireland");
		EntityFactory entityFactory;

		Entity* newEntity1 = mySector.CreateEntity("Entity", "Alex");

		TS_ASSERT_DIFFERS(newEntity1, nullptr);
		TS_ASSERT_EQUALS(newEntity1->GetName(), "Alex");
		TS_ASSERT_EQUALS(newEntity1->GetGUID(), "Ireland_" + std::to_string(UINT32_MAX) + "_Alex_0");
		TS_ASSERT_EQUALS(newEntity1->GetSector(), &mySector);
		TS_ASSERT_EQUALS(mySector.GetEntities()->GetSize(), 1); // One with a unique name.
		TS_ASSERT_EQUALS(mySector["entities"].GetTable()->Find("Alex")->GetTable(0), newEntity1);

		Entity* newEntity2 = mySector.CreateEntity("Entity", "Alicia");
		Entity* newEntity3 = mySector.CreateEntity("Entity", "Alex");

		TS_ASSERT_EQUALS(newEntity2->GetGUID(), "Ireland_" + std::to_string(UINT32_MAX) + "_Alicia_0");
		TS_ASSERT_EQUALS(newEntity3->GetGUID(), "Ireland_" + std::to_string(UINT32_MAX) + "_Alex_1");
		TS_ASSERT_EQUALS(mySector.GetEntities()->GetSize(), 2); // Two with unique names.
		TS_ASSERT_EQUALS(mySector["entities"].GetTable()->Find("Alicia")->GetTable(0), newEntity2);
		TS_ASSERT_EQUALS(mySector["entities"].GetTable()->Find("Alex")->GetTable(1), newEntity3);
	}
};