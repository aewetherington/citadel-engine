#pragma once

#include "IXmlParseHelper.h"
#include "XmlParseMaster.h"
#include "HashMap.h"

namespace UnitTests
{

	class FooParseHelper : public Library::IXmlParseHelper
	{
	public:
		FooParseHelper();
		virtual ~FooParseHelper() = default;

		virtual void Initialize() override;
		virtual bool StartElementHandler(Library::XmlParseMaster::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes) override;
		virtual bool EndElementHandler(Library::XmlParseMaster::SharedData* sharedData, std::string name) override;
		virtual bool CharElementHandler(Library::XmlParseMaster::SharedData* sharedData, const char* data, int length) override;
		virtual IXmlParseHelper* Clone() const override;

		std::uint32_t mMaxDepth;
		std::uint32_t mNumStartHandles;
		std::uint32_t mNumEndHandles;
		std::uint32_t mNumCharHandles;
		bool mIsClone;
	};

}
