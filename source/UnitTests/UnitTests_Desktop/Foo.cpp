#include "Foo.h"

RTTI_DEFINITIONS(Foo)

std::uint32_t FooHash::operator()(const Foo& key) const
{
	return static_cast<std::uint32_t>(93u * key.getValue());
}

Foo::Foo(const float val)
	: mValue(nullptr)
{
	mValue = new float();
	setValue(val);
}

Foo::Foo(const Foo& rhs)
	: Foo()
{
	operator=(rhs);
}

Foo::~Foo()
{
	delete mValue;
}

void Foo::setValue(float val) {
	*mValue = val;
}

float Foo::getValue() const {
	return *mValue;
}

Foo& Foo::operator=(const Foo& rhs)
{
	if (mValue != nullptr)
	{
		*mValue = rhs.getValue();
	}

	return *this;
}

bool Foo::operator==(const Foo& rhs) const
{
	return *(mValue) == *(rhs.mValue);
}

bool Foo::operator!=(const Foo& rhs) const
{
	return !operator==(rhs);
}