#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include "XmlParseMaster.h"
#include "XmlParseHelperTable.h"
#include "XmlParseHelperEntity.h"
#include "XmlParseHelperSector.h"
#include "XmlParseHelperWorld.h"
#include "XmlParseHelperPrimitive.h"
#include "SharedDataTable.h"
#include "World.h"
#include "Sector.h"
#include "Entity.h"

using namespace Library;

class XmlParseHelperTableTestSuite : public CxxTest::TestSuite
{
public:
	void TestXmlParseHelperTableValid()
	{
		const char* xml =	"<table name='Player'>"
								"<str name='PlayerName'>ErichTheEpich</str>"
								"<table name='Resources'>"
									"<value>"
										"<int name='Health' size='3'>"
											"<value>100</value>"
											"<value>10</value>"
											"<value>1</value>"
										"</int>"
										"<int name='Mana'>"
											"<value>200</value>"
											"<value>20</value>"
											"<value>2</value>"
										"</int>"
									"</value>"
									"<value>"
										"<int name='Attack'>25</int>"
										"<float name='Defense'>12.3</float>"
									"</value>"
									"<value>"
										"<table name='Inventory'>"
											"<int name='Potion'>3</int>"
											"<vec name='VectorOfPower'>(1, 2, 3, 4)</vec>"
											"<vec name='VectorOfWisdom'>(5, 6, 7)</vec>"
											"<mat name='MatrixOfMagic'>[2 0 0 0, 0 2 0 0, 0 0 2 0, 0 0 0 2]</mat>"
										"</table>"
									"</value>"
								"</table>"
							"</table>";

		Scope root;
		SharedDataTable sharedData(root);
		XmlParseHelperTable parseHelper;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseHelper);
		parseMaster.AddHelper(parsePrimitive);

		parseMaster.Parse(xml, strlen(xml), true);

		TS_ASSERT_DIFFERS(root.Find("Resources"), nullptr);
		TS_ASSERT_DIFFERS(root.Find("PlayerName"), nullptr);

		TS_ASSERT_EQUALS(root["PlayerName"].Get<std::string>(), "ErichTheEpich");
		TS_ASSERT_EQUALS(root["Resources"].GetSize(), 3);

		TS_ASSERT_DIFFERS(root["Resources"].GetTable(0)->Find("Health"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(0)->Find("Health")->Get<int>(0), 100);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(0)->Find("Health")->Get<int>(1), 10);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(0)->Find("Health")->Get<int>(2), 1);
		TS_ASSERT_DIFFERS(root["Resources"].GetTable(0)->Find("Mana"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(0)->Find("Mana")->Get<int>(0), 200);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(0)->Find("Mana")->Get<int>(1), 20);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(0)->Find("Mana")->Get<int>(2), 2);

		TS_ASSERT_DIFFERS(root["Resources"].GetTable(1)->Find("Attack"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(1)->Find("Attack")->Get<int>(), 25);
		TS_ASSERT_DIFFERS(root["Resources"].GetTable(1)->Find("Defense"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(1)->Find("Defense")->Get<float>(), 12.3f);

		TS_ASSERT_DIFFERS(root["Resources"].GetTable(2)->Find("Inventory"), nullptr);
		TS_ASSERT_DIFFERS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("Potion"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("Potion")->Get<int>(), 3);
		TS_ASSERT_DIFFERS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("VectorOfPower"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("VectorOfPower")->Get<glm::vec4>(), glm::vec4(1, 2, 3, 4));
		TS_ASSERT_DIFFERS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("VectorOfWisdom"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("VectorOfWisdom")->Get<glm::vec4>(), glm::vec4(5, 6, 7, 1));
		TS_ASSERT_DIFFERS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("MatrixOfMagic"), nullptr);
		TS_ASSERT_EQUALS(root["Resources"].GetTable(2)->Find("Inventory")->GetTable()->Find("MatrixOfMagic")->Get<glm::mat4>(), glm::mat4(2));


		SharedDataTable* sharedData2 = reinterpret_cast<SharedDataTable*>(sharedData.Clone());
		XmlParseHelperTable* parseHelper2 = reinterpret_cast<XmlParseHelperTable*>(parseHelper.Clone());

		TS_ASSERT_DIFFERS(&parseHelper, parseHelper2);
		TS_ASSERT_DIFFERS(&sharedData, sharedData2);
		TS_ASSERT_DIFFERS(&root, sharedData2->GetRoot());

		delete sharedData2;
		delete parseHelper2;
	}

	void TestXmlParseHelperTableInvalid()
	{
		Scope root;
		SharedDataTable sharedData(root);
		XmlParseHelperTable parseHelper;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseHelper);
		parseMaster.AddHelper(parsePrimitive);
		
		const char* xml0 =  "<table name='root'>"
							"</table>";
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml0, strlen(xml0), true));
		
		const char* xml1 = "<int name='Whoo'>10</int>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml1, strlen(xml1), true));

		const char* xml2 = "<table><int name='Whoo'>10</int></table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml2, strlen(xml2), true));

		const char* xml3 =	"<table name='root'>"
								"<value>"
									"<float name='TotesLegit'>10.0f</float>"
								"</value>"
								"<int name='Whoo'>10</int>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml3, strlen(xml3), true));

		const char* xml4 =  "<table name='root'>"
								"<value>"
									"<float name='TotesLegit'>10.0f</int>"
								"</value>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml4, strlen(xml4), true));

		const char* xml5 = "<table name='root'>"
								"<value>"
									"<value>"
										"<float name='TotesLegit'>10.0f</float>"
									"</value>"
								"</value>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml5, strlen(xml5), true));

		const char* xml6 =  "<table name='root'>"
								"<float name='Totes'>"
									"<float name='Legit'>10.0f</float>"
								"</float>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml6, strlen(xml6), true));

		const char* xml7 =  "<table name='root'>"
								"<float name='TotesLegit' size='3'>10.0</float>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml7, strlen(xml7), true));

		const char* xml8 =  "<table name='root'>"
								"<float name='TotesLegit'></float>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml8, strlen(xml8), true));

		const char* xml9 =  "<table name='root'>"
								"<float name='TotesLegit'>"
									"<table name='child'>"
									"</table>"
								"</float>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml9, strlen(xml9), true));

		const char* xml10 = "<table name='root'>"
								"<totes name='Legit'>BTT</totes>"
							"</table>";
		TS_ASSERT_THROWS_ANYTHING(parseMaster.Parse(xml0, strlen(xml10), true));

		
	}

	void TestXMLParseHelperWorld()
	{
		World root;
		SharedDataTable sharedData(root);
		XmlParseHelperTable parseTable;
		XmlParseHelperEntity parseEntity;
		XmlParseHelperSector parseSector;
		XmlParseHelperWorld parseWorld;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseMaster parseMaster(&sharedData);
		EntityFactory entFac;

		parseMaster.AddHelper(parseTable);
		parseMaster.AddHelper(parseEntity);
		parseMaster.AddHelper(parseSector);
		parseMaster.AddHelper(parseWorld);
		parseMaster.AddHelper(parsePrimitive);

		const char* xml0 =  "<world name='Earth'>"
								"<sector name='Ireland'>"
									"<entity name='Alex' class='Entity'>"
										"<int name='MyInt'>10</int>"
									"</entity>"
									"<entity name='Alicia' class='Entity' size='3'>"
										"<value><int name='MyID'>1</int></value>"
										"<value><int name='MyID'>2</int></value>"
										"<value><int name='MyID'>3</int></value>"
									"</entity>"
								"</sector>"
							"</world>";
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml0, strlen(xml0), true));

		TS_ASSERT_DIFFERS(root.Find("sectors"), nullptr);
		TS_ASSERT_DIFFERS(root.Find("sectors")->GetTable()->Find("Ireland"), nullptr);
		TS_ASSERT_DIFFERS(root.Find("sectors")->GetTable()->Find("Ireland")->GetTable()->Find("entities"), nullptr);
		TS_ASSERT_DIFFERS(root.Find("sectors")->GetTable()->Find("Ireland")->GetTable()->Find("entities")->GetTable()->Find("Alex")->GetTable()->Find("MyInt"), nullptr);
		TS_ASSERT_EQUALS(root.Find("sectors")->GetTable()->Find("Ireland")->GetTable()->Find("entities")->GetTable()->Find("Alex")->GetTable()->Find("MyInt")->Get<int>(), 10);

		TS_ASSERT_DIFFERS(root.Find("sectors")->GetTable()->Find("Ireland")->GetTable()->Find("entities")->GetTable()->Find("Alicia")->GetTable(1)->Find("MyID"), nullptr);
		TS_ASSERT_EQUALS(root.Find("sectors")->GetTable()->Find("Ireland")->GetTable()->Find("entities")->GetTable()->Find("Alicia")->GetTable(1)->Find("MyID")->Get<int>(), 2);

	}
};