#include "FooSharedData.h"

using namespace Library;

namespace UnitTests
{
	FooSharedData::FooSharedData()
		: SharedData(), mMaxDepth(0), mIsClone(false)
	{

	}

	FooSharedData::SharedData* FooSharedData::Clone() const
	{
		auto newData = new FooSharedData();
		newData->mIsClone = true;
		newData->mMaxDepth = 0;
		return newData;
	}

	void FooSharedData::IncrementDepth()
	{
		SharedData::IncrementDepth();
		auto depth = GetDepth();
		mMaxDepth = (depth > mMaxDepth) ? depth : mMaxDepth;
	}

	void FooSharedData::DecrementDepth()
	{
		SharedData::DecrementDepth();
	}

}