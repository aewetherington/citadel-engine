#include <cxxtest/TestSuite.h>
#include <string>
#include "HashMap.h"
#include "Foo.h"

using namespace Library;

class HashMapTestSuite : public CxxTest::TestSuite
{
public:
	void setUp()
	{

	}

	void tearDown()
	{

	}

	void TestHashMapIterator()
	{
		int nums[] = { 222, 444, 666, 888, 111 };

		HashMap<int, float> intHash(3);
		HashMap<int, float>::Iterator it1 = intHash.begin(), it2 = intHash.begin();
		TS_ASSERT(it1 == it2);
		HashMap<int, float> tmpHash(3);
		it2 = tmpHash.begin();
		TS_ASSERT(it1 != it2);
		intHash.Insert(HashMap<int, float>::PairType(nums[0], 0.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[1], 1.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[2], 2.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[3], 3.0f));
		it1 = intHash.begin();
		it2 = intHash.end();
		TS_ASSERT_THROWS_NOTHING(*it1);
		TS_ASSERT_THROWS_NOTHING(it1->first);
		TS_ASSERT_THROWS(*it2, std::runtime_error);
		TS_ASSERT_THROWS(it2->first, std::runtime_error);
	}

	void TestHashMapConstruct()
	{
		int num = 10;
		HashMap<int, float> intHash(3);
		TS_ASSERT_EQUALS(intHash.GetSize(), 0);
		TS_ASSERT(intHash.begin() == intHash.end());
		TS_ASSERT(intHash.Find(num) == intHash.end());

		HashMap<int*, float> ptrHash(3);
		TS_ASSERT_EQUALS(ptrHash.GetSize(), 0);
		TS_ASSERT(ptrHash.begin() == ptrHash.end());
		TS_ASSERT(ptrHash.Find(&num) == ptrHash.end());

		HashMap<std::string, float> strHash(3);
		TS_ASSERT_EQUALS(strHash.GetSize(), 0);
		TS_ASSERT(strHash.begin() == strHash.end());
		TS_ASSERT(strHash.Find("num") == strHash.end());

		Foo foo(3.14f);
		HashMap<Foo, float, FooHash> fooHash(3);
		TS_ASSERT_EQUALS(fooHash.GetSize(), 0);
		TS_ASSERT(fooHash.begin() == fooHash.end());
		TS_ASSERT(fooHash.Find(foo) == fooHash.end());
	}

	void TestHashMapCopy()
	{
		int nums[] = { 222, 444, 666, 888, 111 };
		int i;

		HashMap<int, float> intHash(3);
		intHash.Insert(HashMap<int, float>::PairType(nums[0], 0.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[1], 1.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[2], 2.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[3], 3.0f));
		HashMap<int, float> intHash1 = intHash, intHash2;
		intHash2 = intHash;
		TS_ASSERT_EQUALS(intHash.GetSize(), 4);
		intHash1.Insert(HashMap<int, float>::PairType(nums[4], 4.0f));
		intHash2.Remove(nums[2]);
		TS_ASSERT_EQUALS(intHash.GetSize(), 4);
		TS_ASSERT_EQUALS(intHash1.GetSize(), 5);
		TS_ASSERT_EQUALS(intHash2.GetSize(), 3);
		i = 0;
		for (auto& val : intHash)
		{
			val.second = -1.0f;
			++i;
		}
		TS_ASSERT_EQUALS(i, 4);
		for (auto& val : intHash1)
		{
			TS_ASSERT_DIFFERS(val.second, -1.0f);
		}

		HashMap<int*, float> ptrHash(3);
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[0], 0.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[1], 1.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[2], 2.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[3], 3.0f));
		HashMap<int*, float> ptrHash1 = ptrHash, ptrHash2;
		ptrHash2 = ptrHash;
		TS_ASSERT_EQUALS(ptrHash.GetSize(), 4);
		ptrHash1.Insert(HashMap<int*, float>::PairType(&nums[4], 4.0f));
		ptrHash2.Remove(&nums[2]);
		TS_ASSERT_EQUALS(ptrHash.GetSize(), 4);
		TS_ASSERT_EQUALS(ptrHash1.GetSize(), 5);
		TS_ASSERT_EQUALS(ptrHash2.GetSize(), 3);
		i = 0;
		for (auto& val : ptrHash)
		{
			val.second = -1.0f;
			++i;
		}
		TS_ASSERT_EQUALS(i, 4);
		for (auto& val : ptrHash1)
		{
			TS_ASSERT_DIFFERS(val.second, -1.0f);
		}

		HashMap<std::string, float> strHash(3);
		strHash.Insert(HashMap<std::string, float>::PairType("Two", 0.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Four", 1.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Six", 2.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Eight", 3.0f));
		HashMap<std::string, float> strHash1 = strHash, strHash2;
		strHash2 = strHash;
		TS_ASSERT_EQUALS(strHash.GetSize(), 4);
		strHash1.Insert(HashMap<std::string, float>::PairType("One", 4.0f));
		strHash2.Remove("Six");
		TS_ASSERT_EQUALS(strHash.GetSize(), 4);
		TS_ASSERT_EQUALS(strHash1.GetSize(), 5);
		TS_ASSERT_EQUALS(strHash2.GetSize(), 3);
		i = 0;
		for (auto& val : strHash)
		{
			val.second = -1.0f;
			++i;
		}
		TS_ASSERT_EQUALS(i, 4);
		for (auto& val : strHash1)
		{
			TS_ASSERT_DIFFERS(val.second, -1.0f);
		}

		HashMap<Foo, float, FooHash> fooHash(3);
		Foo foo;
		foo.setValue(static_cast<float>(nums[0]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 0.0f));
		foo.setValue(static_cast<float>(nums[1]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 1.0f));
		foo.setValue(static_cast<float>(nums[2]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 2.0f));
		foo.setValue(static_cast<float>(nums[3]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 3.0f));
		HashMap<Foo, float, FooHash> fooHash1 = fooHash, fooHash2;
		fooHash2 = fooHash;
		TS_ASSERT_EQUALS(fooHash.GetSize(), 4);
		foo.setValue(static_cast<float>(nums[4]));
		fooHash1.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 4.0f));
		foo.setValue(static_cast<float>(nums[2]));
		fooHash2.Remove(foo);
		TS_ASSERT_EQUALS(fooHash.GetSize(), 4);
		TS_ASSERT_EQUALS(fooHash1.GetSize(), 5);
		TS_ASSERT_EQUALS(fooHash2.GetSize(), 3);
		i = 0;
		for (auto& val : fooHash)
		{
			val.second = -1.0f;
			++i;
		}
		TS_ASSERT_EQUALS(i, 4);
		for (auto& val : fooHash1)
		{
			TS_ASSERT_DIFFERS(val.second, -1.0f);
		}
	}

	void TestHashMapInsert()
	{
		int nums[] = { 222, 444, 666, 888, 111};

		HashMap<int, float> intHash(3);
		intHash.Insert(HashMap<int, float>::PairType(nums[0], 0.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[1], 1.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[2], 2.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[3], 3.0f));
		TS_ASSERT_EQUALS(intHash.GetSize(), 4);
		TS_ASSERT(intHash.ContainsKey(nums[1]));
		TS_ASSERT_EQUALS(intHash.Find(nums[2])->second, 2.0f);
		TS_ASSERT(intHash.Find(nums[4]) == intHash.end());

		HashMap<int*, float> ptrHash(3);
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[0], 0.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[1], 1.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[2], 2.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[3], 3.0f));
		TS_ASSERT_EQUALS(ptrHash.GetSize(), 4);
		TS_ASSERT(ptrHash.ContainsKey(&nums[1]));
		TS_ASSERT_EQUALS(ptrHash.Find(&nums[2])->second, 2.0f);
		TS_ASSERT(ptrHash.Find(&nums[4]) == ptrHash.end());

		HashMap<std::string, float> strHash(3);
		strHash.Insert(HashMap<std::string, float>::PairType("Two", 0.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Four", 1.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Six", 2.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Eight", 3.0f));
		TS_ASSERT_EQUALS(strHash.GetSize(), 4);
		TS_ASSERT(strHash.ContainsKey("Four"));
		TS_ASSERT_EQUALS(strHash.Find("Six")->second, 2.0f);
		TS_ASSERT(strHash.Find("One") == strHash.end());

		HashMap<Foo, float, FooHash> fooHash(3);
		Foo foo;
		foo.setValue(static_cast<float>(nums[0]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 0.0f));
		foo.setValue(static_cast<float>(nums[1]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 1.0f));
		foo.setValue(static_cast<float>(nums[2]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 2.0f));
		foo.setValue(static_cast<float>(nums[3]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 3.0f));
		TS_ASSERT_EQUALS(fooHash.GetSize(), 4);
		foo.setValue(static_cast<float>(nums[1]));
		TS_ASSERT(fooHash.ContainsKey(foo));
		foo.setValue(static_cast<float>(nums[2]));
		TS_ASSERT_EQUALS(fooHash.Find(foo)->second, 2.0f);
		foo.setValue(static_cast<float>(nums[4]));
		TS_ASSERT(fooHash.Find(foo) == fooHash.end());
	}
	
	void TestHashMapRemove()
	{
		int nums[] = { 222, 444, 666, 888, 111 };

		HashMap<int, float> intHash(3);
		intHash.Insert(HashMap<int, float>::PairType(nums[0], 0.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[1], 1.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[2], 2.0f));
		intHash.Insert(HashMap<int, float>::PairType(nums[3], 3.0f));
		intHash.Remove(nums[4]);
		TS_ASSERT_EQUALS(intHash.GetSize(), 4);
		intHash.Remove(nums[2]);
		for (auto& val : intHash)
		{
			TS_ASSERT_DIFFERS(val.first, nums[2]);
		}

		HashMap<int*, float> ptrHash(3);
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[0], 0.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[1], 1.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[2], 2.0f));
		ptrHash.Insert(HashMap<int*, float>::PairType(&nums[3], 3.0f));
		ptrHash.Remove(&nums[4]);
		TS_ASSERT_EQUALS(ptrHash.GetSize(), 4);
		ptrHash.Remove(&nums[2]);
		for (auto& val : ptrHash)
		{
			TS_ASSERT_DIFFERS(val.first, &nums[2]);
		}

		HashMap<std::string, float> strHash(3);
		strHash.Insert(HashMap<std::string, float>::PairType("Two", 0.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Four", 1.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Six", 2.0f));
		strHash.Insert(HashMap<std::string, float>::PairType("Eight", 3.0f));
		strHash.Remove("One");
		TS_ASSERT_EQUALS(strHash.GetSize(), 4);
		strHash.Remove("Six");
		for (auto& val : strHash)
		{
			TS_ASSERT_DIFFERS(val.first, "Six");
		}

		HashMap<Foo, float, FooHash> fooHash(3);
		Foo foo;
		foo.setValue(static_cast<float>(nums[0]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 0.0f));
		foo.setValue(static_cast<float>(nums[1]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 1.0f));
		foo.setValue(static_cast<float>(nums[2]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 2.0f));
		foo.setValue(static_cast<float>(nums[3]));
		fooHash.Insert(HashMap<Foo, float, FooHash>::PairType(foo, 3.0f));
		foo.setValue(static_cast<float>(nums[4]));
		fooHash.Remove(foo);
		TS_ASSERT_EQUALS(fooHash.GetSize(), 4);
		foo.setValue(static_cast<float>(nums[2]));
		fooHash.Remove(foo);
		for (auto& val : fooHash)
		{
			TS_ASSERT_DIFFERS(val.first, static_cast<float>(nums[2]));
		}
	}

	void TestHashMapPrelim()
	{
		HashMap<int, float> hash(3);
		HashMap<int, float>::Iterator it;

		// Prelim insertion tests.
		TS_ASSERT_EQUALS(hash.GetSize(), 0);
		TS_ASSERT(hash.begin() == hash.end());
		TS_ASSERT(hash.Find(10) == hash.end());
		hash.Insert(HashMap<int, float>::PairType(10, 3.14f));
		TS_ASSERT_EQUALS(hash.GetSize(), 1);
		TS_ASSERT_EQUALS(hash.begin()->first, 10);
		TS_ASSERT_EQUALS(hash.begin()->second, 3.14f);
		it = hash.Find(10);
		TS_ASSERT_EQUALS(it->second, 3.14f);
		it = hash.Insert(HashMap<int, float>::PairType(10, 1.00f));
		TS_ASSERT_EQUALS(it->second, 3.14f);
		it = hash.Insert(HashMap<int, float>::PairType(9001, 11.11f));
		TS_ASSERT_EQUALS(hash.GetSize(), 2);
		TS_ASSERT_EQUALS(it->first, 9001);
		TS_ASSERT_EQUALS(it->second, 11.11f);
		hash.Insert(HashMap<int, float>::PairType(53, 63.6f));
		hash.Insert(HashMap<int, float>::PairType(22, 47.77f));

		TS_ASSERT_EQUALS(hash.GetSize(), 4);
		hash[5] = 1.23f;
		TS_ASSERT_EQUALS(hash.GetSize(), 5);

		TS_ASSERT(hash.ContainsKey(5));

		// Prelim removal tests.
		hash.Remove(53);
		TS_ASSERT_EQUALS(hash.GetSize(), 4);

		hash.Remove(10);
		TS_ASSERT_EQUALS(hash.GetSize(), 3);

		// Remove nonexistant
		hash.Remove(10);
		TS_ASSERT_EQUALS(hash.GetSize(), 3);

		hash.Remove(9001);
		TS_ASSERT_EQUALS(hash.GetSize(), 2);

		hash.Remove(22);
		TS_ASSERT_EQUALS(hash.GetSize(), 1);

		hash.Remove(5);
		TS_ASSERT_EQUALS(hash.GetSize(), 0);

		// Remove nonexistant with empty hashmap
		hash.Remove(5);
		TS_ASSERT_EQUALS(hash.GetSize(), 0);

		// Don't run this loop, it's an empty HashMap.
		for (auto& val : hash)
		{
			TS_FAIL("Failed to iterate properly.");
			val.second = 0.0f;
		}

		// Ensure it cleans OK
		hash.Clear();
		TS_ASSERT_EQUALS(hash.GetSize(), 0);
	}
};