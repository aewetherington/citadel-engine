#include "AttributedBar.h"

namespace UnitTests
{
	RTTI_DEFINITIONS(AttributedBar);

	AttributedBar::AttributedBar()
	{
		int val[] = { 10, 20 };
		mVal = 100;

		INIT_SIGNATURE();

		INTERNAL_ATTRIBUTE("Num", Library::Datum::DatumType::Integer, &val, 2);
		EXTERNAL_ATTRIBUTE("Bar", Library::Datum::DatumType::Integer, &mVal, 1);

		Populate();
	}
}