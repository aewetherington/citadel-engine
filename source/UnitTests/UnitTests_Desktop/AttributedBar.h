#pragma once

#include "Attributed.h"

namespace UnitTests
{

	class AttributedBar : public Library::Attributed
	{
		RTTI_DECLARATIONS(AttributedBar, Attributed);

	public:
		AttributedBar();
		virtual ~AttributedBar() = default;

		int mVal;
	};

}