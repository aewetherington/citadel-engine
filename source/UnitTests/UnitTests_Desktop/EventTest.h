#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>

#include "FooSubscriber.h"
#include "FooMessage.h"
#include "Event.h"
#include "EventQueue.h"

using namespace Library;
using namespace std;
using namespace std::chrono;
using namespace UnitTests;

class EventTestSuite : public CxxTest::TestSuite
{
public:
	void tearDown()
	{
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::UnsubscribeAll());
	}

	void TestEventImmediate()
	{
		EventQueue queue;
		FooSubscriber fooSub;
		FooMessage foo;

		// Set the initial value.
		TS_ASSERT_THROWS_NOTHING(foo.SetValue(10));

		auto fooEvent = make_shared<Event<FooMessage>>(foo);

		// Send the event.
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub));
		TS_ASSERT_THROWS_NOTHING(queue.Send(fooEvent));

		// Ensure values are correct.
		TS_ASSERT_EQUALS(fooSub.GetValue(), 10);
	}

	void TestEventEnqueueNoDelay()
	{
		EventQueue queue;
		FooSubscriber fooSub;
		FooMessage foo;

		// Set the initial value.
		TS_ASSERT_THROWS_NOTHING(foo.SetValue(10));

		auto fooEvent = make_shared<Event<FooMessage>>(foo);

		// Enqueue the event.
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub));
		TS_ASSERT_THROWS_NOTHING(queue.Enqueue(fooEvent, high_resolution_clock::now()));

		// This should send without needing an update. Ensure values are correct.
		TS_ASSERT_EQUALS(fooSub.GetValue(), 10);
	}

	void TestEventEnqueueDelay()
	{
		EventQueue queue;
		FooSubscriber fooSub;
		FooMessage foo;

		// Set the initial value.
		TS_ASSERT_THROWS_NOTHING(foo.SetValue(10));

		auto fooEvent = make_shared<Event<FooMessage>>(foo);

		// Enqueue the event with a 2-second delay.
		high_resolution_clock::time_point time = high_resolution_clock::now();
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub));
		TS_ASSERT_THROWS_NOTHING(queue.Enqueue(fooEvent, time, milliseconds(2000)));

		TS_ASSERT_EQUALS(fooEvent->GetTimeEnqueued(), time);
		TS_ASSERT_EQUALS(fooEvent->GetDelay(), milliseconds(2000));

		// +1 second, shouldn't have delivered.
		time += milliseconds(1000);
		TS_ASSERT_THROWS_NOTHING(queue.Update(time));
		TS_ASSERT_EQUALS(fooSub.GetValue(), 0);

		// +1 second, shouldn't have delivered. ( must be > the delay)
		time += milliseconds(1000);
		TS_ASSERT_THROWS_NOTHING(queue.Update(time));
		TS_ASSERT_EQUALS(fooSub.GetValue(), 0);

		TS_ASSERT(!queue.IsEmpty());
		TS_ASSERT_EQUALS(queue.GetSize(), 1);

		// +1 second (3 seconds total). Should be delivered now.
		time += milliseconds(1000);
		TS_ASSERT_THROWS_NOTHING(queue.Update(time));
		TS_ASSERT_EQUALS(fooSub.GetValue(), 10);

		TS_ASSERT(queue.IsEmpty());
		TS_ASSERT_EQUALS(queue.GetSize(), 0);
	}

	void TestEventRemoveSubscriber()
	{
		EventQueue queue;
		FooSubscriber fooSub;
		FooMessage foo;

		auto fooEvent = make_shared<Event<FooMessage>>(foo);

		// Set the initial value.
		TS_ASSERT_THROWS_NOTHING(foo.SetValue(10));

		// Send the event.
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Subscribe(fooSub));
		TS_ASSERT_THROWS_NOTHING(Event<FooMessage>::Unsubscribe(fooSub));
		TS_ASSERT_THROWS_NOTHING(queue.Send(fooEvent));

		// Ensure values are correct. (It didn't get sent to the subscriber.)
		TS_ASSERT_EQUALS(fooSub.GetValue(), 0);
	}

};