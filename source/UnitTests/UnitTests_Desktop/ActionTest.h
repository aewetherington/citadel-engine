#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>

#include "Action.h"
#include "ActionList.h"
#include "ActionCreateAction.h"
#include "ActionDestroyAction.h"

#include "Entity.h"
#include "World.h"
#include "Sector.h"

#include "XmlParseHelperAction.h"
#include "XmlParseHelperPrimitive.h"
#include "XmlParseHelperSector.h"
#include "XmlParseHelperWorld.h"
#include "XmlParseMaster.h"

using namespace Library;

class ActionTestSuite : public CxxTest::TestSuite
{
public:
	void TestActionCreateAction()
	{
		ActionListFactory alf;
		ActionCreateAction* aca = new ActionCreateAction("FirstAction");
		Entity ent;
		WorldState ws;

		// Give info about what to create on update
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeClass("ActionList"));
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeName("SecondAction"));
		
		// Attach action to entity
		TS_ASSERT_THROWS_NOTHING(aca->SetOwner(&ent));

		// Check that only it is attached.
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("FirstAction"), nullptr);
		TS_ASSERT_EQUALS(ent.GetActions()->Find("SecondAction"), nullptr);
		
		// Update!
		TS_ASSERT_THROWS_NOTHING(ent.Update(ws));

		// Check that the new action was made and attached
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("FirstAction"), nullptr);
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("SecondAction"), nullptr);
	}

	void TestActionCreateActionXML()
	{
		ActionListFactory alf;
		ActionCreateActionFactory acaf;
		WorldState ws;
		World world;

		SharedDataTable sharedData(world);
		XmlParseHelperSector parseSector;
		XmlParseHelperWorld parseWorld;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseHelperAction parseAction;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseSector);
		parseMaster.AddHelper(parseWorld);
		parseMaster.AddHelper(parsePrimitive);
		parseMaster.AddHelper(parseAction);

		const char* xml =	"<world name='MyWorld'>"
								"<sector name='MySector'>"
									"<action class='ActionCreateAction' name='FirstAction'>"
										"<str name='prototypeClass'>ActionList</str>"
										"<str name='prototypeName'>SecondAction</str>"
									"</action>"
								"</sector>"
							"</world>";

		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml, strlen(xml), true));

		// Check that only it is attached.
		TS_ASSERT_DIFFERS(world["sectors"].GetTable()->Find("MySector"), nullptr);
		TS_ASSERT_DIFFERS(world["sectors"].GetTable()->Find("MySector")->GetTable()->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(world["sectors"].GetTable()->Find("MySector")->GetTable()->Find("actions")->GetTable()->Find("FirstAction"), nullptr);
		TS_ASSERT_EQUALS(world["sectors"].GetTable()->Find("MySector")->GetTable()->Find("actions")->GetTable()->Find("SecondAction"), nullptr);

		TS_ASSERT_THROWS_NOTHING(world.Update(ws));

		TS_ASSERT_DIFFERS(world["sectors"].GetTable()->Find("MySector")->GetTable()->Find("actions")->GetTable()->Find("FirstAction"), nullptr);
		TS_ASSERT_DIFFERS(world["sectors"].GetTable()->Find("MySector")->GetTable()->Find("actions")->GetTable()->Find("SecondAction"), nullptr);
	}

	void TestActionDeleteAction()
	{
		ActionListFactory alf;
		ActionCreateAction* aca = new ActionCreateAction("FirstAction");
		ActionDestroyAction* ada = new ActionDestroyAction("DestroyAction");
		Entity ent;
		WorldState ws;

		// Give info about what to create on update
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeClass("ActionList"));
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeName("SecondAction"));

		// Attach action to entity
		TS_ASSERT_THROWS_NOTHING(aca->SetOwner(&ent));

		// Update!
		TS_ASSERT_THROWS_NOTHING(ent.Update(ws));

		// Give info about what to destroy.
		TS_ASSERT_THROWS_NOTHING(ada->SetDeleteAction("SecondAction"));

		// Attach destroy action to entity.
		TS_ASSERT_THROWS_NOTHING(ada->SetOwner(&ent));

		// Check that the actions are as they should be, first.
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("FirstAction"), nullptr);
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("SecondAction"), nullptr);
		TS_ASSERT_EQUALS(ent.GetActions()->Find("SecondAction")->GetSize(), 1);
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("DestroyAction"), nullptr);

		// Update!
		TS_ASSERT_THROWS_NOTHING(ent.Update(ws));
		
		// Check that the items are still there, but the SecondAction entry is now empty.
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("FirstAction"), nullptr);
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("SecondAction"), nullptr);
		TS_ASSERT_EQUALS(ent.GetActions()->Find("SecondAction")->GetSize(), 0);
		TS_ASSERT_DIFFERS(ent.GetActions()->Find("DestroyAction"), nullptr);

	}

	void TestActionDeleteActionXML()
	{
		ActionListFactory alf;
		ActionCreateActionFactory acaf;
		ActionDestroyActionFactory adaf;
		WorldState ws;
		World world;

		SharedDataTable sharedData(world);
		XmlParseHelperSector parseSector;
		XmlParseHelperWorld parseWorld;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseHelperAction parseAction;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseSector);
		parseMaster.AddHelper(parseWorld);
		parseMaster.AddHelper(parsePrimitive);
		parseMaster.AddHelper(parseAction);

		const char* xml =	"<world name='MyWorld'>"
								"<action class='ActionList' name='MyAction'>"
								"</action>"
								"<action class='ActionDestroyAction' name='DestroyAction'>"
									"<str name='destroyActionName'>MyAction</str>"
								"</action>"
							"</world>";

		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml, strlen(xml), true));

		TS_ASSERT_DIFFERS(world["actions"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("MyAction"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("MyAction")->GetSize(), 1);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("DestroyAction"), nullptr);

		TS_ASSERT_THROWS_NOTHING(world.Update(ws));

		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("MyAction"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("MyAction")->GetSize(), 0);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("DestroyAction"), nullptr);
	}

	void TestActionList()
	{
		ActionListFactory alf;
		ActionList* al = new ActionList("List");
		ActionList* ald = new ActionList("DestroyedList");
		ActionCreateAction* aca = new ActionCreateAction("CreateAction");
		ActionDestroyAction* ada = new ActionDestroyAction("DestroyAction");
		World world;
		WorldState ws;

		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeClass("ActionList"));
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeName("CreatedList"));
		TS_ASSERT_THROWS_NOTHING(ada->SetDeleteAction("DestroyedList"));

		TS_ASSERT_THROWS_NOTHING(al->SetOwner(&world));
		TS_ASSERT_THROWS_NOTHING(ald->SetOwner(al));
		TS_ASSERT_THROWS_NOTHING(aca->SetOwner(al));
		TS_ASSERT_THROWS_NOTHING(ada->SetOwner(al));

		// Check pre-update state.
		TS_ASSERT_DIFFERS(world["actions"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyAction"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreatedList"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList")->GetSize(), 1);

		// Update!
		TS_ASSERT_THROWS_NOTHING(world.Update(ws));

		// Check post-update state.
		TS_ASSERT_DIFFERS(world["actions"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyAction"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreatedList"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList")->GetSize(), 0);

	}

	void TestActionListXML()
	{
		ActionListFactory alf;
		ActionCreateActionFactory acaf;
		ActionDestroyActionFactory adaf;
		WorldState ws;
		World world;

		SharedDataTable sharedData(world);
		XmlParseHelperSector parseSector;
		XmlParseHelperWorld parseWorld;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseHelperAction parseAction;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseSector);
		parseMaster.AddHelper(parseWorld);
		parseMaster.AddHelper(parsePrimitive);
		parseMaster.AddHelper(parseAction);

		const char* xml =	"<world name='MyWorld'>"
								"<action name='List' class='ActionList'>"
									"<action name='DestroyedList' class='ActionList'></action>"
									"<action name='CreateAction' class='ActionCreateAction'>"
										"<str name='prototypeName'>CreatedList</str>"
										"<str name='prototypeClass'>ActionList</str>"
									"</action>"
									"<action name='DestroyAction' class='ActionDestroyAction'>"
										"<str name='destroyActionName'>DestroyedList</str>"
									"</action>"
								"</action>"
							"</world>";

		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml, strlen(xml), true));

		// Check pre-update state.
		TS_ASSERT_DIFFERS(world["actions"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyAction"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreatedList"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList")->GetSize(), 1);

		// Update!
		TS_ASSERT_THROWS_NOTHING(world.Update(ws));

		// Check post-update state.
		TS_ASSERT_DIFFERS(world["actions"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyAction"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("CreatedList"), nullptr);
		TS_ASSERT_DIFFERS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList"), nullptr);
		TS_ASSERT_EQUALS(world["actions"].GetTable()->Find("List")->GetTable()->Find("actions")->GetTable()->Find("DestroyedList")->GetSize(), 0);

	}
};