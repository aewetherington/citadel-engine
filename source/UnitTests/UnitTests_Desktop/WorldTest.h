#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>

#include "World.h"
#include "Sector.h"
#include "WorldState.h"

using namespace Library;

class WorldTestSuite : public CxxTest::TestSuite
{
public:
	void TestWorldInstantiate()
	{
		WorldState myWorldState;
		World myWorld("Earth");

		TS_ASSERT_THROWS_NOTHING(myWorld.Update(myWorldState));

		TS_ASSERT_EQUALS(myWorld.GetName(), "Earth");
		TS_ASSERT_EQUALS(myWorld.GetGUID(), "Earth");
		TS_ASSERT_EQUALS(myWorld.GetSectors()->GetSize(), 0);

		TS_ASSERT_THROWS_NOTHING(myWorld.Update(myWorldState));
	}

	void TestWorldCreateSector()
	{
		WorldState myWorldState;
		World myWorld("Earth");

		TS_ASSERT_THROWS_NOTHING(myWorld.Update(myWorldState));

		Sector* newSector1 = myWorld.CreateSector("Ireland");

		TS_ASSERT_DIFFERS(newSector1, nullptr);
		TS_ASSERT_EQUALS(newSector1->GetName(), "Ireland");
		TS_ASSERT_EQUALS(newSector1->GetGUID(), "Earth_Ireland_0");
		TS_ASSERT_EQUALS(newSector1->GetWorld(), &myWorld);
		TS_ASSERT_EQUALS(myWorld.GetSectors()->GetSize(), 1); // There is 1 unique name.
		TS_ASSERT_EQUALS(myWorld["sectors"].GetTable()->Find("Ireland")->GetTable(0), newSector1);

		TS_ASSERT_THROWS_NOTHING(myWorld.Update(myWorldState));

		Sector* newSector2 = myWorld.CreateSector("England");
		Sector* newSector3 = myWorld.CreateSector("Ireland");

		TS_ASSERT_EQUALS(newSector2->GetGUID(), "Earth_England_0");
		TS_ASSERT_EQUALS(newSector3->GetGUID(), "Earth_Ireland_1");
		TS_ASSERT_EQUALS(myWorld.GetSectors()->GetSize(), 2); // There are 2 unique names.
		TS_ASSERT_EQUALS(myWorld["sectors"].GetTable()->Find("England")->GetTable(0), newSector2);
		TS_ASSERT_EQUALS(myWorld["sectors"].GetTable()->Find("Ireland")->GetTable(1), newSector3);

		TS_ASSERT_THROWS_NOTHING(myWorld.Update(myWorldState));

	}
};