#include "FooSubscriber.h"
#include "EventPublisher.h"
#include "FooMessage.h"
#include "Event.h"

using namespace Library;

namespace UnitTests
{

	FooSubscriber::FooSubscriber()
		: EventSubscriber(), mVal(0)

	{
	}

	void FooSubscriber::Notify(std::shared_ptr<EventPublisher> eventPublisher)
	{
		const FooMessage& message = eventPublisher->As<Event<FooMessage>>()->GetMessage();

		mVal = message.GetValue();
	}

	int FooSubscriber::GetValue() const
	{
		return mVal;
	}

}