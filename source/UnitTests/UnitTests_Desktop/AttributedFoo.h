#pragma once

#include <cstdint>
#include "Attributed.h"
#include "Foo.h"

namespace UnitTests
{

	class AttributedFoo : public Library::Attributed
	{
		RTTI_DECLARATIONS(AttributedFoo, Attributed);

	public:
		AttributedFoo();
		AttributedFoo(AttributedFoo& rhs);
		virtual ~AttributedFoo();
		AttributedFoo& operator=(AttributedFoo& rhs);

		void Populate();

		int mInt;
		float mFloat;
		glm::vec4 mVec;
		glm::mat4 mMat;
		std::string mStr;

		int* mIntArray;
		float* mFloatArray;
		glm::vec4* mVecArray;
		glm::mat4* mMatArray;
		std::string* mStrArray;

		Foo aFoo;
		Foo *aFooArray;
	};

}