#include <cxxtest/TestSuite.h>
#include "Stack.h"
#include "Foo.h"

using namespace Library;

class StackTestSuite : public CxxTest::TestSuite
{
public:
	void setUp()
	{
		TS_ASSERT(mIntStack.IsEmpty());
		TS_ASSERT(mFooStack.IsEmpty());
		TS_ASSERT(mPtrStack.IsEmpty());
	}

	void tearDown()
	{
		mIntStack.Clear();
		mFooStack.Clear();
		mPtrStack.Clear();
	}

	void TestStackInstantiation()
	{
		Stack<int>* tmpIntStack = new Stack<int>();
		TS_ASSERT(tmpIntStack->IsEmpty());
		delete tmpIntStack;

		Stack<int>* tmpFooStack = new Stack<int>();
		TS_ASSERT(tmpFooStack->IsEmpty());
		delete tmpFooStack;

		Stack<int>* tmpPtrStack = new Stack<int>();
		TS_ASSERT(tmpPtrStack->IsEmpty());
		delete tmpPtrStack;
	}

	void TestStackManip()
	{
		mIntStack.Push(3);
		mIntStack.Push(2);
		mIntStack.Push(1);
		TS_ASSERT_EQUALS(mIntStack.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntStack.Top(), 1);
		TS_ASSERT_EQUALS(mIntStack.Pop(), 1);
		TS_ASSERT_EQUALS(mIntStack.GetSize(), 2);
		TS_ASSERT_EQUALS(mIntStack.Top(), 2);
		mIntStack.Clear();
		TS_ASSERT_EQUALS(mIntStack.GetSize(), 0);
		TS_ASSERT(mIntStack.IsEmpty());

		Foo foo;
		foo.setValue(3);
		mFooStack.Push(foo);
		foo.setValue(2);
		mFooStack.Push(foo);
		foo.setValue(1);
		mFooStack.Push(foo);
		TS_ASSERT_EQUALS(mFooStack.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooStack.Top().getValue(), 1);
		TS_ASSERT_EQUALS(mFooStack.Pop().getValue(), 1);
		TS_ASSERT_EQUALS(mFooStack.GetSize(), 2);
		TS_ASSERT_EQUALS(mFooStack.Top().getValue(), 2);
		mFooStack.Clear();
		TS_ASSERT_EQUALS(mFooStack.GetSize(), 0);
		TS_ASSERT(mFooStack.IsEmpty());

		int tmpPtrs[] = { 3, 2, 1 };
		mPtrStack.Push(&tmpPtrs[0]);
		mPtrStack.Push(&tmpPtrs[1]);
		mPtrStack.Push(&tmpPtrs[2]);
		TS_ASSERT_EQUALS(mPtrStack.GetSize(), 3);
		TS_ASSERT_EQUALS(*mPtrStack.Top(), 1);
		TS_ASSERT_EQUALS(*mPtrStack.Pop(), 1);
		TS_ASSERT_EQUALS(mPtrStack.GetSize(), 2);
		TS_ASSERT_EQUALS(*mPtrStack.Top(), 2);
		mPtrStack.Clear();
		TS_ASSERT_EQUALS(mPtrStack.GetSize(), 0);
		TS_ASSERT(mPtrStack.IsEmpty());
	}

	void TestStackCopy()
	{
		mIntStack.Push(3);
		mIntStack.Push(2);
		mIntStack.Push(1);
		const Stack<int>& cIntStack = mIntStack;
		Stack<int> tmpIntStack1 = mIntStack, tmpIntStack2;
		tmpIntStack2 = mIntStack;
		TS_ASSERT_EQUALS(cIntStack.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpIntStack1.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpIntStack2.GetSize(), 3);
		TS_ASSERT_EQUALS(cIntStack.Top(), 1);
		TS_ASSERT_EQUALS(tmpIntStack1.Top(), 1);
		TS_ASSERT_EQUALS(tmpIntStack2.Top(), 1);
		mIntStack.Pop();
		TS_ASSERT_EQUALS(cIntStack.GetSize(), 2);
		TS_ASSERT_EQUALS(tmpIntStack1.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpIntStack2.GetSize(), 3);
		TS_ASSERT_EQUALS(cIntStack.Top(), 2);
		TS_ASSERT_EQUALS(tmpIntStack1.Top(), 1);
		TS_ASSERT_EQUALS(tmpIntStack2.Top(), 1);

		Foo foo;
		foo.setValue(3);
		mFooStack.Push(foo);
		foo.setValue(2);
		mFooStack.Push(foo);
		foo.setValue(1);
		mFooStack.Push(foo);
		const Stack<Foo>& cFooStack = mFooStack;
		Stack<Foo> tmpFooStack1 = mFooStack, tmpFooStack2;
		tmpFooStack2 = mFooStack;
		TS_ASSERT_EQUALS(cFooStack.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpFooStack1.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpFooStack2.GetSize(), 3);
		TS_ASSERT_EQUALS(cFooStack.Top().getValue(), 1);
		TS_ASSERT_EQUALS(tmpFooStack1.Top().getValue(), 1);
		TS_ASSERT_EQUALS(tmpFooStack2.Top().getValue(), 1);
		mFooStack.Pop();
		TS_ASSERT_EQUALS(cFooStack.GetSize(), 2);
		TS_ASSERT_EQUALS(tmpFooStack1.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpFooStack2.GetSize(), 3);
		TS_ASSERT_EQUALS(cFooStack.Top().getValue(), 2);
		TS_ASSERT_EQUALS(tmpFooStack1.Top().getValue(), 1);
		TS_ASSERT_EQUALS(tmpFooStack2.Top().getValue(), 1);

		int tmpPtrs[] = { 3, 2, 1 };
		mPtrStack.Push(&tmpPtrs[0]);
		mPtrStack.Push(&tmpPtrs[1]);
		mPtrStack.Push(&tmpPtrs[2]);
		const Stack<int*>& cPtrStack = mPtrStack;
		Stack<int*> tmpPtrStack1 = mPtrStack, tmpPtrStack2;
		tmpPtrStack2 = mPtrStack;
		TS_ASSERT_EQUALS(cPtrStack.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpPtrStack1.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpPtrStack2.GetSize(), 3);
		TS_ASSERT_EQUALS(*cPtrStack.Top(), 1);
		TS_ASSERT_EQUALS(*tmpPtrStack1.Top(), 1);
		TS_ASSERT_EQUALS(*tmpPtrStack2.Top(), 1);
		mPtrStack.Pop();
		TS_ASSERT_EQUALS(cPtrStack.GetSize(), 2);
		TS_ASSERT_EQUALS(tmpPtrStack1.GetSize(), 3);
		TS_ASSERT_EQUALS(tmpPtrStack2.GetSize(), 3);
		TS_ASSERT_EQUALS(*cPtrStack.Top(), 2);
		TS_ASSERT_EQUALS(*tmpPtrStack1.Top(), 1);
		TS_ASSERT_EQUALS(*tmpPtrStack2.Top(), 1);
	}

	void TestStackThrows()
	{
		TS_ASSERT_THROWS(mIntStack.Pop(), std::runtime_error);
		TS_ASSERT_THROWS(mIntStack.Top(), std::runtime_error);
		const Stack<int>& cIntStack = mIntStack;
		TS_ASSERT_THROWS(cIntStack.Top(), std::runtime_error);

		TS_ASSERT_THROWS(mFooStack.Pop(), std::runtime_error);
		TS_ASSERT_THROWS(mFooStack.Top(), std::runtime_error);
		const Stack<Foo>& cFooStack = mFooStack;
		TS_ASSERT_THROWS(cFooStack.Top(), std::runtime_error);

		TS_ASSERT_THROWS(mPtrStack.Pop(), std::runtime_error);
		TS_ASSERT_THROWS(mPtrStack.Top(), std::runtime_error);
		const Stack<int*>& cPtrStack = mPtrStack;
		TS_ASSERT_THROWS(cPtrStack.Top(), std::runtime_error);
	}

private:
	Stack<int> mIntStack;
	Stack<Foo> mFooStack;
	Stack<int*> mPtrStack;
};