#include <cxxtest/TestSuite.h>
#include "SList.h"
#include "Foo.h"

using namespace Library;

// Test with at least one native data type, and at least one custom data type (class).
// Look into using the memory leak detection code (crtdbg) for unit tests.

class SListTestSuite : public CxxTest::TestSuite
{
public:
	// Something about "testFixtureSetUp" happening at the beginning of the entire test suite?

	void setUp()
	{
		// Happens before every single test in the test suite
		TS_ASSERT(mIntList.IsEmpty());
		TS_ASSERT(mFooList.IsEmpty());
		TS_ASSERT(mPtrList.IsEmpty());
	}

	void tearDown()
	{
		// Happens after every single test in the test suite
		mIntList.Clear();
		mFooList.Clear();
		mPtrList.Clear();
	}
	
	// Test names need to start with "Test", apparently
	void TestSListInstantiation(void)
	{
		SList<int> * tmpIntList = new SList<int>();
		TS_ASSERT(tmpIntList->IsEmpty());
		delete tmpIntList;

		SList<Foo> * tmpFooList = new SList<Foo>();
		TS_ASSERT(tmpFooList->IsEmpty());
		delete tmpFooList;
	}

	void TestSListPrependEmpty(void)
	{
		int intVal = 1;
		mIntList.PushFront(intVal);	// [1]
		intVal = 2;
		mIntList.PushFront(intVal);	// [2, 1]
		mIntList.PushFront(intVal);	// [2, 2, 1]
		TS_ASSERT_EQUALS(mIntList.Front(), 2);
		TS_ASSERT_EQUALS(mIntList.Back(), 1);
		TS_ASSERT_EQUALS(mIntList.GetSize(), 3);
		intVal = mIntList.PopFront();
		TS_ASSERT_EQUALS(intVal, 2);	// [2, 1]
		TS_ASSERT_EQUALS(mIntList.GetSize(), 2);
		mIntList.Clear();	// [ ]
		TS_ASSERT_EQUALS(mIntList.GetSize(), 0);

		Foo foo;
		foo.setValue(1);
		mFooList.PushFront(foo);	// [1]
		foo.setValue(2);
		mFooList.PushFront(foo);	// [2, 1]
		mFooList.PushFront(foo);	// [2, 2, 1]
		TS_ASSERT_EQUALS(mFooList.Front().getValue(), 2);
		TS_ASSERT_EQUALS(mFooList.Back().getValue(), 1);
		TS_ASSERT_EQUALS(mFooList.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooList.PopFront().getValue(), 2);	// [2, 1]
		TS_ASSERT_EQUALS(mFooList.GetSize(), 2);
		mFooList.Clear();	// [ ]
		TS_ASSERT_EQUALS(mFooList.GetSize(), 0);

		int* ptr;
		ptr = new int(1);
		mPtrList.PushFront(ptr);	// [1]
		ptr = new int(2);
		mPtrList.PushFront(ptr);	// [2, 1]
		ptr = new int(2);
		mPtrList.PushFront(ptr);	// [2, 2, 1]
		TS_ASSERT_EQUALS(*mPtrList.Front(), 2);
		TS_ASSERT_EQUALS(*mPtrList.Back(), 1);
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 3);
		ptr = mPtrList.PopFront();
		TS_ASSERT_EQUALS(*ptr, 2);	// [2, 1]
		delete ptr;
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 2);
		while (mPtrList.GetSize() > 0) { // [ ]
			delete mPtrList.PopFront();
		}
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 0);
	}

	void TestSListAppendEmpty(void)
	{
		mIntList.PushBack(1);	// [1]
		mIntList.PushBack(2);	// [1, 2]
		mIntList.PushBack(3);	// [1, 2, 3]
		TS_ASSERT_EQUALS(mIntList.Front(), 1);
		TS_ASSERT_EQUALS(mIntList.Back(), 3);
		TS_ASSERT_EQUALS(mIntList.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntList.PopFront(), 1);	// [2, 3]
		TS_ASSERT_EQUALS(mIntList.GetSize(), 2);
		mIntList.Clear();	// [ ]
		TS_ASSERT_EQUALS(mIntList.GetSize(), 0);

		Foo foo;
		foo.setValue(1);
		mFooList.PushBack(foo);	// [1]
		foo.setValue(2);
		mFooList.PushBack(foo);	// [1, 2]
		foo.setValue(3);
		mFooList.PushBack(foo);	// [1, 2, 3]
		TS_ASSERT_EQUALS(mFooList.Front().getValue(), 1);
		TS_ASSERT_EQUALS(mFooList.Back().getValue(), 3);
		TS_ASSERT_EQUALS(mFooList.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooList.PopFront().getValue(), 1);	// [2, 3]
		TS_ASSERT_EQUALS(mFooList.GetSize(), 2);
		mFooList.Clear();	// [ ]
		TS_ASSERT_EQUALS(mFooList.GetSize(), 0);

		int* ptr;
		ptr = new int(1);
		mPtrList.PushBack(ptr);	// [1]
		ptr = new int(2);
		mPtrList.PushBack(ptr);	// [1, 2]
		ptr = new int(3);
		mPtrList.PushBack(ptr);	// [1, 2, 3]
		TS_ASSERT_EQUALS(*mPtrList.Front(), 1);
		TS_ASSERT_EQUALS(*mPtrList.Back(), 3);
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 3);
		ptr = mPtrList.PopFront();
		TS_ASSERT_EQUALS(*ptr, 1);	// [2, 1]
		delete ptr;
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 2);
		while (mPtrList.GetSize() > 0) { // [ ]
			delete mPtrList.PopFront();
		}
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 0);
	}

	void TestSListMixedEmpty(void)
	{
		mIntList.PushBack(1);	// [1]
		mIntList.PushFront(2);	// [2, 1]
		mIntList.PushBack(3);	// [2, 1, 3]
		TS_ASSERT_EQUALS(mIntList.Front(), 2);
		TS_ASSERT_EQUALS(mIntList.Back(), 3);
		TS_ASSERT_EQUALS(mIntList.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntList.PopFront(), 2);	// [1, 3]
		TS_ASSERT_EQUALS(mIntList.GetSize(), 2);
		TS_ASSERT_EQUALS(mIntList.PopFront(), 1);	// [3]
		TS_ASSERT_EQUALS(mIntList.PopFront(), 3);	// [ ]
		TS_ASSERT_EQUALS(mIntList.GetSize(), 0);

		Foo foo;
		foo.setValue(1);
		mFooList.PushBack(foo);		// [1]
		foo.setValue(2);
		mFooList.PushFront(foo);	// [2, 1]
		foo.setValue(3);
		mFooList.PushBack(foo);		// [2, 1, 3]
		TS_ASSERT_EQUALS(mFooList.Front().getValue(), 2);
		TS_ASSERT_EQUALS(mFooList.Back().getValue(), 3);
		TS_ASSERT_EQUALS(mFooList.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooList.PopFront().getValue(), 2);	// [1, 3]
		TS_ASSERT_EQUALS(mFooList.GetSize(), 2);
		TS_ASSERT_EQUALS(mFooList.PopFront().getValue(), 1);	// [3]
		TS_ASSERT_EQUALS(mFooList.PopFront().getValue(), 3);	// [ ]
		TS_ASSERT_EQUALS(mFooList.GetSize(), 0);

		int* ptr;
		ptr = new int(1);
		mPtrList.PushBack(ptr);		// [1]
		ptr = new int(2);
		mPtrList.PushFront(ptr);	// [2, 1]
		ptr = new int(3);
		mPtrList.PushBack(ptr);		// [2, 1, 3]
		TS_ASSERT_EQUALS(*mPtrList.Front(), 2);
		TS_ASSERT_EQUALS(*mPtrList.Back(), 3);
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 3);
		ptr = mPtrList.PopFront();	// [1, 3]
		TS_ASSERT_EQUALS(*ptr, 2);
		delete ptr;
		ptr = mPtrList.PopFront();	// [3]
		TS_ASSERT_EQUALS(*ptr, 1);
		delete ptr;
		ptr = mPtrList.PopFront();	// [ ]
		TS_ASSERT_EQUALS(*ptr, 3);
		delete ptr;
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 0);
	}

	void TestSListCopy(void)
	{
		SList<int> mIntTmp1List;

		mIntList.PushFront(1);
		mIntList.PushFront(2);
		mIntList.PushFront(3);
		mIntTmp1List = mIntList;
		SList<int> mIntTmp2List = mIntList;
		mIntList.PopFront();
		mIntTmp2List.PushFront(50);
		TS_ASSERT_EQUALS(mIntList.GetSize(), 2);
		TS_ASSERT_EQUALS(mIntList.Front(), 2);
		TS_ASSERT_EQUALS(mIntTmp1List.GetSize(), 3);
		TS_ASSERT_EQUALS(mIntTmp1List.Front(), 3);
		TS_ASSERT_EQUALS(mIntTmp2List.GetSize(), 4);
		TS_ASSERT_EQUALS(mIntTmp2List.Front(), 50);
		mIntList.Clear();

		SList<Foo> mFooTmp1List;
		Foo foo;
		foo.setValue(1);
		mFooList.PushFront(foo);
		foo.setValue(2);
		mFooList.PushFront(foo);
		foo.setValue(3);
		mFooList.PushFront(foo);
		mFooTmp1List = mFooList;
		SList<Foo> mFooTmp2List = mFooList;
		mFooList.PopFront();
		foo.setValue(50);
		mFooTmp2List.PushFront(foo);
		TS_ASSERT_EQUALS(mFooList.GetSize(), 2);
		TS_ASSERT_EQUALS(mFooList.Front().getValue(), 2);
		TS_ASSERT_EQUALS(mFooTmp1List.GetSize(), 3);
		TS_ASSERT_EQUALS(mFooTmp1List.Front().getValue(), 3);
		TS_ASSERT_EQUALS(mFooTmp2List.GetSize(), 4);
		TS_ASSERT_EQUALS(mFooTmp2List.Front().getValue(), 50);
		mFooList.Clear();

		SList<int*> mPtrTmp1List;
		int* ptr;
		ptr = new int(1);
		mPtrList.PushFront(ptr);
		ptr = new int(2);
		mPtrList.PushFront(ptr);
		ptr = new int(3);
		mPtrList.PushFront(ptr);
		mPtrTmp1List = mPtrList;
		SList<int*> mPtrTmp2List = mPtrList;
		mPtrList.PopFront();
		ptr = new int(50);
		mPtrTmp2List.PushFront(ptr);
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 2);
		TS_ASSERT_EQUALS(*mPtrList.Front(), 2);
		TS_ASSERT_EQUALS(mPtrTmp1List.GetSize(), 3);
		TS_ASSERT_EQUALS(*mPtrTmp1List.Front(), 3);
		TS_ASSERT_EQUALS(mPtrTmp2List.GetSize(), 4);
		TS_ASSERT_EQUALS(*mPtrTmp2List.Front(), 50);
		while (mPtrTmp2List.GetSize() > 0) {
			delete mPtrTmp2List.PopFront();
		}
	}

	void TestSListEmpty(void)
	{
		// For the int list.
		TS_ASSERT_THROWS(mIntList.PopFront(), std::runtime_error);
		TS_ASSERT_THROWS(mIntList.Front(), std::runtime_error);
		TS_ASSERT_THROWS(mIntList.Back(), std::runtime_error);

		// For the Foo list.
		TS_ASSERT_THROWS(mFooList.PopFront(), std::runtime_error);
		TS_ASSERT_THROWS(mFooList.Front(), std::runtime_error);
		TS_ASSERT_THROWS(mFooList.Back(), std::runtime_error);

		// For the Ptr list.
		TS_ASSERT_THROWS(mPtrList.PopFront(), std::runtime_error);
		TS_ASSERT_THROWS(mPtrList.Front(), std::runtime_error);
		TS_ASSERT_THROWS(mPtrList.Back(), std::runtime_error);
	}


	void TestSListIteratorCompare()
	{
		mIntList.PushBack(1);
		mIntList.PushBack(2);
		mIntList.PushBack(2);
		mIntList.PushBack(3);	//	[1, 2, 2, 3]

		SList<int>::Iterator it1, it2;
		TS_ASSERT(it1 == it2);
		TS_ASSERT(!(it1 != it2));

		it1 = mIntList.begin();
		it2 = mIntList.begin();
		TS_ASSERT(it1 == it2);
		++it2;
		TS_ASSERT(it1 != it2);
		it1 = it2++;	// They now point to the two 2s.
		TS_ASSERT(it1 != it2);
		it1++;
		TS_ASSERT(it1 == it2); // They now point to the same 2.
		it1 = ++it2;	// They both point to the 3.
		TS_ASSERT(it1 == it2);
		it2++;
		TS_ASSERT_EQUALS(*it1, 3);
		TS_ASSERT(it2 == mIntList.end());
		TS_ASSERT_THROWS(*it2, std::runtime_error);

		SList<int> mTmpList;
		mTmpList.PushBack(1);
		it1 = mIntList.begin();
		it2 = mTmpList.end();
		TS_ASSERT(it1 != it2);
	}

	void TestSListInsertAfter()
	{
		// Testing with ints.
		mIntList.PushBack(1);
		mIntList.PushBack(2);
		mIntList.PushBack(3);
		SList<int>::Iterator it = mIntList.begin();
		it++;
		TS_ASSERT_EQUALS(*it, 2);
		// Insert!
		mIntList.InsertAfter(it, 5);
		TS_ASSERT_EQUALS(*it, 2);
		TS_ASSERT_EQUALS(mIntList.GetSize(), 4);
		it++;
		TS_ASSERT_EQUALS(*it, 5);
		it++;
		TS_ASSERT_EQUALS(*it, 3);
		it++;
		TS_ASSERT(it == mIntList.end());

		SList<int> tmpIntList;
		it = tmpIntList.begin();
		TS_ASSERT_THROWS(mIntList.FindNext(it, 5), std::runtime_error);

		mIntList.Clear();

		// Testing with Foos
		Foo tmpFoo;
		tmpFoo.setValue(1);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(2);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(3);
		mFooList.PushBack(tmpFoo);
		SList<Foo>::Iterator itF = mFooList.begin();
		itF++;
		TS_ASSERT_EQUALS((*itF).getValue(), 2);
		tmpFoo.setValue(5);
		mFooList.InsertAfter(itF, tmpFoo);
		TS_ASSERT_EQUALS((*itF).getValue(), 2);
		TS_ASSERT_EQUALS(mFooList.GetSize(), 4);
		itF++;
		TS_ASSERT_EQUALS((*itF).getValue(), 5);
		itF++;
		TS_ASSERT_EQUALS((*itF).getValue(), 3);
		itF++;
		TS_ASSERT(itF == mFooList.end());

		SList<Foo> tmpFooList;
		itF = tmpFooList.begin();
		TS_ASSERT_THROWS(mFooList.FindNext(itF, tmpFoo), std::runtime_error);

		mFooList.Clear();

		// Testing with pointers
		int tmpInts[] = { 1, 2, 3, 5 };
		mPtrList.PushBack(&tmpInts[0]);
		mPtrList.PushBack(&tmpInts[1]);
		mPtrList.PushBack(&tmpInts[2]);
		SList<int*>::Iterator itP = mPtrList.begin();
		itP++;
		TS_ASSERT_EQUALS(**itP, 2);
		// Insert!
		mPtrList.InsertAfter(itP, &tmpInts[3]);
		TS_ASSERT_EQUALS(**itP, 2);
		TS_ASSERT_EQUALS(mPtrList.GetSize(), 4);
		itP++;
		TS_ASSERT_EQUALS(**itP, 5);
		itP++;
		TS_ASSERT_EQUALS(**itP, 3);
		itP++;
		TS_ASSERT(itP == mPtrList.end());

		SList<int*> tmpPtrList;
		itP = tmpPtrList.begin();
		TS_ASSERT_THROWS(mPtrList.FindNext(itP, &tmpInts[3]), std::runtime_error);

		mPtrList.Clear();
	}

	// Tests both Find functions.
	void TestSListFind()
	{
		// Testing with ints.
		mIntList.PushBack(1);
		mIntList.PushBack(2);
		mIntList.PushBack(2);
		mIntList.PushBack(3);
		mIntList.PushBack(2);
		mIntList.PushBack(4);
		SList<int>::Iterator itI = mIntList.begin();
		SList<int> tmpIntList;

		itI = mIntList.Find(2);
		TS_ASSERT(itI != mIntList.end() && itI != mIntList.begin());	// We found the first 2?
		itI++;
		itI = mIntList.FindNext(itI, 2);
		TS_ASSERT(itI != mIntList.end() && itI != mIntList.begin());	// We found the second 2?
		itI++;
		TS_ASSERT(*itI == 3)											// Yeah, that was the second 2.
		itI = mIntList.FindNext(itI, 2);
		TS_ASSERT(itI != mIntList.end() && itI != mIntList.begin());	// Found the third 2
		itI++;
		itI = mIntList.FindNext(itI, 2);
		TS_ASSERT(itI == mIntList.end());								// Tried to search the rest of the list where there are no more 2s.
		itI++;
		itI = mIntList.FindNext(itI, 2);
		TS_ASSERT(itI == mIntList.end());								// Tried to search the rest of the list starting from the end.

		itI = tmpIntList.begin();
		TS_ASSERT_THROWS(mIntList.FindNext(itI, 2), std::runtime_error);
		mIntList.Clear();

		// Testing with Foos
		Foo tmpFoo;
		tmpFoo.setValue(1);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(2);
		mFooList.PushBack(tmpFoo);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(3);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(2);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(4);
		mFooList.PushBack(tmpFoo);
		SList<Foo>::Iterator itF = mFooList.begin();
		SList<Foo> tmpFooList;

		tmpFoo.setValue(2);
		itF = mFooList.Find(tmpFoo);
		TS_ASSERT(itF != mFooList.end() && itF != mFooList.begin());	// We found the first 2?
		itF++;
		itF = mFooList.FindNext(itF, tmpFoo);
		TS_ASSERT(itF != mFooList.end() && itF != mFooList.begin());	// We found the second 2?
		itF++;
		TS_ASSERT((*itF).getValue() == 3)											// Yeah, that was the second 2.
			itF = mFooList.FindNext(itF, tmpFoo);
		TS_ASSERT(itF != mFooList.end() && itF != mFooList.begin());	// Found the third 2
		itF++;
		itF = mFooList.FindNext(itF, tmpFoo);
		TS_ASSERT(itF == mFooList.end());								// Tried to search the rest of the list where there are no more 2s.
		itF++;
		itF = mFooList.FindNext(itF, tmpFoo);
		TS_ASSERT(itF == mFooList.end());								// Tried to search the rest of the list starting from the end.

		itF = tmpFooList.begin();
		TS_ASSERT_THROWS(mFooList.FindNext(itF, tmpFoo), std::runtime_error);
		mFooList.Clear();

		// Testing with pointers
		int tmpInts[] = { 1, 2, 2, 3, 4 };
		mPtrList.PushBack(&tmpInts[0]);
		mPtrList.PushBack(&tmpInts[1]);
		mPtrList.PushBack(&tmpInts[2]);
		mPtrList.PushBack(&tmpInts[3]);
		mPtrList.PushBack(&tmpInts[1]);
		mPtrList.PushBack(&tmpInts[4]);
		SList<int*>::Iterator itP = mPtrList.begin();
		SList<int*> tmpPtrList;


		itP = mPtrList.Find(&tmpInts[1]);
		TS_ASSERT(itP != mPtrList.end() && itP != mPtrList.begin());	// We found the first 2?
		itP++;
		itP = mPtrList.FindNext(itP, &tmpInts[1]);
		TS_ASSERT(itP != mPtrList.end() && itP != mPtrList.begin());	// We found the second 2?
		itP++;
		TS_ASSERT(**itP == 4);											// Yeah, that was the second 2.
		itP++;
		itP = mPtrList.FindNext(itP, &tmpInts[1]);
		TS_ASSERT(itP == mPtrList.end());								// Tried to search the rest of the list where there are no more 2s.
		itP++;
		itP = mPtrList.FindNext(itP, &tmpInts[1]);
		TS_ASSERT(itP == mPtrList.end());								// Tried to search the rest of the list starting from the end.

		itP = tmpPtrList.begin();
		TS_ASSERT_THROWS(mPtrList.FindNext(itP, &tmpInts[1]), std::runtime_error);
		mPtrList.Clear();
	}

	// Tests both Remove functions.
	void TestSListRemove()
	{
		// Testing with ints.
		mIntList.PushBack(1);
		mIntList.PushBack(2);
		mIntList.PushBack(2);
		mIntList.PushBack(3);
		mIntList.PushBack(2);
		mIntList.PushBack(4);

		TS_ASSERT(mIntList.GetSize() == 6);
		mIntList.Remove(2);
		TS_ASSERT(mIntList.GetSize() == 5);
		mIntList.RemoveAll(2);
		TS_ASSERT(mIntList.GetSize() == 3);
		SList<int>::Iterator itI = mIntList.begin();
		TS_ASSERT(*itI == 1);
		mIntList.Remove(2);				// Do nothing.
		itI++;
		TS_ASSERT(*itI == 3);
		mIntList.RemoveAll(2);			// Do nothing.
		itI++;
		TS_ASSERT(*itI == 4);
		itI++;
		TS_ASSERT(itI == mIntList.end());

		// Testing with Foos
		Foo tmpFoo;
		tmpFoo.setValue(1);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(2);
		mFooList.PushBack(tmpFoo);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(3);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(2);
		mFooList.PushBack(tmpFoo);
		tmpFoo.setValue(4);
		mFooList.PushBack(tmpFoo);

		tmpFoo.setValue(2);
		TS_ASSERT(mFooList.GetSize() == 6);
		mFooList.Remove(tmpFoo);
		TS_ASSERT(mFooList.GetSize() == 5);
		mFooList.RemoveAll(tmpFoo);
		TS_ASSERT(mFooList.GetSize() == 3);
		SList<Foo>::Iterator itF = mFooList.begin();
		TS_ASSERT((*itF).getValue() == 1);
		mFooList.Remove(tmpFoo);			// Should do nothing
		itF++;
		TS_ASSERT((*itF).getValue() == 3);
		mFooList.RemoveAll(tmpFoo);			// Should do nothing
		itF++;
		TS_ASSERT((*itF).getValue() == 4);
		itF++;
		TS_ASSERT(itF == mFooList.end());

		// Testing with pointers
		int tmpInts[] = { 1, 2, 3, 4 };
		mPtrList.PushBack(&tmpInts[0]);
		mPtrList.PushBack(&tmpInts[1]);
		mPtrList.PushBack(&tmpInts[1]);
		mPtrList.PushBack(&tmpInts[2]);
		mPtrList.PushBack(&tmpInts[1]);
		mPtrList.PushBack(&tmpInts[3]);
		
		TS_ASSERT(mPtrList.GetSize() == 6);
		mPtrList.Remove(&tmpInts[1]);
		TS_ASSERT(mPtrList.GetSize() == 5);
		mPtrList.RemoveAll(&tmpInts[1]);
		TS_ASSERT(mPtrList.GetSize() == 3);
		SList<int*>::Iterator itP = mPtrList.begin();
		TS_ASSERT(**itP == 1);
		mPtrList.Remove(&tmpInts[1]);		// Do nothing
		itP++;
		TS_ASSERT(**itP == 3);
		mPtrList.RemoveAll(&tmpInts[1]);	// Do nothing
		itP++;
		TS_ASSERT(**itP == 4);
		itP++;
		TS_ASSERT(itP == mPtrList.end());
	}

private:
	SList<int> mIntList;
	SList<Foo> mFooList;
	SList<int*> mPtrList;
};