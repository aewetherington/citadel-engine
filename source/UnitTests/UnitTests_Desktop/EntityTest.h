#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>

#include "Entity.h"
#include "Sector.h"

using namespace Library;

class EntityTestSuite : public CxxTest::TestSuite
{
public:
	void TestEntityInstantiate()
	{
		Entity myEntity("Alex");

		TS_ASSERT_EQUALS(myEntity.GetName(), "Alex");
		TS_ASSERT_EQUALS(myEntity.GetGUID(), "Alex_" + std::to_string(UINT32_MAX));
		TS_ASSERT_EQUALS(myEntity.GetSector(), nullptr);
	}

	void TestEntityParentSector()
	{
		Sector mySector("Ireland");
		Entity myEntity("Alex");

		TS_ASSERT_THROWS_NOTHING(myEntity.SetSector(&mySector));

		TS_ASSERT_EQUALS(myEntity.GetName(), "Alex");
		TS_ASSERT_EQUALS(myEntity.GetGUID(), "Ireland_" + std::to_string(UINT32_MAX) + "_Alex_0");
		TS_ASSERT_EQUALS(myEntity.GetSector(), &mySector);
	}

	void TestEntityCopy()
	{
		Sector mySector("Ireland");
		Entity myEntity("Alex");

		TS_ASSERT_THROWS_NOTHING(myEntity.SetSector(&mySector));

		TS_ASSERT_EQUALS(myEntity.GetName(), "Alex");
		TS_ASSERT_EQUALS(myEntity.GetGUID(), "Ireland_" + std::to_string(UINT32_MAX) + "_Alex_0");
		TS_ASSERT_EQUALS(myEntity.GetSector(), &mySector);

		Entity entity1 = myEntity, entity2;
		entity2 = myEntity;

		TS_ASSERT_EQUALS(entity1.GetName(), "Alex");
		TS_ASSERT_EQUALS(entity1.GetGUID(), "Alex_" + std::to_string(UINT32_MAX));
		TS_ASSERT_EQUALS(entity1.GetSector(), nullptr);

		TS_ASSERT_EQUALS(entity1.GetName(), "Alex");
		TS_ASSERT_EQUALS(entity1.GetGUID(), "Alex_" + std::to_string(UINT32_MAX));
		TS_ASSERT_EQUALS(entity1.GetSector(), nullptr);

		TS_ASSERT_THROWS_NOTHING(myEntity.SetName("Alicia"));

		TS_ASSERT_EQUALS(myEntity.GetName(), "Alicia");
		TS_ASSERT_EQUALS(myEntity.GetGUID(), "Ireland_" + std::to_string(UINT32_MAX) + "_Alicia_0");
		TS_ASSERT_EQUALS(myEntity.GetSector(), &mySector);

		TS_ASSERT_EQUALS(entity1.GetName(), "Alex");
		TS_ASSERT_EQUALS(entity1.GetGUID(), "Alex_" + std::to_string(UINT32_MAX));
		TS_ASSERT_EQUALS(entity1.GetSector(), nullptr);

		TS_ASSERT_EQUALS(entity1.GetName(), "Alex");
		TS_ASSERT_EQUALS(entity1.GetGUID(), "Alex_" + std::to_string(UINT32_MAX));
		TS_ASSERT_EQUALS(entity1.GetSector(), nullptr);
	}
};