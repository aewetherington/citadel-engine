#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include "Scope.h"

using namespace Library;

class ScopeTestSuite : public CxxTest::TestSuite
{
public:

	void TestScopeAppend()
	{
		Scope scope;
		int nums[] = { 1, 2, 3 };

		scope["Number"] = 10;
		scope["Values"].SetType(Datum::DatumType::Integer);
		scope["Values"].SetStorage(nums, 3);
		scope["Number"].Set(11, 1);

		TS_ASSERT_EQUALS(scope["Number"].Get<int>(0), 10);
		TS_ASSERT_EQUALS(scope["Number"].Get<int>(1), 11);

		TS_ASSERT_EQUALS(scope["Values"].Get<int>(0), 1);
		TS_ASSERT_EQUALS(scope["Values"].Get<int>(1), 2);
		TS_ASSERT_EQUALS(scope["Values"].Get<int>(2), 3);

		TS_ASSERT_EQUALS(scope[1].Get<int>(1), 2);
	}

	void TestScopeAppendScope()
	{
		Scope root, *extChild = new Scope();

		root["Num"] = 0;
		extChild->Append("Num") = 2;

		root.AppendScope("Internal")["Num"] = 1;
		root.Append("External").SetStorage(&extChild, 1);
		TS_ASSERT_THROWS_ANYTHING(root.AppendScope("Num"));
		TS_ASSERT_THROWS_NOTHING(root.AppendScope("Internal"));
		TS_ASSERT_THROWS_ANYTHING(root.AppendScope("External"));

		TS_ASSERT_EQUALS(root["Num"], 0);
		TS_ASSERT_EQUALS(root["Internal"].GetTable()->Append("Num"), 1);
		TS_ASSERT_EQUALS(root["External"].GetTable()->Append("Num"), 2);
		TS_ASSERT_EQUALS(&root, root["Internal"].GetTable()->GetParent());

		delete extChild;
	}

	void TestScopeAdopt()
	{
		Scope root1, root2;

		root1["Num"] = 0;
		root1.AppendScope("Root1Child");
		root1["Root1Child"].GetTable()->Append("Num") = 1;
		root1["Root1Child"].GetTable()->AppendScope("Root1ChildChild");
		root1["Root1Child"].GetTable()->Append("Root1ChildChild").GetTable()->Append("Num") = 2;

		TS_ASSERT_THROWS_NOTHING(root2.Adopt(*root1["Root1Child"].GetTable(), "Root2Child"));

		TS_ASSERT_EQUALS(root1["Root1Child"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(root2.Find("Root2Child"), nullptr);
		TS_ASSERT_DIFFERS(root2["Root2Child"].GetTable(), nullptr);
		TS_ASSERT_DIFFERS(root2["Root2Child"].GetTable()->Find("Root1ChildChild"), nullptr);

		root1.Clear();
		root2.Clear();

		Scope *sc1 = new Scope();
		Scope *sc2 = new Scope();
		Scope *sc3 = new Scope();

		sc1->Append("Num") = 1;
		sc2->Append("Num") = 2;
		sc3->Append("Num") = 3;

		TS_ASSERT_THROWS_NOTHING(root1.Adopt(*sc1, "Children"));
		TS_ASSERT_THROWS_NOTHING(root1.Adopt(*sc2, "Children"));
		TS_ASSERT_THROWS_NOTHING(root1.Adopt(*sc3, "Children", 1));

		root2.Append("Num") = 1;
		root2.Append("External").SetStorage(&sc1, 1);
		TS_ASSERT_THROWS_ANYTHING(root2.Adopt(*sc3, "Num"));
		TS_ASSERT_THROWS_ANYTHING(root2.Adopt(*sc3, "External"));

		TS_ASSERT_EQUALS(root1["Children"].GetTable(0)->Find("Num")->Get<int>(), 1);
		TS_ASSERT_EQUALS(root1["Children"].GetTable(1)->Find("Num")->Get<int>(), 3);
		TS_ASSERT_EQUALS(root1["Children"].GetTable(2)->Find("Num")->Get<int>(), 2);

		// Test shuffling down to a nullptr, by reorganizing (readopting) the children.
		TS_ASSERT_THROWS_NOTHING(root1.Adopt(*sc3, "Children", 0));

		TS_ASSERT_EQUALS(root1["Children"].GetTable(0)->Find("Num")->Get<int>(), 3);
		TS_ASSERT_EQUALS(root1["Children"].GetTable(1)->Find("Num")->Get<int>(), 1);
		TS_ASSERT_EQUALS(root1["Children"].GetTable(2)->Find("Num")->Get<int>(), 2);

		std::string foundName;
		TS_ASSERT_EQUALS(root1.FindName(*sc3, foundName), true);
		TS_ASSERT_EQUALS(foundName, "Children");
		TS_ASSERT_EQUALS(root2.FindName(*sc3, foundName), false);
		TS_ASSERT_EQUALS(foundName, "");
	}

	void TestScopeCopy()
	{
		Scope root1;

		root1["Num"] = 0;
		root1.AppendScope("Root1Child");
		root1["Root1Child"].GetTable()->Append("Num") = 1;
		root1["Root1Child"].GetTable()->AppendScope("Root1ChildChild");
		root1["Root1Child"].GetTable()->Append("Root1ChildChild").GetTable()->Append("Num") = 2;

		Scope root2 = root1, root3;
		TS_ASSERT_THROWS_NOTHING(root3 = root1);

		TS_ASSERT(root1 == root2);
		TS_ASSERT(root1 == root3);

		root1["Root1Child"].GetTable()->Append("Unique") = 123;
		TS_ASSERT_DIFFERS(root1["Root1Child"].GetTable()->Find("Unique"), nullptr);
		TS_ASSERT_EQUALS(root2["Root1Child"].GetTable()->Find("Unique"), nullptr);
		TS_ASSERT_EQUALS(root3["Root1Child"].GetTable()->Find("Unique"), nullptr);
	}

	void TestScopeSearch()
	{
		Scope root1;

		root1["Num"] = 0;
		Scope& childLv1 = root1.AppendScope("Root1Child");
		root1["Root1Child"].GetTable()->Append("Num") = 1;
		Scope& childLv2 = root1["Root1Child"].GetTable()->AppendScope("Root1ChildChild");

		Scope* assocScope = nullptr;
		TS_ASSERT_EQUALS(childLv2.Search("Num", &assocScope)->Get<int>(), 1);
		TS_ASSERT_EQUALS(assocScope, &childLv1);
		TS_ASSERT_EQUALS(childLv1.Search("Num", &assocScope)->Get<int>(), 1);
		TS_ASSERT_EQUALS(assocScope, &childLv1);
		TS_ASSERT_EQUALS(childLv2.Search("Root1Child", &assocScope)->GetTable(), &childLv1);
		TS_ASSERT_EQUALS(assocScope, &root1);
	}

};