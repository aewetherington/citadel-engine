#include "FooParseHelper.h"
#include "XmlParseMaster.h"

namespace UnitTests
{

	FooParseHelper::FooParseHelper()
		: IXmlParseHelper(), mMaxDepth(0), mNumStartHandles(0), mNumEndHandles(0), mNumCharHandles(0), mIsClone(false)
	{

	}

	void FooParseHelper::Initialize()
	{
		mMaxDepth = 0;
		mNumStartHandles = 0;
		mNumEndHandles = 0;
		mNumCharHandles = 0;
	}

	bool FooParseHelper::StartElementHandler(Library::XmlParseMaster::SharedData* sharedData, const std::string& name, const Library::HashMap<std::string, std::string>& attributes)
	{
		++mNumStartHandles;
		return true;
	}

	bool FooParseHelper::EndElementHandler(Library::XmlParseMaster::SharedData* sharedData, std::string name)
	{
		++mNumEndHandles;
		return true;
	}

	bool FooParseHelper::CharElementHandler(Library::XmlParseMaster::SharedData* sharedData, const char* data, int length)
	{
		++mNumCharHandles;
		return true;
	}

	Library::IXmlParseHelper* FooParseHelper::Clone() const
	{
		FooParseHelper* newFoo = new FooParseHelper();
		newFoo->mIsClone = true;
		return newFoo;
	}

}