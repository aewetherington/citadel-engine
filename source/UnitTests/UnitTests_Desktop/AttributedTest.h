#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include "AttributedFoo.h"
#include "AttributedBar.h"

using namespace Library;

class AttributedTestSuite : public CxxTest::TestSuite
{
public:

	void TestAttributedInstantiation()
	{
		UnitTests::AttributedFoo foo;
	}

	void TestAttributedClear()
	{
		UnitTests::AttributedFoo foo;
		
		TS_ASSERT_DIFFERS(foo.Find("this"), nullptr);
		TS_ASSERT_DIFFERS(foo.Find("mInt"), nullptr);

		TS_ASSERT(foo.IsPrescribedAttribute("this"));
		TS_ASSERT(foo.IsPrescribedAttribute("mInt"));

		TS_ASSERT_THROWS_NOTHING(foo.Clear());

		TS_ASSERT_DIFFERS(foo.Find("this"), nullptr);
		TS_ASSERT_EQUALS(foo.Find("mInt"), nullptr);

		TS_ASSERT(foo.IsPrescribedAttribute("this"));
		TS_ASSERT(!foo.IsPrescribedAttribute("mInt"));

		TS_ASSERT_THROWS_NOTHING(foo.Populate());

		TS_ASSERT_DIFFERS(foo.Find("this"), nullptr);
		TS_ASSERT_DIFFERS(foo.Find("mInt"), nullptr);

		TS_ASSERT(foo.IsPrescribedAttribute("this"));
		TS_ASSERT(foo.IsPrescribedAttribute("mInt"));
	}

	void TestAttributedUniqueSignature()
	{
		UnitTests::AttributedFoo foo;
		UnitTests::AttributedBar bar;

		TS_ASSERT(foo.IsPrescribedAttribute("mInt"));
		TS_ASSERT(!foo.IsPrescribedAttribute("Num"));

		TS_ASSERT(!bar.IsPrescribedAttribute("mInt"));
		TS_ASSERT(bar.IsPrescribedAttribute("Num"));
	}

	void TestAttributedAuxiliaryAttribute()
	{
		UnitTests::AttributedFoo foo;

		TS_ASSERT_THROWS_NOTHING(foo.AppendAuxiliaryAttribute("MyNum") = 100);

		TS_ASSERT(foo.IsAttribute("MyNum"));
		TS_ASSERT(!foo.IsPrescribedAttribute("MyNum"));
		TS_ASSERT(foo.IsAuxiliaryAttribute("MyNum"));

		TS_ASSERT_EQUALS(foo["MyNum"], 100);
		TS_ASSERT_EQUALS(foo.AuxiliaryBegin(), 25);
		TS_ASSERT_EQUALS(foo[foo.AuxiliaryBegin()], 100);
	}

	void TestAttributedCopy()
	{
		UnitTests::AttributedFoo foo;

		TS_ASSERT_EQUALS(foo["mInt"], 1);
		TS_ASSERT_THROWS_NOTHING(foo["mInt"] = 5);
		TS_ASSERT_EQUALS(foo.mInt, 5);

		UnitTests::AttributedFoo foo1 = foo;
		UnitTests::AttributedFoo foo2;

		TS_ASSERT_THROWS_NOTHING(foo1["mInt"]);
		TS_ASSERT_EQUALS(foo1["mInt"], 5);

		TS_ASSERT_EQUALS(foo2["mInt"], 1);
		TS_ASSERT_THROWS_NOTHING(foo2 = foo);
		TS_ASSERT_EQUALS(foo2["mInt"], 5);

		TS_ASSERT_THROWS_NOTHING(foo["mInt"] = 100);
		TS_ASSERT_EQUALS(foo1["mInt"], 100);
		TS_ASSERT_EQUALS(foo2["mInt"], 100);

		TS_ASSERT_EQUALS(foo["aInt"], -1);
		TS_ASSERT_EQUALS(foo1["aInt"], -1);
		TS_ASSERT_EQUALS(foo2["aInt"], -1);
		TS_ASSERT_THROWS_NOTHING(foo["aInt"] = -100);
		TS_ASSERT_EQUALS(foo1["aInt"], -1);
		TS_ASSERT_EQUALS(foo2["aInt"], -1);

		TS_ASSERT_EQUALS(foo["this"].GetPointer(), &foo);
		TS_ASSERT_EQUALS(foo1["this"].GetPointer(), &foo1);
		TS_ASSERT_EQUALS(foo2["this"].GetPointer(), &foo2);
	}
};