#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>
#include "XmlParseMaster.h"
#include "FooParseHelper.h"
#include "FooSharedData.h"

using namespace Library;

class XmlParseMasterTestSuite : public CxxTest::TestSuite
{
public:
	void setUp()
	{

	}

	void tearDown()
	{

	}

	void TestXmlParseMasterInstantiate()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		TS_ASSERT_EQUALS(&sharedData, parseMaster.GetSharedData());
	}

	void TestXmlParseMasterNoHelperParse()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		const char* str = "<integer name=\"Health\" value=\"100\">PlayerName</integer>";
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(str, strlen(str), true));
	}

	void TestXmlParseMasterParse1()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		UnitTests::FooParseHelper fooParse;
		TS_ASSERT_THROWS_NOTHING(parseMaster.AddHelper(fooParse));

		const char* str = "<integer name=\"Health\" value=\"100\"></integer>";
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(str, strlen(str), true));

		TS_ASSERT_EQUALS(fooParse.mNumStartHandles, 1);
		TS_ASSERT_EQUALS(fooParse.mNumEndHandles, 1);
		TS_ASSERT_EQUALS(fooParse.mNumCharHandles, 0);
		TS_ASSERT_EQUALS(sharedData.mMaxDepth, 1);
	}

	void TestXmlParseMasterParse2()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		UnitTests::FooParseHelper fooParse;
		TS_ASSERT_THROWS_NOTHING(parseMaster.AddHelper(fooParse));

		const char* str = "<integer><name>Health</name><value>100</value></integer>";
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(str, strlen(str), true));

		TS_ASSERT_EQUALS(fooParse.mNumStartHandles, 3);
		TS_ASSERT_EQUALS(fooParse.mNumEndHandles, 3);
		TS_ASSERT_EQUALS(fooParse.mNumCharHandles, 2);
		TS_ASSERT_EQUALS(sharedData.mMaxDepth, 2);
	}

	void TestXmlParseMasterParse3()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		UnitTests::FooParseHelper fooParse;
		TS_ASSERT_THROWS_NOTHING(parseMaster.AddHelper(fooParse));

		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse("<integer><name>Health</name><value>1", 36, false));
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse("00</value></integer>", 20, false));
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse("", 0, true));

		TS_ASSERT_EQUALS(fooParse.mNumStartHandles, 3);
		TS_ASSERT_EQUALS(fooParse.mNumEndHandles, 3);
		TS_ASSERT_EQUALS(fooParse.mNumCharHandles, 3);
		TS_ASSERT_EQUALS(sharedData.mMaxDepth, 2);
	}

	void TestXmlParseMasterParseFile()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		UnitTests::FooParseHelper fooParse;
		TS_ASSERT_THROWS_NOTHING(parseMaster.AddHelper(fooParse));

		TS_ASSERT_THROWS_NOTHING(parseMaster.ParseFromFile("common\\test.xml"));

		TS_ASSERT_EQUALS(fooParse.mNumStartHandles, 5);
		TS_ASSERT_EQUALS(fooParse.mNumEndHandles, 5);
		TS_ASSERT_EQUALS(fooParse.mNumCharHandles, 8);
		TS_ASSERT_EQUALS(sharedData.mMaxDepth, 2);
	}

	void TestXmlParseMasterParseFileClone()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		UnitTests::FooParseHelper fooParse;
		TS_ASSERT_THROWS_NOTHING(parseMaster.AddHelper(fooParse));

		TS_ASSERT_THROWS_NOTHING(parseMaster.ParseFromFile("common\\test.xml"));

		TS_ASSERT_EQUALS(fooParse.mNumStartHandles, 5);
		TS_ASSERT_EQUALS(fooParse.mNumEndHandles, 5);
		TS_ASSERT_EQUALS(fooParse.mNumCharHandles, 8);
		TS_ASSERT_EQUALS(sharedData.mMaxDepth, 2);

		// Try to use make and use a clone.
		XmlParseMaster* newMaster = parseMaster.Clone();
		UnitTests::FooSharedData* newData = reinterpret_cast<UnitTests::FooSharedData*>(newMaster->GetSharedData());

		TS_ASSERT_EQUALS(newMaster->GetFileName(), "");
		TS_ASSERT_EQUALS(newData->mMaxDepth, 0);

		TS_ASSERT_THROWS_NOTHING(newMaster->ParseFromFile("common\\test.xml"));

		TS_ASSERT_EQUALS(newMaster->GetFileName(), "common\\test.xml");
		TS_ASSERT_EQUALS(newData->mMaxDepth, 2);

		delete newMaster;
	}

	void TestXmlParseMasterManageHelper()
	{
		UnitTests::FooSharedData sharedData;
		XmlParseMaster parseMaster(&sharedData);

		UnitTests::FooParseHelper fooParse;
		TS_ASSERT_THROWS_NOTHING(parseMaster.AddHelper(fooParse));

		TS_ASSERT_THROWS_NOTHING(parseMaster.RemoveHelper(fooParse));

		const char* str = "<integer name=\"Health\" value=\"100\"></integer>";
		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(str, strlen(str), true));

		TS_ASSERT_EQUALS(fooParse.mNumStartHandles, 0);
		TS_ASSERT_EQUALS(fooParse.mNumEndHandles, 0);
		TS_ASSERT_EQUALS(fooParse.mNumCharHandles, 0);
		TS_ASSERT_EQUALS(sharedData.mMaxDepth, 1);
	}
};