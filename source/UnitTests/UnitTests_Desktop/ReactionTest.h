#include <cxxtest/TestSuite.h>
#include <string>
#include <cstdint>

#include "Event.h"
#include "World.h"
#include "WorldState.h"
#include "EventMessageAttributed.h"
#include "ReactionAttributed.h"
#include "ActionEvent.h"

#include "ActionCreateAction.h"
#include "ActionList.h"

#include "XmlParseHelperAction.h"
#include "XmlParseHelperPrimitive.h"
#include "XmlParseHelperWorld.h"
#include "XmlParseMaster.h"

using namespace std;
using namespace Library;

class ReactionTestSuite : public CxxTest::TestSuite
{
public:

	void TestReactionInstantiate()
	{
		ActionEvent ae;
		EventMessageAttributed ema;
		ReactionAttributed ra;
	}

	void TestReactionEmpty()
	{
		World world;
		WorldState state;
		ReactionAttributed* ra = new ReactionAttributed();

		// Add reaction to the world, and ensure the update call is OK.
		TS_ASSERT_THROWS_NOTHING(ra->Find(ReactionAttributed::sSubtypeReactionsTag)->Set("FooSubtype", 0));
		TS_ASSERT_THROWS_NOTHING(ra->SetOwner(&world));
		TS_ASSERT_THROWS_NOTHING(world.Update(state));

		// Make a new event and send it off!
		EventMessageAttributed ema;
		TS_ASSERT_THROWS_NOTHING(ema.SetSubtype("FooSubtype"));
		TS_ASSERT_THROWS_NOTHING(ema.SetWorld(&world));
		auto eema = make_shared<Event<EventMessageAttributed>>(ema);
		TS_ASSERT_THROWS_NOTHING(world.GetEventQueue().Send(eema));
	}

	void TestReactionCreate()
	{
		ActionListFactory alf;

		World world;
		WorldState state;
		ReactionAttributed* ra = new ReactionAttributed("MyReaction");
		ActionCreateAction* aca = new ActionCreateAction("CreateAction");

		// Set up the reaction.
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeClass("ActionList"));
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeName("CreatedAction"));
		TS_ASSERT_THROWS_NOTHING(aca->SetOwner(ra));

		TS_ASSERT_DIFFERS(ra->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(ra->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);

		// Add reaction to the world, and ensure the update call behaves as expected.
		TS_ASSERT_THROWS_NOTHING(ra->Find(ReactionAttributed::sSubtypeReactionsTag)->Set("FooSubtype", 0));
		TS_ASSERT_THROWS_NOTHING(ra->SetOwner(&world));
		TS_ASSERT_THROWS_NOTHING(world.Update(state));

		TS_ASSERT_DIFFERS(ra->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(ra->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);

		// Make a new event and send it off!
		EventMessageAttributed ema;
		TS_ASSERT_THROWS_NOTHING(ema.SetSubtype("FooSubtype"));
		TS_ASSERT_THROWS_NOTHING(ema.SetWorld(&world));
		auto eema = make_shared<Event<EventMessageAttributed>>(ema);
		TS_ASSERT_THROWS_NOTHING(world.GetEventQueue().Send(eema));

		TS_ASSERT_DIFFERS(ra->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);
	}

	void TestActionEventReactionCreate()
	{
		ActionListFactory alf;

		World world;
		WorldState state;
		ReactionAttributed* ra = new ReactionAttributed("MyReaction");
		ActionCreateAction* aca = new ActionCreateAction("CreateAction");

		// Set up the reaction.
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeClass("ActionList"));
		TS_ASSERT_THROWS_NOTHING(aca->SetPrototypeName("CreatedAction"));
		TS_ASSERT_THROWS_NOTHING(aca->SetOwner(ra));

		TS_ASSERT_DIFFERS(ra->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(ra->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);

		// Add reaction to the world, and ensure the update call behaves as expected.
		TS_ASSERT_THROWS_NOTHING(ra->Find(ReactionAttributed::sSubtypeReactionsTag)->Set("FooSubtype", 0));
		TS_ASSERT_THROWS_NOTHING(ra->SetOwner(&world));
		TS_ASSERT_THROWS_NOTHING(world.Update(state));

		TS_ASSERT_DIFFERS(ra->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(ra->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);

		// Make a new event and attach it!
		ActionEvent* ae = new ActionEvent();
		TS_ASSERT_THROWS_NOTHING(ae->SetSubtype("FooSubtype"));
		TS_ASSERT_THROWS_NOTHING(ae->SetDelay(0));
		TS_ASSERT_THROWS_NOTHING(ae->SetOwner(&world));
		TS_ASSERT_THROWS_NOTHING(world.Update(state));

		TS_ASSERT_DIFFERS(ra->Find("actions"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(ra->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);
	}

	void TestReactionXML1()
	{
		ActionListFactory alf;
		ReactionAttributedFactory raf;
		ActionCreateActionFactory acaf;

		World world;
		WorldState state;

		SharedDataTable sharedData(world);
		XmlParseHelperWorld parseWorld;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseHelperAction parseAction;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseWorld);
		parseMaster.AddHelper(parsePrimitive);
		parseMaster.AddHelper(parseAction);

		const char* xml =	"<world name='MyWorld'>"
								"<action name='MyReaction' class='ReactionAttributed' subtypeReactions='FooSubtype'>"
									"<action name='CreateAction' class='ActionCreateAction' prototypeName='CreatedAction' prototypeClass='ActionList'>"
									"</action>"
								"</action>"
							"</world>";

		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml, strlen(xml), true));

		// Check that the state pre-update is accurate.
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);

		TS_ASSERT_THROWS_NOTHING(world.Update(state));

		// Check that the state post-update is accurate.
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);
	}

	void TestReactionXML2()
	{
		ActionListFactory alf;
		ReactionAttributedFactory raf;
		ActionCreateActionFactory acaf;
		ActionEventFactory aef;

		World world;
		WorldState state;

		SharedDataTable sharedData(world);
		XmlParseHelperWorld parseWorld;
		XmlParseHelperPrimitive parsePrimitive;
		XmlParseHelperAction parseAction;
		XmlParseMaster parseMaster(&sharedData);

		parseMaster.AddHelper(parseWorld);
		parseMaster.AddHelper(parsePrimitive);
		parseMaster.AddHelper(parseAction);

		const char* xml =   "<world name='MyWorld'>"
								"<action name='MyReaction' class='ReactionAttributed' subtypeReactions='FooSubtype'>"
									"<action name='CreateAction' class='ActionCreateAction' prototypeName='CreatedAction' prototypeClass='ActionList'>"
									"</action>"
								"</action>"
								"<action name='MyEvent' class='ActionEvent' subtype='FooSubtype'>"
									"<int name='delay'>0</int>"
								"</action>"
							"</world>";

		TS_ASSERT_THROWS_NOTHING(parseMaster.Parse(xml, strlen(xml), true));

		// Check that the state pre-update is accurate.
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyEvent"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_EQUALS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);

		TS_ASSERT_THROWS_NOTHING(world.Update(state));

		// Check that the state post-update is accurate.
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyEvent"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreateAction"), nullptr);
		TS_ASSERT_DIFFERS(world.Find("actions")->GetTable()->Find("MyReaction")->GetTable()->Find("actions")->GetTable()->Find("CreatedAction"), nullptr);
	}
};