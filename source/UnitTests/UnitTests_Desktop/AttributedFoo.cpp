#include "AttributedFoo.h"
#include "Foo.h"

using namespace Library;

namespace UnitTests
{
	RTTI_DEFINITIONS(AttributedFoo);

	AttributedFoo::AttributedFoo()
		: mInt(1), mFloat(1.0f), mVec(glm::vec4(1)), mMat(glm::mat4(1)), mStr("One"),
		mIntArray(new int[2]), mFloatArray(new float[2]), mVecArray(new glm::vec4[2]),
		mMatArray(new glm::mat4[2]), mStrArray(new std::string[2]),
		aFooArray(new Foo[2])
	{
		// Initiialize the external storage arrays.
		mIntArray[0] = 2;
		mIntArray[1] = 3;
		mFloatArray[0] = 2.0f;
		mFloatArray[1] = 3.0f;
		mVecArray[0] = glm::vec4(2);
		mVecArray[1] = glm::vec4(3);
		mMatArray[0] = glm::mat4(2);
		mMatArray[1] = glm::mat4(3);
		mStrArray[0] = "Two";
		mStrArray[1] = "Three";

		Populate();
	}

	AttributedFoo::AttributedFoo(AttributedFoo& rhs)
		: Attributed(rhs)
	{
		// Note: This testing class does not update its own member variables during copy.
		// They currently aren't used except for initialization, and a single test to verify
		// that external storage is functioning correctly, so it would be a waste of time
		// to bother with it, presently.
	}

	AttributedFoo::~AttributedFoo()
	{
		delete[] mIntArray;
		delete[] mFloatArray;
		delete[] mVecArray;
		delete[] mMatArray;
		delete[] mStrArray;
		delete[] aFooArray;
	}


	AttributedFoo& AttributedFoo::operator=(AttributedFoo& rhs)
	{
		// Note: This testing class does not update its own member variables during copy.
		// They currently aren't used except for initialization, and a single test to verify
		// that external storage is functioning correctly, so it would be a waste of time
		// to bother with it, presently.
		Attributed::operator=(rhs);
		return *this;
	}

	void AttributedFoo::Populate()
	{
		// Internal storage things.
		int aInt = -1;
		float aFloat = -1.0f;
		glm::vec4 aVec = glm::vec4(-1);
		glm::mat4 aMat = glm::mat4(-1);
		std::string aStr = "Negative One";
		Scope* aScope = new Scope();

		int aIntArray[] = { -2, -3 };
		float aFloatArray[] = { -2.0f, -3.0f };
		glm::vec4 aVecArray[] = { glm::vec4(-2), glm::vec4(-3) };
		glm::mat4 aMatArray[] = { glm::mat4(-2), glm::mat4(-3) };
		std::string aStrArray[] = { "Negative Two", "Negative Three" };
		Scope* aScopeArray[2] = { new Scope() };

		// Begin loading the signature.
		INIT_SIGNATURE();

		// Externals
		EXTERNAL_ATTRIBUTE("mInt", Datum::DatumType::Integer, &mInt, 1);
		EXTERNAL_ATTRIBUTE("mFloat", Datum::DatumType::Float, &mFloat, 1);
		EXTERNAL_ATTRIBUTE("mVec", Datum::DatumType::Vector, &mVec, 1);
		EXTERNAL_ATTRIBUTE("mMat", Datum::DatumType::Matrix, &mMat, 1);
		EXTERNAL_ATTRIBUTE("mStr", Datum::DatumType::String, &mStr, 1);

		EXTERNAL_ATTRIBUTE("mIntArray", Datum::DatumType::Integer, mIntArray, 2);
		EXTERNAL_ATTRIBUTE("mFloatArray", Datum::DatumType::Float, mFloatArray, 2);
		EXTERNAL_ATTRIBUTE("mVecArray", Datum::DatumType::Vector, mVecArray, 2);
		EXTERNAL_ATTRIBUTE("mMatArray", Datum::DatumType::Matrix, mMatArray, 2);
		EXTERNAL_ATTRIBUTE("mStrArray", Datum::DatumType::String, mStrArray, 2);

		// Internals
		INTERNAL_ATTRIBUTE("aInt", Datum::DatumType::Integer, &aInt, 1);
		INTERNAL_ATTRIBUTE("aFloat", Datum::DatumType::Float, &aFloat, 1);
		INTERNAL_ATTRIBUTE("aVec", Datum::DatumType::Vector, &aVec, 1);
		INTERNAL_ATTRIBUTE("aMat", Datum::DatumType::Matrix, &aMat, 1);
		INTERNAL_ATTRIBUTE("aStr", Datum::DatumType::String, &aStr, 1);
		INTERNAL_ATTRIBUTE("aScope", Datum::DatumType::Table, &aScope, 1);
		INTERNAL_ATTRIBUTE("aFoo", Datum::DatumType::Pointer, &aFoo, 1);

		INTERNAL_ATTRIBUTE("aIntArray", Datum::DatumType::Integer, aIntArray, 2);
		INTERNAL_ATTRIBUTE("aFloatArray", Datum::DatumType::Float, aFloatArray, 2);
		INTERNAL_ATTRIBUTE("aVecArray", Datum::DatumType::Vector, aVecArray, 2);
		INTERNAL_ATTRIBUTE("aMatArray", Datum::DatumType::Matrix, aMatArray, 2);
		INTERNAL_ATTRIBUTE("aStrArray", Datum::DatumType::String, aStrArray, 2);
		INTERNAL_ATTRIBUTE("aScopeArray", Datum::DatumType::Table, aScopeArray, 2);
		INTERNAL_ATTRIBUTE("aFooArray", Datum::DatumType::Pointer, aFooArray, 2);

		Attributed::Populate();
	}
}